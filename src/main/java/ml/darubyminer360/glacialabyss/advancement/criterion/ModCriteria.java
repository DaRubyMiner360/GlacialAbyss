package ml.darubyminer360.glacialabyss.advancement.criterion;

import com.google.gson.JsonObject;
import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.advancement.criterion.custom.*;
import net.minecraft.advancement.criterion.*;
import net.minecraft.util.Identifier;

import java.lang.reflect.InvocationTargetException;

public class ModCriteria {
//    public static final OnHelpedCriterion PLAYER_HELPED_ENTITY = (OnHelpedCriterion) register("player_helped_entity", OnHelpedCriterion.class);
    public static final OnHelpedCriterion PLAYER_HELPED_ENTITY = Criteria.register(new OnHelpedCriterion(new Identifier(GlacialAbyss.MOD_ID, "player_helped_entity")));

//    private static Criterion register(String name, Class<? extends Criterion> clazz) {
//        try {
//            return Criteria.register(clazz.getConstructor().newInstance(new Identifier(GlacialAbyss.MOD_ID, name)));
//        } catch (InstantiationException | NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
//            throw new RuntimeException(e);
//        }
//    }

    public static void register() {
        GlacialAbyss.LOGGER.info("Registering Mod Criteria for " + GlacialAbyss.MOD_ID);
    }
}
