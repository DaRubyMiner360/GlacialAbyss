package ml.darubyminer360.glacialabyss.advancement.criterion.custom;

import com.google.gson.JsonObject;
import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.advancement.criterion.ModCriteria;
import net.minecraft.advancement.criterion.AbstractCriterion;
import net.minecraft.advancement.criterion.AbstractCriterionConditions;
import net.minecraft.advancement.criterion.Criteria;
import net.minecraft.entity.Entity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.loot.context.LootContext;
import net.minecraft.predicate.entity.AdvancementEntityPredicateDeserializer;
import net.minecraft.predicate.entity.AdvancementEntityPredicateSerializer;
import net.minecraft.predicate.entity.DamageSourcePredicate;
import net.minecraft.predicate.entity.EntityPredicate;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Identifier;

public class OnHelpedCriterion extends AbstractCriterion<OnHelpedCriterion.Conditions> {
    public final Identifier id;

    public OnHelpedCriterion(Identifier id) {
        this.id = id;
    }

    @Override
    public Identifier getId() {
        return id;
    }

    public Conditions conditionsFromJson(JsonObject jsonObject, EntityPredicate.Extended extended, AdvancementEntityPredicateDeserializer advancementEntityPredicateDeserializer) {
        return new Conditions(this.id, extended, EntityPredicate.Extended.getInJson(jsonObject, "entity", advancementEntityPredicateDeserializer));
    }

    public void trigger(ServerPlayerEntity player, Entity entity) {
        LootContext lootContext = EntityPredicate.createAdvancementEntityLootContext(player, entity);
        this.trigger(player, (conditions) -> conditions.test(player, lootContext));
    }

    public static class Conditions extends AbstractCriterionConditions {
        private final EntityPredicate.Extended entity;

        public Conditions(Identifier id, EntityPredicate.Extended player, EntityPredicate.Extended entity) {
            super(id, player);
            this.entity = entity;
        }

        public static Conditions createPlayerHelpedEntity(EntityPredicate killedEntityPredicate) {
            return new Conditions(ModCriteria.PLAYER_HELPED_ENTITY.id, EntityPredicate.Extended.EMPTY, EntityPredicate.Extended.ofLegacy(killedEntityPredicate));
        }

        public static Conditions createPlayerHelpedEntity(EntityPredicate.Builder killedEntityPredicateBuilder) {
            return new Conditions(ModCriteria.PLAYER_HELPED_ENTITY.id, EntityPredicate.Extended.EMPTY, EntityPredicate.Extended.ofLegacy(killedEntityPredicateBuilder.build()));
        }

        public static Conditions createPlayerHelpedEntity() {
            return new Conditions(ModCriteria.PLAYER_HELPED_ENTITY.id, EntityPredicate.Extended.EMPTY, EntityPredicate.Extended.EMPTY);
        }

        public static Conditions createPlayerHelpedEntity(EntityPredicate killedEntityPredicate, DamageSourcePredicate damageSourcePredicate) {
            return new Conditions(ModCriteria.PLAYER_HELPED_ENTITY.id, EntityPredicate.Extended.EMPTY, EntityPredicate.Extended.ofLegacy(killedEntityPredicate));
        }

        public static Conditions createPlayerHelpedEntity(EntityPredicate.Builder killedEntityPredicateBuilder, DamageSourcePredicate damageSourcePredicate) {
            return new Conditions(ModCriteria.PLAYER_HELPED_ENTITY.id, EntityPredicate.Extended.EMPTY, EntityPredicate.Extended.ofLegacy(killedEntityPredicateBuilder.build()));
        }

        public static Conditions createPlayerHelpedEntity(EntityPredicate killedEntityPredicate, DamageSourcePredicate.Builder damageSourcePredicateBuilder) {
            return new Conditions(ModCriteria.PLAYER_HELPED_ENTITY.id, EntityPredicate.Extended.EMPTY, EntityPredicate.Extended.ofLegacy(killedEntityPredicate));
        }

        public static Conditions createPlayerHelpedEntity(EntityPredicate.Builder killedEntityPredicateBuilder, DamageSourcePredicate.Builder killingBlowBuilder) {
            return new Conditions(ModCriteria.PLAYER_HELPED_ENTITY.id, EntityPredicate.Extended.EMPTY, EntityPredicate.Extended.ofLegacy(killedEntityPredicateBuilder.build()));
        }

        public boolean test(ServerPlayerEntity player, LootContext killedEntityContext) {
            return this.entity.test(killedEntityContext);
        }

        public JsonObject toJson(AdvancementEntityPredicateSerializer predicateSerializer) {
            JsonObject jsonObject = super.toJson(predicateSerializer);
            jsonObject.add("entity", this.entity.toJson(predicateSerializer));
            return jsonObject;
        }
    }
}
