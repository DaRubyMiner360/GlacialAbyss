package ml.darubyminer360.glacialabyss.util;

import me.shedaniel.autoconfig.ConfigData;
import me.shedaniel.autoconfig.annotation.Config;
import me.shedaniel.autoconfig.annotation.ConfigEntry;

// Variables are in snake case for consistency in the language file.
@Config(name = "glacialabyss")
@Config.Gui.Background("minecraft:textures/block/dirt.png")
public class GlacialAbyssConfig implements ConfigData {
    public int abyssal_armor_effect_strength = 1;
    public int charged_abyssal_armor_effect_strength = 3;

    @ConfigEntry.Gui.CollapsibleObject(startExpanded = true)
    public AltarPunishment altar_punishment = new AltarPunishment();

    public static class AltarPunishment {
        public boolean strike_lightning = false;
        public boolean do_damage = false;
        public boolean replace_blocks = false;
    }
}
