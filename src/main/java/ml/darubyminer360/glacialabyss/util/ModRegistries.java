package ml.darubyminer360.glacialabyss.util;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.block.ModBlocks;
import ml.darubyminer360.glacialabyss.command.ResetBossProgressionCommand;
import ml.darubyminer360.glacialabyss.entity.ModEntities;
import ml.darubyminer360.glacialabyss.entity.boss.phantasm.PhantasmEntity;
import ml.darubyminer360.glacialabyss.item.ModItems;
import net.fabricmc.fabric.api.command.v1.CommandRegistrationCallback;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricDefaultAttributeRegistry;
import net.fabricmc.fabric.api.registry.FlammableBlockRegistry;
import net.fabricmc.fabric.api.registry.FuelRegistry;
import net.fabricmc.fabric.api.registry.StrippableBlockRegistry;

public class ModRegistries {
    public static void register() {
        registerFuels();
        registerCommands();

        registerStrippables();
        registerFlammableBlock();

        registerAttributes();
    }

    private static void registerFuels() {
        GlacialAbyss.LOGGER.info("Registering Fuels for " + GlacialAbyss.MOD_ID);
        FuelRegistry registry = FuelRegistry.INSTANCE;

        registry.add(ModItems.ABYSSAL_DUST, 200);
        registry.add(ModItems.CHARGED_ABYSSAL_DUST, 1000);
    }

    private static void registerCommands() {
        CommandRegistrationCallback.EVENT.register(ResetBossProgressionCommand::register);
    }

    private static void registerStrippables() {
        StrippableBlockRegistry.register(ModBlocks.FROSTED_LOG, ModBlocks.STRIPPED_FROSTED_LOG);
        StrippableBlockRegistry.register(ModBlocks.FROSTED_WOOD, ModBlocks.STRIPPED_FROSTED_WOOD);

        StrippableBlockRegistry.register(ModBlocks.HAUNTED_LOG, ModBlocks.STRIPPED_HAUNTED_LOG);
        StrippableBlockRegistry.register(ModBlocks.HAUNTED_WOOD, ModBlocks.STRIPPED_HAUNTED_WOOD);
    }

    private static void registerFlammableBlock() {
        FlammableBlockRegistry instance = FlammableBlockRegistry.getDefaultInstance();

        instance.add(ModBlocks.FROSTED_LOG, 5, 5);
        instance.add(ModBlocks.STRIPPED_FROSTED_LOG, 5, 5);
        instance.add(ModBlocks.FROSTED_WOOD, 5, 5);
        instance.add(ModBlocks.STRIPPED_FROSTED_WOOD, 5, 5);
        instance.add(ModBlocks.FROSTED_PLANKS, 5, 20);
        instance.add(ModBlocks.FROSTED_LEAVES, 30, 60);

        instance.add(ModBlocks.HAUNTED_LOG, 5, 5);
        instance.add(ModBlocks.STRIPPED_HAUNTED_LOG, 5, 5);
        instance.add(ModBlocks.HAUNTED_WOOD, 5, 5);
        instance.add(ModBlocks.STRIPPED_HAUNTED_WOOD, 5, 5);
        instance.add(ModBlocks.HAUNTED_PLANKS, 5, 20);
        instance.add(ModBlocks.HAUNTED_LEAVES, 30, 60);
    }

    private static void registerAttributes() {
        FabricDefaultAttributeRegistry.register(ModEntities.PHANTASM, PhantasmEntity.setAttributes());
    }
}
