package ml.darubyminer360.glacialabyss.util;

import ml.darubyminer360.glacialabyss.world.ModDimensions;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectionContext;
import net.fabricmc.fabric.api.biome.v1.NetherBiomes;
import net.minecraft.world.dimension.DimensionOptions;

import java.util.function.Predicate;

public final class ModBiomeSelectors {
    private ModBiomeSelectors() {
    }

    /**
     * Returns a biome selector that will match all biomes that would normally spawn in the Glacial Abyss.
     */
    public static Predicate<BiomeSelectionContext> foundInTheGlacialAbyss() {
        return context -> context.canGenerateIn(ModDimensions.GLACIAL_ABYSS_OPTIONS);
    }
}
