package ml.darubyminer360.glacialabyss.util;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.tag.TagKey;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModTags {
    public static class Blocks {
//        public static final Tag.Identified<Block> FROSTED_LOGS = createTag("frosted_logs");
        public static final TagKey<Block> FROSTED_LOGS = createTag("frosted_logs");
//        public static final Tag.Identified<Block> HAUNTED_LOGS = createTag("haunted_logs");
        public static final TagKey<Block> HAUNTED_LOGS = createTag("haunted_logs");
//        public static final Tag.Identified<Block> HAUNTED_LOGS = createTag("haunted_logs");
        public static final TagKey<Block> BASE_STONE_GLACIAL_ABYSS = createTag("base_stone_glacial_abyss");

        private static TagKey<Block> createTag(String name) {
//            return TagFactory.BLOCK.create(new Identifier(GlacialAbyss.MOD_ID, name));
            return TagKey.of(Registry.BLOCK_KEY, new Identifier(GlacialAbyss.MOD_ID, name));
        }

        private static TagKey<Block> createCommonTag(String name) {
//            return TagFactory.BLOCK.create(new Identifier("c", name));
            return TagKey.of(Registry.BLOCK_KEY, new Identifier("c", name));
        }
    }

    public static class Items {
//        public static final Tag.Identified<Item> FROSTED_LOGS = createTag("frosted_logs");
        public static final TagKey<Item> FROSTED_LOGS = createTag("frosted_logs");
//        public static final Tag.Identified<Item> HAUNTED_LOGS = createTag("haunted_logs");
        public static final TagKey<Item> HAUNTED_LOGS = createTag("haunted_logs");
//        public static final Tag.Identified<Item> CRYSTALS = createTag("crystals");
        public static final TagKey<Item> CRYSTALS = createCommonTag("crystals");

//        public static final Tag.Identified<Item> KEYS = createTag("keys");
        public static final TagKey<Item> KEYS = createTag("keys");
//        public static final Tag.Identified<Item> TIER_ZERO_KEYS = createTag("t0_keys");
        public static final TagKey<Item> TIER_ZERO_KEYS = createTag("t0_keys");
//        public static final Tag.Identified<Item> TIER_ONE_KEYS = createTag("t1_keys");
        public static final TagKey<Item> TIER_ONE_KEYS = createTag("t1_keys");
//        public static final Tag.Identified<Item> TIER_TWO_KEYS = createTag("t2_keys");
        public static final TagKey<Item> TIER_TWO_KEYS = createTag("t2_keys");
//        public static final Tag.Identified<Item> TIER_THREE_KEYS = createTag("t3_keys");
        public static final TagKey<Item> TIER_THREE_KEYS = createTag("t3_keys");
//        public static final Tag.Identified<Item> TIER_FOUR_KEYS = createTag("t4_keys");
        public static final TagKey<Item> TIER_FOUR_KEYS = createTag("t4_keys");
//        public static final Tag.Identified<Item> TIER_FIVE_KEYS = createTag("t5_keys");
        public static final TagKey<Item> TIER_FIVE_KEYS = createTag("t5_keys");
//        public static final Tag.Identified<Item> TIER_SIX_KEYS = createTag("t6_keys");
        public static final TagKey<Item> TIER_SIX_KEYS = createTag("t6_keys");
//        public static final Tag.Identified<Item> TIER_SEVEN_KEYS = createTag("t7_keys");
        public static final TagKey<Item> TIER_SEVEN_KEYS = createTag("t7_keys");
//        public static final Tag.Identified<Item> TIER_EIGHT_KEYS = createTag("t8_keys");
        public static final TagKey<Item> TIER_EIGHT_KEYS = createTag("t8_keys");
//        public static final Tag.Identified<Item> TIER_NINE_KEYS = createTag("t9_keys");
        public static final TagKey<Item> TIER_NINE_KEYS = createTag("t9_keys");
//        public static final Tag.Identified<Item> TIER_TEN_KEYS = createTag("t10_keys");
        public static final TagKey<Item> TIER_TEN_KEYS = createTag("t10_keys");

//        public static final Tag.Identified<Item> KEY_MOLDS = createTag("key_molds");
        public static final TagKey<Item> KEY_MOLDS = createTag("key_molds");
//        public static final Tag.Identified<Item> TIER_ZERO_KEY_MOLDS = createTag("t0_key_molds");
        public static final TagKey<Item> TIER_ZERO_KEY_MOLDS = createTag("t0_key_molds");
//        public static final Tag.Identified<Item> TIER_ONE_KEY_MOLDS = createTag("t1_key_molds");
        public static final TagKey<Item> TIER_ONE_KEY_MOLDS = createTag("t1_key_molds");
//        public static final Tag.Identified<Item> TIER_TWO_KEYS = createTag("t2_key_molds");
        public static final TagKey<Item> TIER_TWO_KEY_MOLDS = createTag("t2_key_molds");
//        public static final Tag.Identified<Item> TIER_THREE_KEY_MOLDS = createTag("t3_key_molds");
        public static final TagKey<Item> TIER_THREE_KEY_MOLDS = createTag("t3_key_molds");
//        public static final Tag.Identified<Item> TIER_FOUR_KEY_MOLDS = createTag("t4_key_molds");
        public static final TagKey<Item> TIER_FOUR_KEY_MOLDS = createTag("t4_key_molds");
//        public static final Tag.Identified<Item> TIER_FIVE_KEY_MOLDS = createTag("t5_key_molds");
        public static final TagKey<Item> TIER_FIVE_KEY_MOLDS = createTag("t5_key_molds");
//        public static final Tag.Identified<Item> TIER_SIX_KEY_MOLDS = createTag("t6_key_molds");
        public static final TagKey<Item> TIER_SIX_KEY_MOLDS = createTag("t6_key_molds");
//        public static final Tag.Identified<Item> TIER_SEVEN_KEY_MOLDS = createTag("t7_key_molds");
        public static final TagKey<Item> TIER_SEVEN_KEY_MOLDS = createTag("t7_key_molds");
//        public static final Tag.Identified<Item> TIER_EIGHT_KEY_MOLDS = createTag("t8_key_molds");
        public static final TagKey<Item> TIER_EIGHT_KEY_MOLDS = createTag("t8_key_molds");
//        public static final Tag.Identified<Item> TIER_NINE_KEY_MOLDS = createTag("t9_key_molds");
        public static final TagKey<Item> TIER_NINE_KEY_MOLDS = createTag("t9_key_molds");
//        public static final Tag.Identified<Item> TIER_TEN_KEY_MOLDS = createTag("t10_key_molds");
        public static final TagKey<Item> TIER_TEN_KEY_MOLDS = createTag("t10_key_molds");

//        public static final Tag.Identified<Item> KEY_PARTS = createTag("key_parts");
        public static final TagKey<Item> KEY_PARTS = createTag("key_parts");
//        public static final Tag.Identified<Item> TIER_ZERO_KEY_PARTS = createTag("t0_key_parts");
        public static final TagKey<Item> TIER_ZERO_KEY_PARTS = createTag("t0_key_parts");
//        public static final Tag.Identified<Item> TIER_ONE_KEY_PARTS = createTag("t1_key_parts");
        public static final TagKey<Item> TIER_ONE_KEY_PARTS = createTag("t1_key_parts");
//        public static final Tag.Identified<Item> TIER_TWO_KEYS = createTag("t2_key_parts");
        public static final TagKey<Item> TIER_TWO_KEY_PARTS = createTag("t2_key_parts");
//        public static final Tag.Identified<Item> TIER_THREE_KEY_PARTS = createTag("t3_key_parts");
        public static final TagKey<Item> TIER_THREE_KEY_PARTS = createTag("t3_key_parts");
//        public static final Tag.Identified<Item> TIER_FOUR_KEY_PARTS = createTag("t4_key_parts");
        public static final TagKey<Item> TIER_FOUR_KEY_PARTS = createTag("t4_key_parts");
//        public static final Tag.Identified<Item> TIER_FIVE_KEY_PARTS = createTag("t5_key_parts");
        public static final TagKey<Item> TIER_FIVE_KEY_PARTS = createTag("t5_key_parts");
//        public static final Tag.Identified<Item> TIER_SIX_KEY_PARTS = createTag("t6_key_parts");
        public static final TagKey<Item> TIER_SIX_KEY_PARTS = createTag("t6_key_parts");
//        public static final Tag.Identified<Item> TIER_SEVEN_KEY_PARTS = createTag("t7_key_parts");
        public static final TagKey<Item> TIER_SEVEN_KEY_PARTS = createTag("t7_key_parts");
//        public static final Tag.Identified<Item> TIER_EIGHT_KEY_PARTS = createTag("t8_key_parts");
        public static final TagKey<Item> TIER_EIGHT_KEY_PARTS = createTag("t8_key_parts");
//        public static final Tag.Identified<Item> TIER_NINE_KEY_PARTS = createTag("t9_key_parts");
        public static final TagKey<Item> TIER_NINE_KEY_PARTS = createTag("t9_key_parts");
//        public static final Tag.Identified<Item> TIER_TEN_KEY_PARTS = createTag("t10_key_parts");
        public static final TagKey<Item> TIER_TEN_KEY_PARTS = createTag("t10_key_parts");

        private static TagKey<Item> createTag(String name) {
//            return TagFactory.ITEM.create(new Identifier(GlacialAbyss.MOD_ID, name);
            return TagKey.of(Registry.ITEM_KEY, new Identifier(GlacialAbyss.MOD_ID, name));
        }

        private static TagKey<Item> createCommonTag(String name) {
//            return TagFactory.ITEM.create(new Identifier("c", name));
            return TagKey.of(Registry.ITEM_KEY, new Identifier("c", name));
        }
    }
}
