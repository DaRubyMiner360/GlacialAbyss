package ml.darubyminer360.glacialabyss.util;

import dev.onyxstudios.cca.api.v3.component.ComponentKey;
import dev.onyxstudios.cca.api.v3.component.ComponentRegistryV3;
import dev.onyxstudios.cca.api.v3.level.LevelComponentFactoryRegistry;
import dev.onyxstudios.cca.api.v3.level.LevelComponentInitializer;
import ml.darubyminer360.glacialabyss.GlacialAbyss;
import net.minecraft.util.Identifier;

public final class ModComponents implements LevelComponentInitializer {
    public static final ComponentKey<BooleanComponent> DEFEATED_PHANTASM = ComponentRegistryV3.INSTANCE.getOrCreate(new Identifier(GlacialAbyss.MOD_ID, "defeated_phantasm"), BooleanComponent.class);
    public static final ComponentKey<BooleanComponent> DEFEATED_PARADISE_KING = ComponentRegistryV3.INSTANCE.getOrCreate(new Identifier(GlacialAbyss.MOD_ID, "defeated_paradise_king"), BooleanComponent.class);
    public static final ComponentKey<BooleanComponent> DEFEATED_HAUNTED_PALADIN = ComponentRegistryV3.INSTANCE.getOrCreate(new Identifier(GlacialAbyss.MOD_ID, "defeated_haunted_paladin"), BooleanComponent.class);
    public static final ComponentKey<BooleanComponent> HELPED_HAUNTED_PALADIN = ComponentRegistryV3.INSTANCE.getOrCreate(new Identifier(GlacialAbyss.MOD_ID, "helped_haunted_paladin"), BooleanComponent.class);

    @Override
    public void registerLevelComponentFactories(LevelComponentFactoryRegistry registry) {
        // Add the component to every WorldProperties instance
        registry.register(DEFEATED_PHANTASM, SyncedBooleanComponent::new);
        registry.register(DEFEATED_PARADISE_KING, SyncedBooleanComponent::new);
        registry.register(DEFEATED_HAUNTED_PALADIN, SyncedBooleanComponent::new);
        registry.register(HELPED_HAUNTED_PALADIN, SyncedBooleanComponent::new);
    }
}
