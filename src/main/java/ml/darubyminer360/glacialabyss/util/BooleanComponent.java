package ml.darubyminer360.glacialabyss.util;

import dev.onyxstudios.cca.api.v3.component.ComponentV3;
import dev.onyxstudios.cca.api.v3.component.sync.AutoSyncedComponent;
import dev.onyxstudios.cca.api.v3.level.LevelComponents;
import net.minecraft.entity.Entity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldProperties;

public interface BooleanComponent extends ComponentV3 {
    void setValue(boolean value);
    boolean getValue();
}

class SyncedBooleanComponent implements BooleanComponent, AutoSyncedComponent {
    private boolean value;
    private final WorldProperties provider;
//    private final MinecraftServer provider;

    public SyncedBooleanComponent(WorldProperties provider) { this.provider = provider; }
//    public SyncedBooleanComponent(MinecraftServer provider) { this.provider = provider; }

    @Override
    public void setValue(boolean value) {
        this.value = value;
//        ModComponents.DEFEATED_PHANTASM.sync(this.provider);
//        ModComponents.DEFEATED_PARADISE_KING.sync(this.provider);
//        ModComponents.DEFEATED_HAUNTED_PALADIN.sync(this.provider);
//        ModComponents.HELPED_HAUNTED_PALADIN.sync(this.provider);
//        LevelComponents.sync(this, provider);
    }

    @Override
    public boolean getValue() { return this.value; }
    @Override
    public void readFromNbt(NbtCompound tag) { this.value = tag.getBoolean("value"); }
    @Override
    public void writeToNbt(NbtCompound tag) { tag.putBoolean("value", this.value); }
}
