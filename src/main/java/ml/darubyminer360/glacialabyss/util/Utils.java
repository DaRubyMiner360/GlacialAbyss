package ml.darubyminer360.glacialabyss.util;

import net.minecraft.client.MinecraftClient;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LightningEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.decoration.ItemFrameEntity;
import net.minecraft.entity.projectile.ProjectileUtil;
import net.minecraft.util.hit.EntityHitResult;
import net.minecraft.util.hit.HitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Vec3d;
import net.minecraft.util.math.Vec3f;
import net.minecraft.world.RaycastContext;
import net.minecraft.world.World;

public class Utils {
    public static void strikeLightning(World world, BlockPos pos) {
        strikeLightning(world, pos, 1);
    }

    public static void strikeLightning(World world, BlockPos pos, int amount) {
        for (int i = 0; i < amount; i++) {
            LightningEntity bolt = new LightningEntity(EntityType.LIGHTNING_BOLT, world);
            bolt.setPosition(Vec3d.ofCenter(pos));
            world.spawnEntity(bolt);
        }
    }
}
