package ml.darubyminer360.glacialabyss.util;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.item.ModItems;
import net.fabricmc.fabric.api.object.builder.v1.client.model.FabricModelPredicateProviderRegistry;
import net.minecraft.item.Item;
import net.minecraft.util.Identifier;

public class ModModelPredicateProvider {
    public static void registerModModels() {
        registerBow(ModItems.ABYSSAL_BOW);
        registerBow(ModItems.CHARGED_ABYSSAL_BOW);

        registerKey(ModItems.WOODEN_KEY);
        registerKey(ModItems.STONE_KEY);
        registerKey(ModItems.IRON_KEY);
        registerKey(ModItems.GOLD_KEY);
        registerKey(ModItems.DIAMOND_KEY);
        registerKey(ModItems.EMERALD_KEY);
        registerKey(ModItems.ABYSSAL_KEY);
        registerKey(ModItems.CRYOGENIC_KEY);
        registerKey(ModItems.PARADISE_KEY);
    }

    private static void registerBow(Item bow) {
        FabricModelPredicateProviderRegistry.register(bow, new Identifier("pull"),
                (stack, world, entity, seed) -> {
                    if (entity == null) {
                        return 0.0f;
                    }
                    if (entity.getActiveItem() != stack) {
                        return 0.0f;
                    }
                    return (float)(stack.getMaxUseTime() - entity.getItemUseTimeLeft()) / 20.0f;
                });

        FabricModelPredicateProviderRegistry.register(bow, new Identifier("pulling"),
                (stack, world, entity, seed) -> entity != null && entity.isUsingItem()
                        && entity.getActiveItem() == stack ? 1.0f : 0.0f);
    }

    private static void registerKey(Item key) {
        FabricModelPredicateProviderRegistry.register(key, new Identifier("shape"),
                (stack, world, entity, seed) -> {
                    if (entity == null) {
                        return 0.0f;
                    }
                    if (!stack.hasNbt()) {
                        return 0.0f;
                    }

                    if (KeyShape.valueOf(stack.getNbt().getString(GlacialAbyss.MOD_ID + ".shape")) == KeyShape.KEY) {
                        return 0.0f;
                    }
                    else if (KeyShape.valueOf(stack.getNbt().getString(GlacialAbyss.MOD_ID + ".shape")) == KeyShape.CRYSTAL) {
                        return 1.0f;
                    }
                    return 0.0f;
                });
    }
}
