package ml.darubyminer360.glacialabyss.util;

import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;

import java.util.List;

public interface IModUsable {
    public IModUsable setTooltip(List<Text> lines);
    public IModUsable addTooltip(Text line);
    public List<Text> getTooltip();
    public boolean hasGlint();
    public boolean hasGlint(ItemStack stack);
    public IModUsable setHasGlint(boolean value);
}
