package ml.darubyminer360.glacialabyss.world;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.World;
import net.minecraft.world.dimension.DimensionOptions;

public class ModDimensions {
    public static final Identifier GLACIAL_ABYSS_ID = new Identifier(GlacialAbyss.MOD_ID, "glacial_abyss");
    public static final RegistryKey<World> GLACIAL_ABYSS_KEY = RegistryKey.of(Registry.WORLD_KEY, GLACIAL_ABYSS_ID);
    public static final RegistryKey<DimensionOptions> GLACIAL_ABYSS_OPTIONS = RegistryKey.of(Registry.DIMENSION_KEY, GLACIAL_ABYSS_ID);

    public static void register() {
        GlacialAbyss.LOGGER.info("Registering Mod Dimensions for " + GlacialAbyss.MOD_ID);
    }
}
