package ml.darubyminer360.glacialabyss.world;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;
import net.minecraft.util.registry.RegistryKey;
import net.minecraft.world.biome.Biome;

public class ModBiomes {
    public static final Identifier ABYSSAL_PLAINS_ID = new Identifier(GlacialAbyss.MOD_ID, "abyssal_plains");
    public static final RegistryKey<Biome> ABYSSAL_PLAINS_KEY = RegistryKey.of(Registry.BIOME_KEY, ABYSSAL_PLAINS_ID);

    public static final Identifier FROSTED_FOREST_ID = new Identifier(GlacialAbyss.MOD_ID, "frosted_forest");
    public static final RegistryKey<Biome> FROSTED_FOREST_KEY = RegistryKey.of(Registry.BIOME_KEY, FROSTED_FOREST_ID);

    public static final Identifier HAUNTED_FOREST_ID = new Identifier(GlacialAbyss.MOD_ID, "haunted_forest");
    public static final RegistryKey<Biome> HAUNTED_FOREST_KEY = RegistryKey.of(Registry.BIOME_KEY, HAUNTED_FOREST_ID);

    public static void register() {
        GlacialAbyss.LOGGER.info("Registering Mod Biomes for " + GlacialAbyss.MOD_ID);
    }
}
