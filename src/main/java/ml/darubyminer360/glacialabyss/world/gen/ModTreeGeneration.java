package ml.darubyminer360.glacialabyss.world.gen;

import ml.darubyminer360.glacialabyss.world.ModBiomes;
import ml.darubyminer360.glacialabyss.world.feature.ModPlacedFeatures;
import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.minecraft.world.gen.GenerationStep;

public class ModTreeGeneration {
    public static void generateTrees() {
//        BiomeSelectors.categories(Biome.Category.PLAINS)
        BiomeModifications.addFeature(BiomeSelectors.includeByKey(ModBiomes.FROSTED_FOREST_KEY), GenerationStep.Feature.VEGETAL_DECORATION, ModPlacedFeatures.FROSTED_PLACED.getKey().get());
        BiomeModifications.addFeature(BiomeSelectors.includeByKey(ModBiomes.HAUNTED_FOREST_KEY), GenerationStep.Feature.VEGETAL_DECORATION, ModPlacedFeatures.HAUNTED_PLACED.getKey().get());
    }
}
