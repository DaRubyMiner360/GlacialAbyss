package ml.darubyminer360.glacialabyss.world.gen;

import ml.darubyminer360.glacialabyss.util.ModBiomeSelectors;
import ml.darubyminer360.glacialabyss.world.feature.ModPlacedFeatures;
import net.fabricmc.fabric.api.biome.v1.BiomeModifications;
import net.fabricmc.fabric.api.biome.v1.BiomeSelectors;
import net.minecraft.world.gen.GenerationStep;

public class ModOreGeneration {
    public static void generateOres() {
        BiomeModifications.addFeature(BiomeSelectors.foundInOverworld(), GenerationStep.Feature.UNDERGROUND_ORES, ModPlacedFeatures.UNSTABLE_ABYSS_ORE_PLACED.getKey().get());

        BiomeModifications.addFeature(ModBiomeSelectors.foundInTheGlacialAbyss(), GenerationStep.Feature.UNDERGROUND_ORES, ModPlacedFeatures.ABYSSAL_ORE_PLACED.getKey().get());
    }
}
