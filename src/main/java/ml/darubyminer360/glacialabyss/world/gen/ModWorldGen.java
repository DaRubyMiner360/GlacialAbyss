package ml.darubyminer360.glacialabyss.world.gen;

public class ModWorldGen {
    public static void generateModWorldGen() {
        ModOreGeneration.generateOres();

        ModFlowerGeneration.generateFlowers();
        ModTreeGeneration.generateTrees();
    }
}
