package ml.darubyminer360.glacialabyss.world.feature;

import net.minecraft.util.registry.RegistryEntry;
import net.minecraft.world.gen.YOffset;
import net.minecraft.world.gen.feature.PlacedFeature;
import net.minecraft.world.gen.feature.PlacedFeatures;
import net.minecraft.world.gen.feature.VegetationPlacedFeatures;
import net.minecraft.world.gen.placementmodifier.BiomePlacementModifier;
import net.minecraft.world.gen.placementmodifier.HeightRangePlacementModifier;
import net.minecraft.world.gen.placementmodifier.RarityFilterPlacementModifier;
import net.minecraft.world.gen.placementmodifier.SquarePlacementModifier;

public class ModPlacedFeatures {
    public static final RegistryEntry<PlacedFeature> FROSTED_PLACED = PlacedFeatures.register("frosted_placed", ModConfiguredFeatures.FROSTED_SPAWN, VegetationPlacedFeatures.modifiers(PlacedFeatures.createCountExtraModifier(1, 0.1f, 2)));
    public static final RegistryEntry<PlacedFeature> HAUNTED_PLACED = PlacedFeatures.register("haunted_placed", ModConfiguredFeatures.HAUNTED_SPAWN, VegetationPlacedFeatures.modifiers(PlacedFeatures.createCountExtraModifier(1, 0.1f, 2)));

    public static final RegistryEntry<PlacedFeature> FROSTED_FLOWER_PLACED = PlacedFeatures.register("frosted_flower_placed", ModConfiguredFeatures.FROSTED_FLOWER, RarityFilterPlacementModifier.of(4), SquarePlacementModifier.of(), PlacedFeatures.MOTION_BLOCKING_HEIGHTMAP, BiomePlacementModifier.of());

    public static final RegistryEntry<PlacedFeature> UNSTABLE_ABYSS_ORE_PLACED = PlacedFeatures.register("unstable_abyss_ore_placed", ModConfiguredFeatures.UNSTABLE_ABYSS_ORE, ModOreFeatures.modifiersWithCount(4, HeightRangePlacementModifier.trapezoid(YOffset.aboveBottom(-80), YOffset.aboveBottom(80))));

    public static final RegistryEntry<PlacedFeature> ABYSSAL_ORE_PLACED = PlacedFeatures.register("abyssal_ore_placed", ModConfiguredFeatures.ABYSSAL_ORE, ModOreFeatures.modifiersWithCount(3, HeightRangePlacementModifier.uniform(YOffset.aboveBottom(-80), YOffset.aboveBottom(80))));
}
