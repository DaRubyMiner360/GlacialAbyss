package ml.darubyminer360.glacialabyss.world.feature;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.block.ModBlocks;
import ml.darubyminer360.glacialabyss.util.ModTags;
import net.minecraft.structure.rule.RuleTest;
import net.minecraft.structure.rule.TagMatchRuleTest;
import net.minecraft.util.math.intprovider.ConstantIntProvider;
import net.minecraft.util.registry.RegistryEntry;
import net.minecraft.world.gen.feature.*;
import net.minecraft.world.gen.feature.size.TwoLayersFeatureSize;
import net.minecraft.world.gen.foliage.BlobFoliagePlacer;
import net.minecraft.world.gen.stateprovider.BlockStateProvider;
import net.minecraft.world.gen.trunk.ForkingTrunkPlacer;
import net.minecraft.world.gen.trunk.StraightTrunkPlacer;

import java.util.List;

public class ModConfiguredFeatures {
    public static final RuleTest BASE_STONE_GLACIAL_ABYSS = new TagMatchRuleTest(ModTags.Blocks.BASE_STONE_GLACIAL_ABYSS);

    public static final RegistryEntry<ConfiguredFeature<TreeFeatureConfig, ?>> FROSTED_TREE =
            ConfiguredFeatures.register("frosted_tree", Feature.TREE, new TreeFeatureConfig.Builder(
                    BlockStateProvider.of(ModBlocks.FROSTED_LOG),
                    new StraightTrunkPlacer(5, 6, 3),
                    BlockStateProvider.of(ModBlocks.FROSTED_LEAVES),
                    new BlobFoliagePlacer(ConstantIntProvider.create(2), ConstantIntProvider.create(0), 4),
                    new TwoLayersFeatureSize(1, 0, 2))
                    .ignoreVines()
                    .build());
    public static final RegistryEntry<ConfiguredFeature<TreeFeatureConfig, ?>> HAUNTED_TREE =
            ConfiguredFeatures.register("haunted_tree", Feature.TREE, new TreeFeatureConfig.Builder(
                    BlockStateProvider.of(ModBlocks.HAUNTED_LOG),
                    new ForkingTrunkPlacer(4, 4, 3),
                    BlockStateProvider.of(ModBlocks.HAUNTED_LEAVES),
                    new BlobFoliagePlacer(ConstantIntProvider.create(2), ConstantIntProvider.create(0), 5),
                    new TwoLayersFeatureSize(1, 0, 3))
                    .ignoreVines()
                    .build());

    public static final RegistryEntry<PlacedFeature> FROSTED_CHECKED =
            PlacedFeatures.register("frosted_checked", FROSTED_TREE,
                    PlacedFeatures.wouldSurvive(ModBlocks.FROSTED_SAPLING));
    public static final RegistryEntry<PlacedFeature> HAUNTED_CHECKED =
            PlacedFeatures.register("haunted_checked", HAUNTED_TREE,
                    PlacedFeatures.wouldSurvive(ModBlocks.HAUNTED_SAPLING));

    public static final RegistryEntry<ConfiguredFeature<RandomFeatureConfig, ?>> FROSTED_SPAWN =
            ConfiguredFeatures.register("frosted_spawn", Feature.RANDOM_SELECTOR,
                    new RandomFeatureConfig(List.of(new RandomFeatureEntry(FROSTED_CHECKED, 0.5f)),
                            FROSTED_CHECKED));
    public static final RegistryEntry<ConfiguredFeature<RandomFeatureConfig, ?>> HAUNTED_SPAWN =
            ConfiguredFeatures.register("haunted_spawn", Feature.RANDOM_SELECTOR,
                    new RandomFeatureConfig(List.of(new RandomFeatureEntry(HAUNTED_CHECKED, 0.5f)),
                            HAUNTED_CHECKED));


    public static final RegistryEntry<ConfiguredFeature<RandomPatchFeatureConfig, ?>> FROSTED_FLOWER =
            ConfiguredFeatures.register("frosted_flower", Feature.FLOWER,
                    ConfiguredFeatures.createRandomPatchFeatureConfig(64, PlacedFeatures.createEntry(Feature.SIMPLE_BLOCK,
                            new SimpleBlockFeatureConfig(BlockStateProvider.of(ModBlocks.FROSTED_FLOWER)))));


    public static final List<OreFeatureConfig.Target> OVERWORLD_UNSTABLE_ABYSS_ORES = List.of(
            OreFeatureConfig.createTarget(OreConfiguredFeatures.STONE_ORE_REPLACEABLES,
                    ModBlocks.UNSTABLE_ABYSS_ORE.getDefaultState()),
            OreFeatureConfig.createTarget(OreConfiguredFeatures.DEEPSLATE_ORE_REPLACEABLES,
                    ModBlocks.DEEPSLATE_UNSTABLE_ABYSS_ORE.getDefaultState()));

    public static final List<OreFeatureConfig.Target> ABYSSAL_ORES = List.of(
            OreFeatureConfig.createTarget(BASE_STONE_GLACIAL_ABYSS,
                    ModBlocks.ABYSSAL_ORE.getDefaultState()));

    public static final RegistryEntry<ConfiguredFeature<OreFeatureConfig, ?>> UNSTABLE_ABYSS_ORE =
            ConfiguredFeatures.register("unstable_abyss_ore", Feature.ORE,
                    new OreFeatureConfig(OVERWORLD_UNSTABLE_ABYSS_ORES, 4));

    public static final RegistryEntry<ConfiguredFeature<OreFeatureConfig, ?>> ABYSSAL_ORE =
            ConfiguredFeatures.register("abyssal_ore", Feature.ORE,
                    new OreFeatureConfig(ABYSSAL_ORES, 3));

    public static void register() {
        System.out.println("Registering ModConfiguredFeatures for " + GlacialAbyss.MOD_ID);
    }
}
