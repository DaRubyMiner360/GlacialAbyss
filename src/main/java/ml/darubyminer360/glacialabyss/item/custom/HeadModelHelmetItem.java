package ml.darubyminer360.glacialabyss.item.custom;

import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.ArmorMaterial;

public class HeadModelHelmetItem extends ModArmorItem {
    public HeadModelHelmetItem(ArmorMaterial material, EquipmentSlot slot, Settings settings) {
        super(material, slot, settings);
    }
}
