package ml.darubyminer360.glacialabyss.item.custom;

public class KeyItem extends ModItem {
    private int tier;
    public int getTier() { return tier; }

    public KeyItem(Settings settings, int tier) {
        super(settings);
        this.tier = tier;
    }
}
