package ml.darubyminer360.glacialabyss.item.custom;

import ml.darubyminer360.glacialabyss.util.IModUsable;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.MusicDiscItem;
import net.minecraft.sound.SoundEvent;
import net.minecraft.text.Text;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ModMusicDiscItem extends MusicDiscItem implements IModUsable {
    private boolean hasGlint = false;
    private List<Text> tooltip = new ArrayList<>();

    public ModMusicDiscItem(int comparatorOutput, SoundEvent sound, Settings settings) {
        super(comparatorOutput, sound, settings);
    }

    @Override
    public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
        super.appendTooltip(stack, world, tooltip, context);
        tooltip.addAll(this.tooltip);
    }

    @Override
    public ModMusicDiscItem setTooltip(List<Text> lines) {
        this.tooltip = lines;
        return this;
    }

    @Override
    public ModMusicDiscItem addTooltip(Text line) {
        this.tooltip.add(line);
        return this;
    }

    @Override
    public List<Text> getTooltip() {
        return this.tooltip;
    }

    @Override
    public boolean hasGlint() {
        return this.hasGlint;
    }

    @Override
    public boolean hasGlint(ItemStack stack) {
        if (this.hasGlint()) {
            return true;
        }
        return super.hasGlint(stack);
    }

    @Override
    public ModMusicDiscItem setHasGlint(boolean value) {
        this.hasGlint = value;
        return this;
    }
}
