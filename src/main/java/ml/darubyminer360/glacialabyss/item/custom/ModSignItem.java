package ml.darubyminer360.glacialabyss.item.custom;

import ml.darubyminer360.glacialabyss.util.IModUsable;
import net.minecraft.block.Block;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.item.SignItem;
import net.minecraft.item.ToolMaterial;
import net.minecraft.text.Text;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ModSignItem extends SignItem implements IModUsable {
    private boolean hasGlint = false;
    private List<Text> tooltip = new ArrayList<>();

    public ModSignItem(Settings settings, Block standingBlock, Block wallBlock) {
        super(settings, standingBlock, wallBlock);
    }

    @Override
    public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
        super.appendTooltip(stack, world, tooltip, context);
        tooltip.addAll(this.tooltip);
    }

    @Override
    public ModSignItem setTooltip(List<Text> lines) {
        this.tooltip = lines;
        return this;
    }

    @Override
    public ModSignItem addTooltip(Text line) {
        this.tooltip.add(line);
        return this;
    }

    @Override
    public List<Text> getTooltip() {
        return this.tooltip;
    }

    @Override
    public boolean hasGlint() {
        return this.hasGlint;
    }

    @Override
    public boolean hasGlint(ItemStack stack) {
        if (this.hasGlint()) {
            return true;
        }
        return super.hasGlint(stack);
    }

    @Override
    public ModSignItem setHasGlint(boolean value) {
        this.hasGlint = value;
        return this;
    }
}
