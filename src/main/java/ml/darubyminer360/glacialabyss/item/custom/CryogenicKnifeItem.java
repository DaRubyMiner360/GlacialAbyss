package ml.darubyminer360.glacialabyss.item.custom;

import ml.darubyminer360.glacialabyss.effect.ModEffects;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolMaterial;

import java.util.Random;

public class CryogenicKnifeItem extends ModSwordItem {
    public CryogenicKnifeItem(ToolMaterial toolMaterial, int attackDamage, float attackSpeed, Settings settings) {
        super(toolMaterial, attackDamage, attackSpeed, settings);
    }

    @Override
    public boolean postHit(ItemStack stack, LivingEntity target, LivingEntity attacker) {
        int rand = new Random().nextInt(17);

        if (attacker instanceof PlayerEntity) {
            target.damage(DamageSource.player((PlayerEntity) attacker), rand);
        }
        else {
            target.damage(DamageSource.mob(attacker), rand);
        }
        target.addStatusEffect(new StatusEffectInstance(ModEffects.FROST, 120, 1));

        return super.postHit(stack, target, attacker);
    }
}
