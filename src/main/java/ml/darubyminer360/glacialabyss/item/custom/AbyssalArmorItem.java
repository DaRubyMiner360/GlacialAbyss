package ml.darubyminer360.glacialabyss.item.custom;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.ItemStack;
import net.minecraft.world.World;
import org.apache.commons.compress.utils.Lists;

import java.util.List;

public class AbyssalArmorItem extends ModArmorItem {
    public AbyssalArmorItem(ArmorMaterial material, EquipmentSlot slot, Settings settings) {
        super(material, slot, settings);
    }

    @Override
    public void inventoryTick(ItemStack stack, World world, Entity entity, int slot, boolean selected) {
        if (!world.isClient()) {
            if (entity instanceof LivingEntity) {
                int level = GlacialAbyss.CONFIG.abyssal_armor_effect_strength;
                int duration = 210;
                LivingEntity livingEntity = (LivingEntity) entity;
                List<ItemStack> armorItems = Lists.newArrayList(livingEntity.getArmorItems().iterator());

                if (this.slot == EquipmentSlot.HEAD && slot == 3 && armorItems.contains(stack)) {
                    livingEntity.addStatusEffect(new StatusEffectInstance(StatusEffects.SATURATION, duration, level, true, false, false));
                }
                else if (this.slot == EquipmentSlot.CHEST && slot == 2 && armorItems.contains(stack)) {
                    livingEntity.addStatusEffect(new StatusEffectInstance(StatusEffects.ABSORPTION, duration, level, true, false, false));
                    livingEntity.addStatusEffect(new StatusEffectInstance(StatusEffects.REGENERATION, duration, level, true, false, false));
                }
                else if (this.slot == EquipmentSlot.LEGS && slot == 1 && armorItems.contains(stack)) {
                    livingEntity.addStatusEffect(new StatusEffectInstance(StatusEffects.HASTE, duration, level, true, false, false));
                }
                else if (this.slot == EquipmentSlot.FEET && slot == 0 && armorItems.contains(stack)) {
                    livingEntity.addStatusEffect(new StatusEffectInstance(StatusEffects.SPEED, duration, level, true, false, false));
                }
            }
        }

        super.inventoryTick(stack, world, entity, slot, selected);
    }
}
