package ml.darubyminer360.glacialabyss.item.custom;

//import com.github.crimsondawn45.fabricshieldlib.lib.object.FabricBannerShieldItem;
//import com.github.crimsondawn45.fabricshieldlib.lib.object.FabricShieldItem;
import ml.darubyminer360.glacialabyss.util.IModUsable;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.ToolMaterial;
import net.minecraft.tag.TagKey;
import net.minecraft.text.Text;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/*public class ModShieldItem extends FabricBannerShieldItem implements IModUsable {
    private boolean hasGlint = false;
    private List<Text> tooltip = new ArrayList<>();

    public ModShieldItem(Settings settings, int cooldownTicks, int enchantability, Item... repairItems) {
        super(settings, cooldownTicks, enchantability, repairItems);
    }

    public ModShieldItem(Settings settings, int cooldownTicks, ToolMaterial material) {
        super(settings, cooldownTicks, material);
    }

    public ModShieldItem(Settings settings, int cooldownTicks, int enchantability, TagKey<Item> repairItemTag) {
        super(settings, cooldownTicks, enchantability, repairItemTag);
    }

    public ModShieldItem(Settings settings, int cooldownTicks, int enchantability, Collection<TagKey<Item>> repairItemTags) {
        super(settings, cooldownTicks, enchantability, repairItemTags);
    }

    @Override
    public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
        super.appendTooltip(stack, world, tooltip, context);
        tooltip.addAll(this.tooltip);
    }

    @Override
    public ModShieldItem setTooltip(List<Text> lines) {
        this.tooltip = lines;
        return this;
    }

    @Override
    public ModShieldItem addTooltip(Text line) {
        this.tooltip.add(line);
        return this;
    }

    @Override
    public List<Text> getTooltip() {
        return this.tooltip;
    }

    @Override
    public boolean hasGlint() {
        return this.hasGlint;
    }

    @Override
    public boolean hasGlint(ItemStack stack) {
        if (this.hasGlint()) {
            return true;
        }
        return super.hasGlint(stack);
    }

    @Override
    public ModShieldItem setHasGlint(boolean value) {
        this.hasGlint = value;
        return this;
    }
}*/
