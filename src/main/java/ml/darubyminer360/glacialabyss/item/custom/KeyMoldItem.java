package ml.darubyminer360.glacialabyss.item.custom;

public class KeyMoldItem extends ModItem {
    private int tier;
    public int getTier() { return tier; }

    public KeyMoldItem(Settings settings, int tier) {
        super(settings);
        this.tier = tier;
    }
}
