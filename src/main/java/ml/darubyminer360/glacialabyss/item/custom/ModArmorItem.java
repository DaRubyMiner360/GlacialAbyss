package ml.darubyminer360.glacialabyss.item.custom;

import com.google.common.collect.ImmutableMap;
import ml.darubyminer360.glacialabyss.item.ModArmorMaterials;
import ml.darubyminer360.glacialabyss.util.IModUsable;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ArmorItem;
import net.minecraft.item.ArmorMaterial;
import net.minecraft.item.ArmorMaterials;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class ModArmorItem extends ArmorItem implements IModUsable {
    private boolean hasGlint = false;
    private List<Text> tooltip = new ArrayList<>();

    public List<UUID> previouslyEquippedPlayers = new ArrayList<>();

    protected static final Map<ArmorMaterial, StatusEffectInstance> MATERIAL_TO_EFFECT_MAP = new ImmutableMap.Builder<ArmorMaterial, StatusEffectInstance>()
//            .put(ModArmorMaterials.ABYSSAL, new StatusEffectInstance(StatusEffects.LUCK, 210, 1))
            .build();
    protected static final List<ArmorMaterial> FLIGHT_MATERIALS = Arrays.asList(
            ModArmorMaterials.ABYSSAL,
            ModArmorMaterials.CHARGED_ABYSSAL
    );

    public ModArmorItem(ArmorMaterial material, EquipmentSlot slot, Settings settings) {
        super(material, slot, settings);
    }

    @Override
    public void inventoryTick(ItemStack stack, World world, Entity entity, int slot, boolean selected) {
        if (!world.isClient()) {
            if (entity instanceof PlayerEntity) {
                PlayerEntity player = (PlayerEntity) entity;

                evaluateArmorEffects(player);

                if (hasFullSuitOfArmorOn(player)) {
                    if (!previouslyEquippedPlayers.contains(player.getUuid())) {
                        previouslyEquippedPlayers.add(player.getUuid());
                    }
                }
                else if (previouslyEquippedPlayers.contains(player.getUuid())) {
                    previouslyEquippedPlayers.remove(player.getUuid());
                }
            }
        }

        super.inventoryTick(stack, world, entity, slot, selected);
    }

    protected void evaluateArmorEffects(PlayerEntity player) {
        if (hasFullSuitOfArmorOn(player)) {
            for (Map.Entry<ArmorMaterial, StatusEffectInstance> entry : MATERIAL_TO_EFFECT_MAP.entrySet()) {
                ArmorMaterial mapArmorMaterial = entry.getKey();
                StatusEffectInstance mapStatusEffect = entry.getValue();

                if (hasCorrectArmorOn(mapArmorMaterial, player)) {
                    addStatusEffectForMaterial(player, mapArmorMaterial, mapStatusEffect);
                }
            }
            for (ArmorMaterial material : FLIGHT_MATERIALS) {
                if (hasCorrectArmorOn(material, player)) {
                    player.getAbilities().allowFlying = true;
                }
            }
        }
        else if (!player.isCreative() && !player.isSpectator()) {
            boolean stopFlight = false;
            for (ArmorMaterial material : FLIGHT_MATERIALS) {
                if (previouslyEquippedPlayers.contains(player.getUuid())) {
                    stopFlight = true;
                }
            }
            if (stopFlight) {
                player.getAbilities().allowFlying = false;
                player.getAbilities().flying = player.getAbilities().allowFlying;
            }
        }
        player.sendAbilitiesUpdate();
    }

    protected void addStatusEffectForMaterial(PlayerEntity player, ArmorMaterial mapArmorMaterial, StatusEffectInstance mapStatusEffect) {
        boolean hasPlayerEffect = player.hasStatusEffect(mapStatusEffect.getEffectType());

        if (hasCorrectArmorOn(mapArmorMaterial, player) && !hasPlayerEffect) {
            player.addStatusEffect(new StatusEffectInstance(mapStatusEffect.getEffectType(),
                    mapStatusEffect.getDuration(), mapStatusEffect.getAmplifier()));

            // if (new Random().nextFloat() > 0.6f) { // 40% of damaging the armor! Possibly!
            //     player.getInventory().damageArmor(DamageSource.MAGIC, 1f, new int[]{0, 1, 2, 3});
            // }
        }
    }

    protected boolean hasFullSuitOfArmorOn(PlayerEntity player) {
        ItemStack boots = player.getInventory().getArmorStack(0);
        ItemStack leggings = player.getInventory().getArmorStack(1);
        ItemStack breastplate = player.getInventory().getArmorStack(2);
        ItemStack helmet = player.getInventory().getArmorStack(3);

        return !helmet.isEmpty() && !breastplate.isEmpty() && !leggings.isEmpty() && !boots.isEmpty();
    }

    protected boolean hasCorrectArmorOn(ArmorMaterial material, PlayerEntity player) {
        ArmorItem boots = ((ArmorItem)player.getInventory().getArmorStack(0).getItem());
        ArmorItem leggings = ((ArmorItem)player.getInventory().getArmorStack(1).getItem());
        ArmorItem breastplate = ((ArmorItem)player.getInventory().getArmorStack(2).getItem());
        ArmorItem helmet = ((ArmorItem)player.getInventory().getArmorStack(3).getItem());

        return helmet.getMaterial() == material && breastplate.getMaterial() == material && leggings.getMaterial() == material && boots.getMaterial() == material;
    }

    @Override
    public void appendTooltip(ItemStack stack, @Nullable World world, List<Text> tooltip, TooltipContext context) {
        super.appendTooltip(stack, world, tooltip, context);
        tooltip.addAll(this.tooltip);
    }

    @Override
    public ModArmorItem setTooltip(List<Text> lines) {
        this.tooltip = lines;
        return this;
    }

    @Override
    public ModArmorItem addTooltip(Text line) {
        this.tooltip.add(line);
        return this;
    }

    @Override
    public List<Text> getTooltip() {
        return this.tooltip;
    }

    @Override
    public boolean hasGlint() {
        return this.hasGlint;
    }

    @Override
    public boolean hasGlint(ItemStack stack) {
        if (this.hasGlint()) {
            return true;
        }
        return super.hasGlint(stack);
    }

    @Override
    public ModArmorItem setHasGlint(boolean value) {
        this.hasGlint = value;
        return this;
    }
}
