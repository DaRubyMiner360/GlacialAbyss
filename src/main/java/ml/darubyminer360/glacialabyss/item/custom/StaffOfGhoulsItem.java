package ml.darubyminer360.glacialabyss.item.custom;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.effect.ModEffects;
import ml.darubyminer360.glacialabyss.enchantment.ModEnchantments;
import ml.darubyminer360.glacialabyss.entity.ModDamageSource;
import ml.darubyminer360.glacialabyss.item.ModItems;
import ml.darubyminer360.glacialabyss.particle.ModParticles;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.enchantment.Enchantments;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.*;
import net.minecraft.recipe.Ingredient;
import net.minecraft.sound.SoundCategory;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.Direction;
import net.minecraft.util.math.Direction;
import net.minecraft.world.World;

import java.util.List;

public class StaffOfGhoulsItem extends ModItem {
    private final int radius;
    private final float damage;

    public StaffOfGhoulsItem(Settings settings) {
        this(settings, 4);
    }
    
    public StaffOfGhoulsItem(Settings settings, int radius) {
        this(settings, radius, 1.0f);
    }
    
    public StaffOfGhoulsItem(Settings settings, int radius, float damage) {
        super(settings);
        this.radius = radius;
        this.damage = damage;
    }

    @Override
    public boolean canRepair(ItemStack stack, ItemStack ingredient) {
        return Ingredient.ofItems(ModItems.PHANTOM_HEART).test(ingredient);
    }

    @Override
    public boolean isEnchantable(ItemStack stack) {
        return true;
    }

    @Override
    public int getEnchantability() {
        // TODO: Tweak
        return 100;
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        BlockPos pos = user.getBlockPos().add(0.5d, 0d,0.5d);
        ItemStack stack = user.getStackInHand(hand);

        attack(world, user, hand, pos, stack);
        return TypedActionResult.success(stack);
    }

    @Override
    public ActionResult useOnEntity(ItemStack stack, PlayerEntity user, LivingEntity entity, Hand hand) {
        World world = entity.getWorld();
        BlockPos pos = entity.getBlockPos().add(0.5d, 0d,0.5d);

        attack(world, user, hand, pos, stack);
        return ActionResult.SUCCESS;
    }

    @Override
    public ActionResult useOnBlock(ItemUsageContext context) {
        World world = context.getWorld();
        PlayerEntity user = context.getPlayer();
        Hand hand = context.getHand();
        BlockPos pos = context.getBlockPos();
        ItemStack stack = context.getStack();

        if (user != null && world.getBlockState(pos).getBlock().asItem() != Items.AIR) {
            attack(world, user, hand, pos, stack);
            return ActionResult.SUCCESS;
        }

        return ActionResult.FAIL;
    }

    private void attack(World world, PlayerEntity user, Hand hand, BlockPos pos, ItemStack stack) {
        int radius = getRadius(stack);
        float damage = getDamage(stack);

        for (int n = 0; n < radius; n++) {
            for (int i = 0; i < 360; i++) {
                double x = pos.getX() + Math.sin(i) * radius;
                double y = pos.getY();
                double z = pos.getZ() + Math.cos(i) * radius;
                world.addParticle(ModParticles.FROST, x, y + 1, z, 0, 0, 0);
            }
        }

        List<LivingEntity> entities = world.getEntitiesByClass(LivingEntity.class, new Box(pos.add(-radius, -radius, -radius), pos.add(radius, radius, radius)), (e) -> e != user);
        for (LivingEntity e : entities) {
            e.damage(ModDamageSource.FREEZE, damage);
            e.addStatusEffect(new StatusEffectInstance(ModEffects.FROST, 200, 1));
        }
    }

    public int getBaseRadius() {
        return radius;
    }
    
    public int getRadius() {
        return getBaseRadius();
    }
    
    public int getRadius(ItemStack stack) {
        return getRadius(EnchantmentHelper.getLevel(ModEnchantments.GHOUL_CHASER, stack));
    }

    public int getRadius(int enchantmentLevel) {
        return getBaseRadius() * (enchantmentLevel + 1);
    }

    public float getBaseDamage() {
        return damage;
    }
    
    public float getDamage() {
        return getBaseDamage();
    }
    
    public float getDamage(ItemStack stack) {
        return getDamage(EnchantmentHelper.getLevel(ModEnchantments.CALL_OF_THE_GHOULS, stack));
    }

    public float getDamage(int enchantmentLevel) {
        return getBaseDamage() * (enchantmentLevel + 1);
    }
}
