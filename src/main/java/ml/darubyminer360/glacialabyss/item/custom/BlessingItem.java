package ml.darubyminer360.glacialabyss.item.custom;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.sound.ModSounds;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.TypedActionResult;
import net.minecraft.world.World;

public class BlessingItem extends ModItem {
    private long duration; // In seconds
    private long cooldown; // In seconds
    private SoundEvent sound;

    public BlessingItem(Settings settings) {
        this(settings, 10);
    }

    public BlessingItem(Settings settings, long duration) {
        this(settings, duration, 60);
    }

    public BlessingItem(Settings settings, long duration, long cooldown) {
        this(settings, duration, cooldown, null);
    }

    public BlessingItem(Settings settings, long duration, long cooldown, SoundEvent sound) {
        super(settings);
        this.duration = duration;
        this.cooldown = cooldown;
        this.sound = sound;
    }

    @Override
    public TypedActionResult<ItemStack> use(World world, PlayerEntity user, Hand hand) {
        ItemStack stack = user.getStackInHand(hand);
        BlessingItem item = (BlessingItem) stack.getItem();

        if (stack.hasNbt() && stack.getNbt().contains(GlacialAbyss.MOD_ID + ".on_cooldown") && stack.getNbt().getBoolean(GlacialAbyss.MOD_ID + ".on_cooldown")) {
            user.sendMessage(stack.getName().copy().append(new TranslatableText("message.glacialabyss.on_cooldown")).formatted(Formatting.RED), true);
            return super.use(world, user, hand);
        }

        if (sound != null) {
            user.playSound(sound, SoundCategory.PLAYERS, 1f, 1f);
        }

        new Thread(() -> {
            try {
                NbtCompound nbt = stack.getOrCreateNbt();
                nbt.putBoolean(GlacialAbyss.MOD_ID + ".on_cooldown", true);
                stack.writeNbt(nbt);

                GlacialAbyss.blessedEntities.add(user.getUuid());
                GlacialAbyss.blessedCooldownEntities.add(user.getUuid());
                Thread.sleep(item.duration * 1000);

                GlacialAbyss.blessedEntities.remove(user.getUuid());

                long cooldownLeft = item.cooldown - item.duration;
                if (cooldownLeft > 0) {
                    Thread.sleep(cooldownLeft * 1000);
                }

                nbt = stack.getOrCreateNbt();
                if (nbt.contains(GlacialAbyss.MOD_ID + ".on_cooldown")) {
                    nbt.remove(GlacialAbyss.MOD_ID + ".on_cooldown");
                    stack.writeNbt(nbt);
                }
                GlacialAbyss.blessedEntities.remove(user.getUuid());
            } catch (InterruptedException ignored) {}
        }).start();

        return TypedActionResult.success(stack);
    }

    public void inventoryTick(ItemStack stack, World world, Entity entity, int slot, boolean selected) {
        if (stack.hasNbt() && stack.getNbt().contains(GlacialAbyss.MOD_ID + ".on_cooldown") && !GlacialAbyss.blessedCooldownEntities.contains(entity.getUuid())) {
            NbtCompound nbt = stack.getNbt();
            nbt.remove(GlacialAbyss.MOD_ID + ".cooldown");
            stack.writeNbt(nbt);
        }

        super.inventoryTick(stack, world, entity, slot, selected);
    }
}
