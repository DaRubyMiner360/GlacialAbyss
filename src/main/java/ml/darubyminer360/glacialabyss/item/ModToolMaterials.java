package ml.darubyminer360.glacialabyss.item;

import net.minecraft.item.ToolMaterial;
import net.minecraft.recipe.Ingredient;
import net.minecraft.util.Lazy;

import java.util.function.Supplier;

public enum ModToolMaterials implements ToolMaterial {
    // TODO: Tweak
    CRYOGENIC(6, 6093, 13F, 4.0F, 16, () -> Ingredient.ofItems(ModItems.CHILLED_ESSENCE)),
    // TODO: Tweak
    HAUNTED(6, 10155, 15F, 8.0F, 16, () -> Ingredient.ofItems(ModItems.CHILLED_ESSENCE)),
    ABYSSAL(7, 15234, 30f, 26.0f, 18, () -> Ingredient.ofItems(ModItems.ABYSSAL_INGOT)),
    CHARGED_ABYSSAL(8, 16758, 35f, 63.0f, 20, () -> Ingredient.ofItems(ModItems.CHARGED_ABYSSAL_INGOT));

    private final int miningLevel;
    private final int itemDurability;
    private final float miningSpeed;
    private final float attackDamage;
    private final int enchantability;
    private final Lazy<Ingredient> repairIngredient;

    ModToolMaterials(int miningLevel, int itemDurability, float miningSpeed, float attackDamage, int enchantability, Supplier<Ingredient> repairIngredient) {
        this.miningLevel = miningLevel;
        this.itemDurability = itemDurability;
        this.miningSpeed = miningSpeed;
        this.attackDamage = attackDamage;
        this.enchantability = enchantability;
        this.repairIngredient = new Lazy<>(repairIngredient);
    }

    @Override
    public int getDurability() {
        return this.itemDurability;
    }

    @Override
    public float getMiningSpeedMultiplier() {
        return this.miningSpeed;
    }

    @Override
    public float getAttackDamage() {
        return this.attackDamage;
    }

    @Override
    public int getMiningLevel() {
        return this.miningLevel;
    }

    @Override
    public int getEnchantability() {
        return this.enchantability;
    }

    @Override
    public Ingredient getRepairIngredient() {
        return this.repairIngredient.get();
    }
}
