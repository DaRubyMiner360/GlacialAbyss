package ml.darubyminer360.glacialabyss.item;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.block.ModBlocks;
import ml.darubyminer360.glacialabyss.item.custom.*;
import ml.darubyminer360.glacialabyss.sound.ModSounds;
import ml.darubyminer360.glacialabyss.util.IModUsable;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.item.*;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.Identifier;
import net.minecraft.util.Rarity;
import net.minecraft.util.registry.Registry;

public class ModItems {
    public static final Item UNSTABLE_ABYSS = registerModItem("unstable_abyss", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).setHasGlint(true));
    public static final Item CHARGED_ABYSSAL_INGOT = registerModItem("charged_abyssal_ingot", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).setHasGlint(true));
    public static final Item CHARGED_ABYSSAL_NUGGET = registerModItem("charged_abyssal_nugget", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).setHasGlint(true));
    public static final Item ABYSSAL_INGOT = registerModItem("abyssal_ingot", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item ABYSSAL_NUGGET = registerModItem("abyssal_nugget", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item ABYSSAL_DUST = registerModItem("abyssal_dust", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).addTooltip(new TranslatableText("item.glacialabyss.abyssal_dust.tooltip").formatted(Formatting.GOLD)));
    public static final Item CHARGED_ABYSSAL_DUST = registerModItem("charged_abyssal_dust", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).addTooltip(new TranslatableText("item.glacialabyss.charged_abyssal_dust.tooltip").formatted(Formatting.GOLD)).setHasGlint(true));
    public static final Item RAW_ABYSS = registerModItem("raw_abyss", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item ABYSSAL_SWORD = registerModItem("abyssal_sword", new ModSwordItem(ModToolMaterials.ABYSSAL, 1, 2f, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item ABYSSAL_AXE = registerModItem("abyssal_axe", new ModAxeItem(ModToolMaterials.ABYSSAL, 3, 1f, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item ABYSSAL_HOE = registerModItem("abyssal_hoe", new ModHoeItem(ModToolMaterials.ABYSSAL, 0, 0f, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item ABYSSAL_SHOVEL = registerModItem("abyssal_shovel", new ModShovelItem(ModToolMaterials.ABYSSAL, 0, 1f, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item ABYSSAL_PICKAXE = registerModItem("abyssal_pickaxe", new ModPickaxeItem(ModToolMaterials.ABYSSAL, 1, 0f, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item ABYSSAL_BOW = registerModItem("abyssal_bow", new ModBowItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS).maxDamage(15234)));
    //    public static final Item ABYSSAL_SHIELD = registerModItem("abyssal_shield", new ModShieldItem(new FabricItemSettings().maxDamage(15234).group(ModItemGroup.GLACIAL_ABYSS), 16, 18, ModItems.ABYSSAL_INGOT, ModItems.CHARGED_ABYSSAL_INGOT));
    public static final Item ABYSSAL_HELMET = registerModItem("abyssal_helmet", new AbyssalArmorItem(ModArmorMaterials.ABYSSAL, EquipmentSlot.HEAD, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item ABYSSAL_CHESTPLATE = registerModItem("abyssal_chestplate", new AbyssalArmorItem(ModArmorMaterials.ABYSSAL, EquipmentSlot.CHEST, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item ABYSSAL_LEGGINGS = registerModItem("abyssal_leggings", new AbyssalArmorItem(ModArmorMaterials.ABYSSAL, EquipmentSlot.LEGS, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item ABYSSAL_BOOTS = registerModItem("abyssal_boots", new AbyssalArmorItem(ModArmorMaterials.ABYSSAL, EquipmentSlot.FEET, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item CHARGED_ABYSSAL_SWORD = registerModItem("charged_abyssal_sword", new ModSwordItem(ModToolMaterials.CHARGED_ABYSSAL, 1, 2f, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).setHasGlint(true));
    public static final Item CHARGED_ABYSSAL_AXE = registerModItem("charged_abyssal_axe", new ModAxeItem(ModToolMaterials.CHARGED_ABYSSAL, 3, 1f, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).setHasGlint(true));
    public static final Item CHARGED_ABYSSAL_HOE = registerModItem("charged_abyssal_hoe", new ModHoeItem(ModToolMaterials.CHARGED_ABYSSAL, 0, 0f, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).setHasGlint(true));
    public static final Item CHARGED_ABYSSAL_SHOVEL = registerModItem("charged_abyssal_shovel", new ModShovelItem(ModToolMaterials.CHARGED_ABYSSAL, 0, 1f, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).setHasGlint(true));
    public static final Item CHARGED_ABYSSAL_PICKAXE = registerModItem("charged_abyssal_pickaxe", new ModPickaxeItem(ModToolMaterials.CHARGED_ABYSSAL, 1, 0f, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).setHasGlint(true));
    public static final Item CHARGED_ABYSSAL_BOW = registerModItem("charged_abyssal_bow", new ModBowItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS).maxDamage(16758)).setHasGlint(true));
    //    public static final Item CHARGED_ABYSSAL_SHIELD = registerModItem("charged_abyssal_shield", new ModShieldItem(new FabricItemSettings().maxDamage(16758).group(ModItemGroup.GLACIAL_ABYSS), 9, 20, ModItems.CHARGED_ABYSSAL_INGOT).setHasGlint(true));
    public static final Item CHARGED_ABYSSAL_HELMET = registerModItem("charged_abyssal_helmet", new ChargedAbyssalArmorItem(ModArmorMaterials.CHARGED_ABYSSAL, EquipmentSlot.HEAD, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).setHasGlint(true));
    public static final Item CHARGED_ABYSSAL_CHESTPLATE = registerModItem("charged_abyssal_chestplate", new ChargedAbyssalArmorItem(ModArmorMaterials.CHARGED_ABYSSAL, EquipmentSlot.CHEST, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).setHasGlint(true));
    public static final Item CHARGED_ABYSSAL_LEGGINGS = registerModItem("charged_abyssal_leggings", new ChargedAbyssalArmorItem(ModArmorMaterials.CHARGED_ABYSSAL, EquipmentSlot.LEGS, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).setHasGlint(true));
    public static final Item CHARGED_ABYSSAL_BOOTS = registerModItem("charged_abyssal_boots", new ChargedAbyssalArmorItem(ModArmorMaterials.CHARGED_ABYSSAL, EquipmentSlot.FEET, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)).setHasGlint(true));
    public static final Item WITHER_BERRY = registerModItem("wither_berry", new ModAliasedBlockItem(ModBlocks.WITHER_BERRY_VINE, new FabricItemSettings().food(ModFoodComponents.WITHER_BERRY).group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item CHILLED_ESSENCE = registerModItem("chilled_essence", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item FROZEN_MEMBRANE = registerModItem("frozen_membrane", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item PHANTOM_HEART = registerModItem("phantom_heart", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item PHANTOM_SOUL = registerModItem("phantom_soul", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item CRYOGENIC_KNIFE = registerModItem("cryogenic_knife", new CryogenicKnifeItem(ModToolMaterials.CRYOGENIC, 3, -2.4f, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item PARADISE_ROSE_PETAL = registerModItem("paradise_rose_petal", new ModItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item STAFF_OF_GHOULS = registerModItem("staff_of_ghouls", new StaffOfGhoulsItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS).maxDamage(0), 3, 8).setHasGlint(true));
    public static final Item PARADISE_CROWN = registerModItem("paradise_crown", new HeadModelHelmetItem(ModArmorMaterials.PARADISE, EquipmentSlot.HEAD, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS).rarity(Rarity.RARE).maxDamage(0)).setHasGlint(true));
    public static final Item HAUNTED_BATTLEAXE = registerModItem("haunted_battleaxe", new ModAxeItem(ModToolMaterials.HAUNTED, 6, -4.25f, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS)));
    public static final Item HAUNTED_BLESSING = registerModItem("haunted_blessing", new BlessingItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS).rarity(Rarity.EPIC), 15, 62).addTooltip(new TranslatableText("item.glacialabyss.haunted_blessing.tooltip").formatted(Formatting.GOLD)));

    public static final Item MUSIC_DISC_DRIFTING_AWAY = registerModItem("music_disc_drifting_away", new ModMusicDiscItem(15, ModSounds.DRIFTING_AWAY, new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS).maxCount(1).rarity(Rarity.RARE)));

    public static final Item FROSTED_SIGN = registerModItem("frosted_sign", new ModSignItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS).maxCount(16), ModBlocks.FROSTED_SIGN_BLOCK, ModBlocks.FROSTED_WALL_SIGN_BLOCK));
    public static final Item HAUNTED_SIGN = registerModItem("haunted_sign", new ModSignItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS).maxCount(16), ModBlocks.HAUNTED_SIGN_BLOCK, ModBlocks.HAUNTED_WALL_SIGN_BLOCK));

    public static final Item WOODEN_KEY = registerModItem("wooden_key", new KeyItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 0));
    public static final Item STONE_KEY = registerModItem("stone_key", new KeyItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 1));
    public static final Item IRON_KEY = registerModItem("iron_key", new KeyItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 2));
    public static final Item GOLD_KEY = registerModItem("gold_key", new KeyItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 3));
    public static final Item DIAMOND_KEY = registerModItem("diamond_key", new KeyItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 4));
    public static final Item EMERALD_KEY = registerModItem("emerald_key", new KeyItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 4));
    public static final Item ABYSSAL_KEY = registerModItem("abyssal_key", new KeyItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS).maxCount(1).rarity(Rarity.EPIC), 5).addTooltip(new TranslatableText("item.glacialabyss.abyssal_key.tooltip").formatted(Formatting.GOLD)).setHasGlint(true));
    public static final Item CRYOGENIC_KEY = registerModItem("cryogenic_key", new KeyItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 6));
    public static final Item PARADISE_KEY = registerModItem("paradise_key", new KeyItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 6));

    public static final Item WOODEN_KEY_MOLD = registerModItem("wooden_key_mold", new KeyMoldItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 0));
    public static final Item STONE_KEY_MOLD = registerModItem("stone_key_mold", new KeyMoldItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 1));
    public static final Item IRON_KEY_MOLD = registerModItem("iron_key_mold", new KeyMoldItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 2));
    public static final Item GOLD_KEY_MOLD = registerModItem("gold_key_mold", new KeyMoldItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 3));
    public static final Item DIAMOND_KEY_MOLD = registerModItem("diamond_key_mold", new KeyMoldItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 4));
    public static final Item EMERALD_KEY_MOLD = registerModItem("emerald_key_mold", new KeyMoldItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 4));
    public static final Item ABYSSAL_KEY_MOLD = registerModItem("abyssal_key_mold", new KeyMoldItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS).maxCount(1).rarity(Rarity.EPIC), 5));
    public static final Item CRYOGENIC_KEY_MOLD = registerModItem("cryogenic_key_mold", new KeyMoldItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 6));
    public static final Item PARADISE_KEY_MOLD = registerModItem("paradise_key_mold", new KeyMoldItem(new FabricItemSettings().group(ModItemGroup.GLACIAL_ABYSS), 6));

    private static Item registerItem(String name, Item item) {
        return Registry.register(Registry.ITEM, new Identifier(GlacialAbyss.MOD_ID, name), item);
    }

    private static Item registerModItem(String name, IModUsable item) {
        return Registry.register(Registry.ITEM, new Identifier(GlacialAbyss.MOD_ID, name), (Item) item);
    }

    public static void register() {
        GlacialAbyss.LOGGER.info("Registering Mod Items for " + GlacialAbyss.MOD_ID);
    }
}
