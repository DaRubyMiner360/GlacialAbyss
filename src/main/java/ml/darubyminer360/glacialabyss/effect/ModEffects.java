package ml.darubyminer360.glacialabyss.effect;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModEffects {
    public static final StatusEffect FROST = registerStatusEffect("frost", new FrostEffect(StatusEffectCategory.HARMFUL, 3124687));

    public static StatusEffect registerStatusEffect(String name, StatusEffect effect) {
        return Registry.register(Registry.STATUS_EFFECT, new Identifier(GlacialAbyss.MOD_ID, name), effect);
    }

    public static void register() {
        GlacialAbyss.LOGGER.info("Registering Mod Effects for " + GlacialAbyss.MOD_ID);
    }
}
