package ml.darubyminer360.glacialabyss.effect;

import ml.darubyminer360.glacialabyss.entity.ModDamageSource;
import ml.darubyminer360.glacialabyss.particle.ModParticles;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.attribute.AttributeContainer;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffect;
import net.minecraft.entity.effect.StatusEffectCategory;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;

import java.util.Random;

public class FrostEffect extends StatusEffect {
    public FrostEffect(StatusEffectCategory statusEffectCategory, int color) {
        super(statusEffectCategory, color);
    }

    @Override
    public void applyUpdateEffect(LivingEntity entity, int amplifier) {
        if (entity.world.isClient() && entity.getStatusEffect(this).shouldShowParticles()) {
            spawnFoundParticles(entity);
        }

        entity.slowMovement(entity.getBlockStateAtPos(), new Vec3d(0.8999999761581421D, 1.5D, 0.8999999761581421D));
        if (entity.world.isClient()) {
            Random random = entity.world.getRandom();
            boolean bl = entity.lastRenderX != entity.getX() || entity.lastRenderZ != entity.getZ();
            if (bl && random.nextBoolean()) {
                entity.world.addParticle(ParticleTypes.SNOWFLAKE, entity.getX(), (double)(entity.getY() + 1), entity.getZ(), (double)(MathHelper.nextBetween(random, -1.0F, 1.0F) * 0.083333336F), 0.05000000074505806D, (double)(MathHelper.nextBetween(random, -1.0F, 1.0F) * 0.083333336F));
            }
        }

        int duration = entity.getStatusEffect(this).getDuration();
        int i = (new Random().nextInt(2, 6) * 20) >> amplifier;
        if (i <= 0 || duration % i == 0) {
            if (!entity.world.isClient()) {
                int amount = new Random().nextInt(3 + (amplifier / 5)) + 1;

                entity.damage(ModDamageSource.FREEZE, amount);
            }

            super.applyUpdateEffect(entity, amplifier);
        }
    }

    @Override
    public boolean canApplyUpdateEffect(int duration, int amplifier) {
//        int i = (new Random().nextInt(3, 8) * 20) >> amplifier;
//        if (i > 0) {
//            return duration % i == 0;
//        }
        return true;
    }

    @Override
    public void onApplied(LivingEntity entity, AttributeContainer attributes, int amplifier) {
        entity.setInPowderSnow(true);
        super.onApplied(entity, attributes, amplifier);
    }

    @Override
    public void onRemoved(LivingEntity entity, AttributeContainer attributes, int amplifier) {
        entity.setInPowderSnow(false);
        super.onRemoved(entity, attributes, amplifier);
    }

    private void spawnFoundParticles(LivingEntity entity) {
        for (int i = 0; i < 5; i++) {
            entity.getWorld().addParticle(ModParticles.FROST, entity.getX() + new Random().nextDouble(0.5d), entity.getY() + 1, entity.getZ() + new Random().nextDouble(0.5d), Math.cos(i) * new Random().nextDouble(0.25d), 0.15d, Math.sin(i) * new Random().nextDouble(0.25d));
        }
    }
}
