package ml.darubyminer360.glacialabyss.event;

import ml.darubyminer360.glacialabyss.block.entity.AbyssalAltarBlockEntity;
import ml.darubyminer360.glacialabyss.recipe.AbyssalAltarRecipe;
import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Optional;

public interface AbyssalAltarStartCraftCallback {
    Event<AbyssalAltarStartCraftCallback> EVENT = EventFactory.createArrayBacked(AbyssalAltarStartCraftCallback.class,
            (listeners) -> (state, world, pos, player, item, entity, recipe) -> {
                for (AbyssalAltarStartCraftCallback listener : listeners) {
                    ActionResult result = listener.startCraft(state, world, pos, player, item, entity, recipe);

                    if (result != ActionResult.PASS) {
                        return result;
                    }
                }

                return ActionResult.PASS;
            });

    ActionResult startCraft(BlockState state, World world, BlockPos pos, PlayerEntity player, ItemStack item, AbyssalAltarBlockEntity entity, Optional<AbyssalAltarRecipe> recipe);
}
