package ml.darubyminer360.glacialabyss.event;

import ml.darubyminer360.glacialabyss.block.entity.AbyssalAltarBlockEntity;
import ml.darubyminer360.glacialabyss.recipe.AbyssalAltarRecipe;
import net.fabricmc.fabric.api.event.Event;
import net.fabricmc.fabric.api.event.EventFactory;
import net.minecraft.block.BlockState;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.Optional;

/**
 * Callback for adding custom actions for when you craft something with the abyssal altar.
 * Called before the sheep is sheared, items are dropped, and items are damaged.
 * Upon return:
 * - SUCCESS cancels further processing and continues with normal behavior.
 * - PASS falls back to further processing and defaults to SUCCESS if no other listeners are available
 * - FAIL cancels further processing and does not do anything else.
 **/
public interface AbyssalAltarCustomCraftActionsCallback {
    Event<AbyssalAltarCustomCraftActionsCallback> EVENT = EventFactory.createArrayBacked(AbyssalAltarCustomCraftActionsCallback.class,
            (listeners) -> (state, world, pos, player, item, entity, recipe) -> {
                for (AbyssalAltarCustomCraftActionsCallback listener : listeners) {
                    ActionResult result = listener.runCustomActions(state, world, pos, player, item, entity, recipe);

                    if (result != ActionResult.PASS) {
                        return result;
                    }
                }

                return ActionResult.PASS;
            });

    ActionResult runCustomActions(BlockState state, World world, BlockPos pos, PlayerEntity player, ItemStack item, AbyssalAltarBlockEntity entity, Optional<AbyssalAltarRecipe> recipe);
}
