package ml.darubyminer360.glacialabyss.entity;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.entity.boss.phantasm.PhantasmEntity;
import net.fabricmc.fabric.api.object.builder.v1.entity.FabricEntityTypeBuilder;
import net.minecraft.block.Blocks;
import net.minecraft.entity.EntityDimensions;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnGroup;
import net.minecraft.entity.boss.WitherEntity;
import net.minecraft.entity.mob.PhantomEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModEntities {
    public static final EntityType<PhantasmEntity> PHANTASM = Registry.register(Registry.ENTITY_TYPE, new Identifier(GlacialAbyss.MOD_ID, "phantasm"), FabricEntityTypeBuilder.create(SpawnGroup.MONSTER, PhantasmEntity::new).dimensions(EntityDimensions.fixed(0.9f * 2.5f, 0.5f * 2)).trackRangeBlocks(8).build());
}
