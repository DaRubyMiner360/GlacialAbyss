package ml.darubyminer360.glacialabyss.entity.boss.phantasm;

import dev.onyxstudios.cca.api.v3.level.LevelComponents;
import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.advancement.criterion.ModCriteria;
import ml.darubyminer360.glacialabyss.entity.IHelpableEntity;
import ml.darubyminer360.glacialabyss.particle.ModParticles;
import ml.darubyminer360.glacialabyss.util.ModComponents;
import net.minecraft.entity.*;
import net.minecraft.entity.ai.TargetPredicate;
import net.minecraft.entity.ai.control.BodyControl;
import net.minecraft.entity.ai.control.LookControl;
import net.minecraft.entity.ai.control.MoveControl;
import net.minecraft.entity.ai.goal.Goal;
import net.minecraft.entity.attribute.DefaultAttributeContainer;
import net.minecraft.entity.attribute.EntityAttributes;
import net.minecraft.entity.boss.BossBar;
import net.minecraft.entity.boss.ServerBossBar;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.data.DataTracker;
import net.minecraft.entity.data.TrackedData;
import net.minecraft.entity.data.TrackedDataHandlerRegistry;
import net.minecraft.entity.mob.FlyingEntity;
import net.minecraft.entity.mob.MobEntity;
import net.minecraft.entity.mob.Monster;
import net.minecraft.entity.mob.PhantomEntity;
import net.minecraft.entity.passive.CatEntity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.network.MessageType;
import net.minecraft.particle.ParticleTypes;
import net.minecraft.predicate.entity.EntityPredicates;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.sound.SoundCategory;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Box;
import net.minecraft.util.math.MathHelper;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.*;
import org.jetbrains.annotations.Nullable;

import java.util.*;

public class PhantasmEntity extends FlyingEntity implements Monster, IHelpableEntity {
    private final ServerBossBar bossBar = (ServerBossBar) new ServerBossBar(this.getDisplayName(), BossBar.Color.BLUE, BossBar.Style.PROGRESS).setDarkenSky(true);

    public static final int field_28641 = MathHelper.ceil(24.166098f);
    Vec3d targetPosition = Vec3d.ZERO;
    BlockPos circlingCenter = BlockPos.ORIGIN;
    PhantasmAIType aiType = PhantasmAIType.CIRCLE;
    private static final TrackedData<Integer> SIZE = DataTracker.registerData(PhantasmEntity.class, TrackedDataHandlerRegistry.INTEGER);

    private boolean isDead = false;
    private boolean startedDespawnDelay = false;

    public PhantasmEntity(EntityType<? extends PhantasmEntity> entityType, World world) {
        super(entityType, world);
        this.setAiDisabled(false);
        this.experiencePoints = 50;
        this.moveControl = new PhantasmMoveControl(this);
        this.lookControl = new PhantasmLookControl(this);
        this.bossBar.setVisible(true);
    }

    @Override
    public long getDespawnDelay() {
        return 6 * 1000;
    }

    @Override
    public void tickDespawnDelay(long timeWaited) {
        if ((timeWaited % 1000) == 0) {
            GlacialAbyss.LOGGER.info(timeWaited / 1000);
        }
    }

    @Override
    public boolean isWaitingToDespawn() {
        return this.startedDespawnDelay;
    }

    @Override
    public boolean startDespawnDelay() {
        if (this.startedDespawnDelay) {
            return false;
        }
        this.startedDespawnDelay = true;

        if (getDespawnDelay() > 0) {
            boolean aiDisabled = isAiDisabled();
            this.setAiDisabled(true);
            final long[] timeWaited = {0};
            new Thread(() -> {
                Timer timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (startedDespawnDelay) {
                            if (timeWaited[0] < getDespawnDelay()) {
                                tickDespawnDelay(timeWaited[0]);
                                timeWaited[0]++;
                            } else {
                                help();
                                setAiDisabled(aiDisabled);
                                timer.cancel();
                            }
                        }
                        else {
                            timer.cancel();
                        }
                    }
                }, 0, 1);
            }).start();
        }

        return true;
    }

    @Override
    public boolean cancelDespawnDelay() {
        if (!this.startedDespawnDelay) {
            return false;
        }
        this.startedDespawnDelay = false;
        return true;
    }

    @Override
    public void help() {
        ModCriteria.PLAYER_HELPED_ENTITY.trigger((ServerPlayerEntity) getAttacker(), this);
        discard();
    }

    public static DefaultAttributeContainer.Builder setAttributes() {
        return FlyingEntity.createMobAttributes()
                .add(EntityAttributes.GENERIC_MAX_HEALTH, 350.0D)
                .add(EntityAttributes.GENERIC_ATTACK_DAMAGE, 35.0f)
                .add(EntityAttributes.GENERIC_ATTACK_SPEED, 8.75f)
                .add(EntityAttributes.GENERIC_MOVEMENT_SPEED, 0.825f);
    }

    @Override
    public boolean hasWings() {
        return (this.method_33588() + this.age) % field_28641 == 0;
    }

    @Override
    protected BodyControl createBodyControl() {
        return new PhantasmBodyControl(this);
    }

    @Override
    protected void initGoals() {
        this.goalSelector.add(1, new StartAttackGoal());
        this.goalSelector.add(2, new SwoopMovementGoal());
        this.goalSelector.add(3, new CircleMovementGoal());
        this.goalSelector.add(4, new SwarmMovementGoal());
        this.goalSelector.add(4, new IceBeamMovementGoal());
        this.targetSelector.add(1, new FindTargetGoal());
    }

    @Override
    protected void initDataTracker() {
        super.initDataTracker();
        this.dataTracker.startTracking(SIZE, 0);
    }

    public void setPhantasmSize(int size) {
        this.dataTracker.set(SIZE, MathHelper.clamp(size, 0, 64));
    }

    private void onSizeChanged() {
        this.calculateDimensions();
        this.getAttributeInstance(EntityAttributes.GENERIC_ATTACK_DAMAGE).setBaseValue((double)(6 + this.getPhantasmSize()));
    }

    public int getPhantasmSize() {
        return this.dataTracker.get(SIZE);
    }

    @Override
    public boolean damage(DamageSource source, float amount) {
        boolean result = super.damage(source, amount);

        if (!isDead && this.getHealth() <= 0 && !world.isClient()) {
            isDead = true;
            world.getServer().getPlayerManager().broadcast(new TranslatableText("message.glacialabyss.defeat_phantasm").formatted(Formatting.GOLD), MessageType.CHAT, this.uuid);
            ModComponents.DEFEATED_PHANTASM.get(world.getLevelProperties()).setValue(true);
            LevelComponents.sync(ModComponents.DEFEATED_PHANTASM, world.getServer());
        }

        return result;
    }

    @Override
    protected float getActiveEyeHeight(EntityPose pose, EntityDimensions dimensions) {
        return dimensions.height * 0.35f;
    }

    @Override
    public void onTrackedDataSet(TrackedData<?> data) {
        if (SIZE.equals(data)) {
            this.onSizeChanged();
        }
        super.onTrackedDataSet(data);
    }

    public int method_33588() {
        return this.getId() * 3;
    }

    @Override
    protected boolean isDisallowedInPeaceful() {
        return true;
    }

    @Override
    public void tick() {
        super.tick();
        if (this.world.isClient()) {
            float f = MathHelper.cos((float)(this.method_33588() + this.age) * 7.448451f * ((float)Math.PI / 180) + (float)Math.PI);
            float g = MathHelper.cos((float)(this.method_33588() + this.age + 1) * 7.448451f * ((float)Math.PI / 180) + (float)Math.PI);
            if (f > 0.0f && g <= 0.0f) {
                this.world.playSound(this.getX(), this.getY(), this.getZ(), SoundEvents.ENTITY_PHANTOM_FLAP, this.getSoundCategory(), 0.95f + this.random.nextFloat() * 0.05f, 0.95f + this.random.nextFloat() * 0.05f, false);
            }

            int i = this.getPhantasmSize();
            float h = MathHelper.cos(this.getYaw() * 0.017453292F) * (1.3F + 0.21F * (float)i);
            float j = MathHelper.sin(this.getYaw() * 0.017453292F) * (1.3F + 0.21F * (float)i);
            float k = (0.3F + f * 0.45F) * ((float)i * 0.2F + 1.0F);
            this.world.addParticle(ParticleTypes.MYCELIUM, this.getX() + (double)h, this.getY() + (double)k, this.getZ() + (double)j, 0.0D, 0.0D, 0.0D);
            this.world.addParticle(ParticleTypes.MYCELIUM, this.getX() - (double)h, this.getY() + (double)k, this.getZ() - (double)j, 0.0D, 0.0D, 0.0D);
        }
    }

    @Override
    public void tickMovement() {
        super.tickMovement();
    }

    @Override
    protected void mobTick() {
        super.mobTick();
    }

    @Override
    public EntityData initialize(ServerWorldAccess world, LocalDifficulty difficulty, SpawnReason spawnReason, @Nullable EntityData entityData, @Nullable NbtCompound entityNbt) {
        this.circlingCenter = this.getBlockPos().up(5);
        this.setPhantasmSize(5);
        return super.initialize(world, difficulty, spawnReason, entityData, entityNbt);
    }

    @Override
    public void readCustomDataFromNbt(NbtCompound nbt) {
        super.readCustomDataFromNbt(nbt);
        if (nbt.contains("AX")) {
            this.circlingCenter = new BlockPos(nbt.getInt("AX"), nbt.getInt("AY"), nbt.getInt("AZ"));
        }
        this.setPhantasmSize(nbt.getInt("Size"));
    }

    @Override
    public void writeCustomDataToNbt(NbtCompound nbt) {
        super.writeCustomDataToNbt(nbt);
        nbt.putInt("AX", this.circlingCenter.getX());
        nbt.putInt("AY", this.circlingCenter.getY());
        nbt.putInt("AZ", this.circlingCenter.getZ());
        nbt.putInt("Size", this.getPhantasmSize());
    }

    @Override
    public boolean shouldRender(double distance) {
        return true;
    }

    @Override
    public SoundCategory getSoundCategory() {
        return SoundCategory.HOSTILE;
    }

    @Override
    protected SoundEvent getAmbientSound() {
        return SoundEvents.ENTITY_PHANTOM_AMBIENT;
    }

    @Override
    protected SoundEvent getHurtSound(DamageSource source) {
        return SoundEvents.ENTITY_PHANTOM_HURT;
    }

    @Override
    protected SoundEvent getDeathSound() {
        return SoundEvents.ENTITY_PHANTOM_DEATH;
    }

    @Override
    public EntityGroup getGroup() {
        return EntityGroup.UNDEAD;
    }

    @Override
    protected float getSoundVolume() {
        return 1.0f;
    }

    @Override
    public boolean canTarget(EntityType<?> type) {
        return true;
    }

    public EntityDimensions getDimensions(EntityPose pose) {
        int i = this.getPhantasmSize();
        EntityDimensions entityDimensions = super.getDimensions(pose);
        float f = (entityDimensions.width + 0.2F * (float)i) / entityDimensions.width;
        return entityDimensions.scaled(f);
    }

    enum PhantasmAIType {
        CIRCLE,
        SWOOP,
        SWARM,
        ICE_BEAM;
    }

    class PhantasmMoveControl extends MoveControl {
        private float targetSpeed;

        public PhantasmMoveControl(MobEntity owner) {
            super(owner);
            this.targetSpeed = 0.1f;
        }

        @Override
        public void tick() {
            if (PhantasmEntity.this.horizontalCollision) {
                PhantasmEntity.this.setYaw(PhantasmEntity.this.getYaw() + 180.0f);
                this.targetSpeed = 0.1f;
            }
            double d = PhantasmEntity.this.targetPosition.x - PhantasmEntity.this.getX();
            double e = PhantasmEntity.this.targetPosition.y - PhantasmEntity.this.getY();
            double f = PhantasmEntity.this.targetPosition.z - PhantasmEntity.this.getZ();
            double g = Math.sqrt(d * d + f * f);
            if (Math.abs(g) > 9.999999747378752E-6D) {
                double h = 1.0D - Math.abs(e * 0.699999988079071D) / g;
                d *= h;
                f *= h;
                g = Math.sqrt(d * d + f * f);
                double i = Math.sqrt(d * d + f * f + e * e);
                float j = PhantasmEntity.this.getYaw();
                float k = (float)MathHelper.atan2(f, d);
                float l = MathHelper.wrapDegrees(PhantasmEntity.this.getYaw() + 90.0F);
                float m = MathHelper.wrapDegrees(k * 57.295776F);
                PhantasmEntity.this.setYaw(MathHelper.stepUnwrappedAngleTowards(l, m, 4.0F) - 90.0F);
                PhantasmEntity.this.bodyYaw = PhantasmEntity.this.getYaw();
                if (PhantasmEntity.this.aiType != PhantasmAIType.ICE_BEAM) {
                    if (MathHelper.angleBetween(j, PhantasmEntity.this.getYaw()) < 3.0F) {
                        this.targetSpeed = MathHelper.stepTowards(this.targetSpeed, 1.8F, 0.005F * (1.8F / this.targetSpeed));
                    } else {
                        this.targetSpeed = MathHelper.stepTowards(this.targetSpeed, 0.2F, 0.025F);
                    }
                }

                float n = (float)(-(MathHelper.atan2(-e, g) * 57.2957763671875D));
                PhantasmEntity.this.setPitch(n);
                float o = PhantasmEntity.this.getYaw() + 90.0F;
                double p = (double)(this.targetSpeed * MathHelper.cos(o * 0.017453292F)) * Math.abs(d / i);
                double q = (double)(this.targetSpeed * MathHelper.sin(o * 0.017453292F)) * Math.abs(f / i);
                double r = (double)(this.targetSpeed * MathHelper.sin(n * 0.017453292F)) * Math.abs(e / i);
                if (PhantasmEntity.this.aiType != PhantasmAIType.ICE_BEAM) {
                    Vec3d vec3d = PhantasmEntity.this.getVelocity();
                    PhantasmEntity.this.setVelocity(vec3d.add((new Vec3d(p, r, q)).subtract(vec3d).multiply(0.2D)));
                }
            }
        }
    }

    class PhantasmLookControl extends LookControl {
        public PhantasmLookControl(MobEntity entity) {
            super(entity);
        }

        @Override
        public void tick() {
        }
    }

    class PhantasmBodyControl extends BodyControl {
        public PhantasmBodyControl(MobEntity entity) {
            super(entity);
        }

        @Override
        public void tick() {
            PhantasmEntity.this.headYaw = PhantasmEntity.this.bodyYaw;
            PhantasmEntity.this.bodyYaw = PhantasmEntity.this.getYaw();
        }
    }



    class StartAttackGoal extends Goal {
        private int cooldown;

        StartAttackGoal() {
        }

        public boolean canStart() {
            LivingEntity livingEntity = PhantasmEntity.this.getTarget();
            return livingEntity != null ? PhantasmEntity.this.isTarget(livingEntity, TargetPredicate.DEFAULT) : false;
        }

        public void start() {
            this.cooldown = this.getTickCount(10);
            PhantasmEntity.this.aiType = PhantasmAIType.CIRCLE;
            this.startSwoop();
        }

        public void stop() {
            PhantasmEntity.this.circlingCenter = PhantasmEntity.this.world.getTopPosition(Heightmap.Type.MOTION_BLOCKING, PhantasmEntity.this.circlingCenter).up(10 + PhantasmEntity.this.random.nextInt(20));
        }

        public void tick() {
            if (PhantasmEntity.this.aiType == PhantasmAIType.CIRCLE) {
                --this.cooldown;
                if (this.cooldown <= 0) {
                    this.startSwoop();

                    int rand = 3;
                    if (PhantasmEntity.this.getHealth() <= PhantasmEntity.this.getMaxHealth() / 15) {
                        rand = PhantasmEntity.this.random.nextInt(6);
                    }
                    else if (PhantasmEntity.this.getHealth() <= PhantasmEntity.this.getMaxHealth() / 10) {
                        rand = PhantasmEntity.this.random.nextInt(8);
                    }
                    else if (PhantasmEntity.this.getHealth() <= PhantasmEntity.this.getMaxHealth() / 5) {
                        rand = PhantasmEntity.this.random.nextInt(12);
                    }
                    else if (PhantasmEntity.this.getHealth() <= PhantasmEntity.this.getMaxHealth() / 2.5f) {
                        rand = PhantasmEntity.this.random.nextInt(20);
                    }

                    if (rand == 0 || rand == 1) {
                        // Ice Beam Attack
                        PhantasmEntity.this.aiType = PhantasmAIType.ICE_BEAM;
                        this.cooldown = this.getTickCount((12 + PhantasmEntity.this.random.nextInt(6)) * 20);
                        // TODO: Replace with new ice sound effect.
                        PhantasmEntity.this.playSound(SoundEvents.ENTITY_PHANTOM_SWOOP, 10.0F, 0.95F + PhantasmEntity.this.random.nextFloat() * 0.1F);
                    }
                    else if (rand == 2) {
                        // Swarm Attack
                        PhantasmEntity.this.aiType = PhantasmAIType.SWARM;
                        this.cooldown = this.getTickCount((12 + PhantasmEntity.this.random.nextInt(6)) * 20);
                        // TODO: Replace with new swarm sound effect.
                        PhantasmEntity.this.playSound(SoundEvents.ENTITY_PHANTOM_SWOOP, 10.0F, 0.95F + PhantasmEntity.this.random.nextFloat() * 0.1F);
                    }
                    else {
                        // Swoop Attack
                        PhantasmEntity.this.aiType = PhantasmAIType.SWOOP;
                        this.cooldown = this.getTickCount((8 + PhantasmEntity.this.random.nextInt(4)) * 20);
                        PhantasmEntity.this.playSound(SoundEvents.ENTITY_PHANTOM_SWOOP, 10.0F, 0.95F + PhantasmEntity.this.random.nextFloat() * 0.1F);
                    }
                }
            }
        }

        private void startSwoop() {
            PhantasmEntity.this.circlingCenter = PhantasmEntity.this.getTarget().getBlockPos().up(20 + PhantasmEntity.this.random.nextInt(20));
            if (PhantasmEntity.this.circlingCenter.getY() < PhantasmEntity.this.world.getSeaLevel()) {
                PhantasmEntity.this.circlingCenter = new BlockPos(PhantasmEntity.this.circlingCenter.getX(), PhantasmEntity.this.world.getSeaLevel() + 1, PhantasmEntity.this.circlingCenter.getZ());
            }
        }
    }

    class SwoopMovementGoal extends MovementGoal {
        SwoopMovementGoal() {
        }

        @Override
        public boolean canStart() {
            return PhantasmEntity.this.getTarget() != null && PhantasmEntity.this.aiType == PhantasmAIType.SWOOP;
        }

        @Override
        public boolean shouldContinue() {
            LivingEntity livingEntity = PhantasmEntity.this.getTarget();
            if (livingEntity == null) {
                return false;
            }
            if (!livingEntity.isAlive()) {
                return false;
            }
            if (livingEntity instanceof PlayerEntity) {
                PlayerEntity playerEntity = (PlayerEntity)livingEntity;
                if (livingEntity.isSpectator() || playerEntity.isCreative()) {
                    return false;
                }
            }
            if (!this.canStart()) {
                return false;
            }
            return true;
        }

        @Override
        public void start() {
            GlacialAbyss.LOGGER.info("A");
        }

        @Override
        public void stop() {
            PhantasmEntity.this.setTarget(null);
            PhantasmEntity.this.aiType = PhantasmAIType.CIRCLE;
        }

        @Override
        public void tick() {
            LivingEntity livingEntity = PhantasmEntity.this.getTarget();
            if (livingEntity == null) {
                return;
            }
            PhantasmEntity.this.targetPosition = new Vec3d(livingEntity.getX(), livingEntity.getBodyY(0.5), livingEntity.getZ());
            if (PhantasmEntity.this.getBoundingBox().expand(0.2f).intersects(livingEntity.getBoundingBox())) {
                PhantasmEntity.this.tryAttack(livingEntity);
                PhantasmEntity.this.aiType = PhantasmAIType.CIRCLE;
                if (!PhantasmEntity.this.isSilent()) {
                    PhantasmEntity.this.world.syncWorldEvent(WorldEvents.PHANTOM_BITES, PhantasmEntity.this.getBlockPos(), 0);
                }
            } else if (PhantasmEntity.this.horizontalCollision || PhantasmEntity.this.hurtTime > 0) {
                PhantasmEntity.this.aiType = PhantasmAIType.CIRCLE;
            }
        }
    }

    class CircleMovementGoal extends MovementGoal {
        private float angle;
        private float radius;
        private float yOffset;
        private float circlingDirection;

        CircleMovementGoal() {
        }

        @Override
        public boolean canStart() {
            return PhantasmEntity.this.getTarget() == null || PhantasmEntity.this.aiType == PhantasmAIType.CIRCLE;
        }

        @Override
        public void start() {
            GlacialAbyss.LOGGER.info("B");

            this.radius = 5.0f + PhantasmEntity.this.random.nextFloat() * 10.0f;
            this.yOffset = -4.0f + PhantasmEntity.this.random.nextFloat() * 9.0f;
            this.circlingDirection = PhantasmEntity.this.random.nextBoolean() ? 1.0f : -1.0f;
            this.adjustDirection();
        }

        @Override
        public void tick() {
            if (PhantasmEntity.this.random.nextInt(this.getTickCount(350)) == 0) {
                this.yOffset = -4.0f + PhantasmEntity.this.random.nextFloat() * 9.0f;
            }
            if (PhantasmEntity.this.random.nextInt(this.getTickCount(250)) == 0) {
                this.radius += 1.0f;
                if (this.radius > 15.0f) {
                    this.radius = 5.0f;
                    this.circlingDirection = -this.circlingDirection;
                }
            }
            if (PhantasmEntity.this.random.nextInt(this.getTickCount(450)) == 0) {
                this.angle = PhantasmEntity.this.random.nextFloat() * 2.0f * (float)Math.PI;
                this.adjustDirection();
            }
            if (this.isNearTarget()) {
                this.adjustDirection();
            }
            if (PhantasmEntity.this.targetPosition.y < PhantasmEntity.this.getY() && !PhantasmEntity.this.world.isAir(PhantasmEntity.this.getBlockPos().down(1))) {
                this.yOffset = Math.max(1.0f, this.yOffset);
                this.adjustDirection();
            }
            if (PhantasmEntity.this.targetPosition.y > PhantasmEntity.this.getY() && !PhantasmEntity.this.world.isAir(PhantasmEntity.this.getBlockPos().up(1))) {
                this.yOffset = Math.min(-1.0f, this.yOffset);
                this.adjustDirection();
            }
        }

        private void adjustDirection() {
            if (BlockPos.ORIGIN.equals(PhantasmEntity.this.circlingCenter)) {
                PhantasmEntity.this.circlingCenter = PhantasmEntity.this.getBlockPos();
            }
            this.angle += this.circlingDirection * 15.0f * ((float)Math.PI / 180);
            PhantasmEntity.this.targetPosition = Vec3d.of(PhantasmEntity.this.circlingCenter).add(this.radius * MathHelper.cos(this.angle), -4.0f + this.yOffset, this.radius * MathHelper.sin(this.angle));
        }
    }

    class SwarmMovementGoal extends MovementGoal {
        private int timeLeft;

        SwarmMovementGoal() {
        }

        @Override
        public boolean canStart() {
            return PhantasmEntity.this.getTarget() != null && PhantasmEntity.this.aiType == PhantasmAIType.SWARM;
        }

        @Override
        public void start() {
            GlacialAbyss.LOGGER.info("C");

            this.timeLeft = this.getTickCount(PhantasmEntity.this.random.nextInt(2, 11) * 20);
        }

        @Override
        public void stop() {
            PhantasmEntity.this.setTarget(null);
            PhantasmEntity.this.aiType = PhantasmAIType.CIRCLE;
        }

        @Override
        public boolean shouldContinue() {
            LivingEntity livingEntity = PhantasmEntity.this.getTarget();
            if (livingEntity == null) {
                return false;
            }
            if (!livingEntity.isAlive()) {
                return false;
            }
            if (livingEntity instanceof PlayerEntity) {
                PlayerEntity playerEntity = (PlayerEntity)livingEntity;
                if (livingEntity.isSpectator() || playerEntity.isCreative()) {
                    return false;
                }
            }
            if (!this.canStart()) {
                return false;
            }
            return true;
        }

        @Override
        public void tick() {
            LivingEntity livingEntity = PhantasmEntity.this.getTarget();
            if (livingEntity == null) {
                return;
            }

            PhantasmEntity.this.targetPosition = new Vec3d(PhantasmEntity.this.getX(), PhantasmEntity.this.getY(), PhantasmEntity.this.getZ());

            PhantomEntity entity = new PhantomEntity(EntityType.PHANTOM, world);
            entity.setPosition(PhantasmEntity.this.targetPosition);
            world.spawnEntity(entity);

            if (timeLeft <= 0) {
                if (PhantasmEntity.this.random.nextInt(6) == 0) {
                    // Start circling again
                    PhantasmEntity.this.aiType = PhantasmAIType.CIRCLE;
                }
                else {
                    // Swoop Attack
                    PhantasmEntity.this.aiType = PhantasmAIType.SWOOP;
                    PhantasmEntity.this.playSound(SoundEvents.ENTITY_PHANTOM_SWOOP, 10.0F, 0.95F + PhantasmEntity.this.random.nextFloat() * 0.1F);
                }
            }

            timeLeft--;
        }
    }

    class IceBeamMovementGoal extends MovementGoal {
//        private float angle;
//        private float radius;
//        private float yOffset;
//        private float circlingDirection;

        private int timeLeft;

        IceBeamMovementGoal() {
        }

        @Override
        public boolean canStart() {
            return PhantasmEntity.this.getTarget() != null && PhantasmEntity.this.aiType == PhantasmAIType.ICE_BEAM;
        }

        @Override
        public void start() {
            GlacialAbyss.LOGGER.info("D");

            this.timeLeft = this.getTickCount(PhantasmEntity.this.random.nextInt(4, 16) * 20);

//            this.radius = 5.0f + PhantasmEntity.this.random.nextFloat() * 10.0f;
//            this.yOffset = -4.0f + PhantasmEntity.this.random.nextFloat() * 9.0f;
//            this.circlingDirection = PhantasmEntity.this.random.nextBoolean() ? 1.0f : -1.0f;
//            this.adjustDirection();
        }

        @Override
        public void stop() {
            PhantasmEntity.this.setTarget(null);
            PhantasmEntity.this.aiType = PhantasmAIType.CIRCLE;
        }

        @Override
        public boolean shouldContinue() {
            LivingEntity livingEntity = PhantasmEntity.this.getTarget();
            if (livingEntity == null) {
                return false;
            }
            if (!livingEntity.isAlive()) {
                return false;
            }
            if (livingEntity instanceof PlayerEntity) {
                PlayerEntity playerEntity = (PlayerEntity)livingEntity;
                if (livingEntity.isSpectator() || playerEntity.isCreative()) {
                    return false;
                }
            }
            if (!this.canStart()) {
                return false;
            }
            return true;
        }

        @Override
        public void tick() {
            LivingEntity livingEntity = PhantasmEntity.this.getTarget();
            if (livingEntity == null) {
                return;
            }

            PhantasmEntity.this.targetPosition = new Vec3d(PhantasmEntity.this.getX(), PhantasmEntity.this.getY(), PhantasmEntity.this.getZ());

//            if (PhantasmEntity.this.random.nextInt(this.getTickCount(350)) == 0) {
//                this.yOffset = -4.0f + PhantasmEntity.this.random.nextFloat() * 9.0f;
//            }
//            if (PhantasmEntity.this.random.nextInt(this.getTickCount(250)) == 0) {
//                this.radius += 1.0f;
//                if (this.radius > 15.0f) {
//                    this.radius = 5.0f;
//                    this.circlingDirection = -this.circlingDirection;
//                }
//            }
//            if (PhantasmEntity.this.random.nextInt(this.getTickCount(450)) == 0) {
//                this.angle = PhantasmEntity.this.random.nextFloat() * 2.0f * (float)Math.PI;
//                this.adjustDirection();
//            }
//            if (this.isNearTarget()) {
//                this.adjustDirection();
//            }
//            if (PhantasmEntity.this.targetPosition.y < PhantasmEntity.this.getY() && !PhantasmEntity.this.world.isAir(PhantasmEntity.this.getBlockPos().down(1))) {
//                this.yOffset = Math.max(1.0f, this.yOffset);
//                this.adjustDirection();
//            }
//            if (PhantasmEntity.this.targetPosition.y > PhantasmEntity.this.getY() && !PhantasmEntity.this.world.isAir(PhantasmEntity.this.getBlockPos().up(1))) {
//                this.yOffset = Math.min(-1.0f, this.yOffset);
//                this.adjustDirection();
//            }

//            PhantasmEntity.this.lookAt(PhantasmEntity.this.getCommandSource().getEntityAnchor(), new Vec3d(PhantasmEntity.this.getTarget().getX(), PhantasmEntity.this.getTarget().getY(), PhantasmEntity.this.getTarget().getZ()));
//            PhantasmEntity.this.lookAtEntity(PhantasmEntity.this.getTarget(), 10000, 10000);






            // TODO: Shoot laser
//            PhantomEntity e = new PhantomEntity(EntityType.PHANTOM, world);
//            e.setPosition(PhantasmEntity.this.targetPosition);
//            world.spawnEntity(e);

            drawParticles(world);






//            PhantasmEntity.this.targetPosition = new Vec3d(PhantasmEntity.this.getX(), PhantasmEntity.this.getY(), PhantasmEntity.this.getZ());







            if (timeLeft <= 0) {
                if (PhantasmEntity.this.random.nextInt(6) == 0) {
                    // Start circling again
                    PhantasmEntity.this.aiType = PhantasmAIType.CIRCLE;
                }
                else {
                    // Swoop Attack
                    PhantasmEntity.this.aiType = PhantasmAIType.SWOOP;
                    PhantasmEntity.this.playSound(SoundEvents.ENTITY_PHANTOM_SWOOP, 10.0F, 0.95F + PhantasmEntity.this.random.nextFloat() * 0.1F);
                }
            }

            timeLeft--;
        }

//        private void adjustDirection() {
//            if (BlockPos.ORIGIN.equals(PhantasmEntity.this.circlingCenter)) {
//                PhantasmEntity.this.circlingCenter = PhantasmEntity.this.getBlockPos();
//            }
//            this.angle += this.circlingDirection * 15.0f * ((float)Math.PI / 180);
//            PhantasmEntity.this.targetPosition = Vec3d.of(PhantasmEntity.this.circlingCenter).add(this.radius * MathHelper.cos(this.angle), -4.0f + this.yOffset, this.radius * MathHelper.sin(this.angle));
//        }

//        public void drawParticles(World world) {
//            if (world.isClient) {
//                BlockPos playerPos = PhantasmEntity.this.getBlockPos();
//                int x = playerPos.getX();
//                int y = playerPos.getY() + 4;
//                int z = playerPos.getZ();
//
//                List<LivingEntity> livingEntities = world.getEntitiesByClass(LivingEntity.class, new Box(x - 10, y - 10, z - 10, x + 10, y + 10, z + 10), EntityPredicates.VALID_ENTITY);
//
//                for (LivingEntity entity : livingEntities) {
//                    if (entity != PhantasmEntity.this) {
//                        Vec3d entityVector = new Vec3d(entity.getX(), entity.getY(), entity.getZ());
//                        Vec3d playerVector = new Vec3d(x, y, z);
//
//                        Vec3d velocity = entityVector.subtract(playerVector);
//
//                        world.addParticle(ParticleTypes.CLOUD, x, y, z, velocity.x, velocity.y, velocity.z);
//                        world.addParticle(ModParticles.FROST, x, y, z, velocity.x, velocity.y, velocity.z);
//                    }
//                }
//            }
//        }
        private void drawParticles(World world) {
            for (int q = 0; q < 360; q++) {
                if (q % 20 == 0) {
                    float f = MathHelper.cos((float)(PhantasmEntity.this.method_33588() + PhantasmEntity.this.age) * 7.448451f * ((float)Math.PI / 180) + (float)Math.PI);
                    float g = MathHelper.cos((float)(PhantasmEntity.this.method_33588() + PhantasmEntity.this.age + 1) * 7.448451f * ((float)Math.PI / 180) + (float)Math.PI);
                    if (f > 0.0f && g <= 0.0f) {
                        PhantasmEntity.this.world.playSound(PhantasmEntity.this.getX(), PhantasmEntity.this.getY(), PhantasmEntity.this.getZ(), SoundEvents.ENTITY_PHANTOM_FLAP, PhantasmEntity.this.getSoundCategory(), 0.95f + PhantasmEntity.this.random.nextFloat() * 0.05f, 0.95f + PhantasmEntity.this.random.nextFloat() * 0.05f, false);
                    }

                    int i = PhantasmEntity.this.getPhantasmSize();
                    float h = MathHelper.cos(PhantasmEntity.this.getYaw() * 0.017453292F) * (1.3F + 0.21F * (float)i);
                    float j = MathHelper.sin(PhantasmEntity.this.getYaw() * 0.017453292F) * (1.3F + 0.21F * (float)i);
                    float k = (0.3F + f * 0.45F) * ((float)i * 0.2F + 1.0F);
                    PhantasmEntity.this.world.addParticle(ModParticles.FROST, PhantasmEntity.this.getX() + (double)h, PhantasmEntity.this.getY() + (double)k, PhantasmEntity.this.getZ() + (double)j, 0.0D, 0.0D, 0.0D);
                    PhantasmEntity.this.world.addParticle(ModParticles.FROST, PhantasmEntity.this.getX() - (double)h, PhantasmEntity.this.getY() + (double)k, PhantasmEntity.this.getZ() - (double)j, 0.0D, 0.0D, 0.0D);

                    world.addParticle(ModParticles.FROST,
                            PhantasmEntity.this.getX() + 0.5d, PhantasmEntity.this.getY() + 1, PhantasmEntity.this.getZ() + 0.5d,
                            Math.cos(q) * 0.25d, 0.15d, Math.sin(i) * 0.25d);
                }
            }
        }
    }


    class FindTargetGoal extends Goal {
        private final TargetPredicate PLAYERS_IN_RANGE_PREDICATE = TargetPredicate.createAttackable().setBaseMaxDistance(64.0);
        private int delay = FindTargetGoal.toGoalTicks(20);

        FindTargetGoal() {
        }

        @Override
        public boolean canStart() {
            if (this.delay > 0) {
                --this.delay;
                return false;
            }
            this.delay = FindTargetGoal.toGoalTicks(60);
            List<PlayerEntity> list = PhantasmEntity.this.world.getPlayers(this.PLAYERS_IN_RANGE_PREDICATE, PhantasmEntity.this, PhantasmEntity.this.getBoundingBox().expand(16.0, 64.0, 16.0));
            if (!list.isEmpty()) {
                list.sort(Comparator.comparing(Entity::getY).reversed());
                for (PlayerEntity playerEntity : list) {
                    if (!PhantasmEntity.this.isTarget(playerEntity, TargetPredicate.DEFAULT)) continue;
                    PhantasmEntity.this.setTarget(playerEntity);
                    return true;
                }
            }
            return false;
        }

        @Override
        public boolean shouldContinue() {
            LivingEntity livingEntity = PhantasmEntity.this.getTarget();
            if (livingEntity != null) {
                return PhantasmEntity.this.isTarget(livingEntity, TargetPredicate.DEFAULT);
            }
            return false;
        }
    }

    abstract class MovementGoal extends Goal {
        public MovementGoal() {
            this.setControls(EnumSet.of(Goal.Control.MOVE));
        }

        protected boolean isNearTarget() {
            return PhantasmEntity.this.targetPosition.squaredDistanceTo(PhantasmEntity.this.getX(), PhantasmEntity.this.getY(), PhantasmEntity.this.getZ()) < 4.0;
        }
    }
}
