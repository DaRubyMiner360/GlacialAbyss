package ml.darubyminer360.glacialabyss.entity.render;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.GlacialAbyssClient;
import ml.darubyminer360.glacialabyss.entity.boss.phantasm.PhantasmEntity;
import ml.darubyminer360.glacialabyss.entity.model.PhantasmEntityModel;
import net.minecraft.client.render.entity.EntityRendererFactory;
import net.minecraft.client.render.entity.MobEntityRenderer;
import net.minecraft.client.render.entity.feature.PhantomEyesFeatureRenderer;
import net.minecraft.client.render.entity.model.EntityModelLayers;
import net.minecraft.client.render.entity.model.PhantomEntityModel;
import net.minecraft.client.util.math.MatrixStack;
import net.minecraft.entity.mob.PhantomEntity;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.Vec3f;

public class PhantasmEntityRenderer extends MobEntityRenderer<PhantasmEntity, PhantasmEntityModel> {
    public PhantasmEntityRenderer(EntityRendererFactory.Context context) {
        super(context, new PhantasmEntityModel(context.getPart(GlacialAbyssClient.MODEL_PHANTASM_LAYER)), 0.75f);
//        this.addFeature(new PhantasmEyesFeatureRenderer<PhantasmEntity>(this));
    }

    @Override
    public Identifier getTexture(PhantasmEntity entity) {
        return new Identifier(GlacialAbyss.MOD_ID, "textures/entity/phantasm/phantasm.png");
    }

    @Override
    protected void scale(PhantasmEntity phantasmEntity, MatrixStack matrixStack, float f) {
        int i = phantasmEntity.getPhantasmSize();
        float g = 1.0F + 0.15F * (float)i;
        matrixStack.scale(g, g, g);
        matrixStack.translate(0.0D, 1.3125D, 0.1875D);
    }

    @Override
    protected void setupTransforms(PhantasmEntity phantasmEntity, MatrixStack matrixStack, float f, float g, float h) {
        super.setupTransforms(phantasmEntity, matrixStack, f, g, h);
        matrixStack.multiply(Vec3f.POSITIVE_X.getDegreesQuaternion(phantasmEntity.getPitch()));
    }
}
