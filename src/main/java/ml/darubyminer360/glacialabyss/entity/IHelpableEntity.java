package ml.darubyminer360.glacialabyss.entity;

public interface IHelpableEntity {
    long getDespawnDelay(); // In milliseconds
    void tickDespawnDelay(long timeWaited); // timeWaited is in milliseconds
    boolean isWaitingToDespawn();
    boolean startDespawnDelay();
    boolean cancelDespawnDelay();
    void help();
}
