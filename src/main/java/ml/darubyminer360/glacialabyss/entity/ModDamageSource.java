package ml.darubyminer360.glacialabyss.entity;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import net.minecraft.entity.damage.DamageSource;

public class ModDamageSource extends DamageSource {
    public static final DamageSource FREEZE = new ModDamageSource(GlacialAbyss.MOD_ID + ".freeze").setBypassesArmor();

    public ModDamageSource(String name) {
        super(name);
    }
}
