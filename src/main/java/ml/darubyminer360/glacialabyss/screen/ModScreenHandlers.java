package ml.darubyminer360.glacialabyss.screen;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import net.fabricmc.fabric.api.screenhandler.v1.ScreenHandlerRegistry;
import net.minecraft.screen.ScreenHandlerType;
import net.minecraft.util.Identifier;

public class ModScreenHandlers {
    public static ScreenHandlerType<RemolderScreenHandler> REMOLDER_SCREEN_HANDLER = ScreenHandlerRegistry.registerSimple(new Identifier(GlacialAbyss.MOD_ID, "remolder"), RemolderScreenHandler::new);

    public static void register() {
        GlacialAbyss.LOGGER.info("Registering Mod Screen Handlers for " + GlacialAbyss.MOD_ID);
    }
}
