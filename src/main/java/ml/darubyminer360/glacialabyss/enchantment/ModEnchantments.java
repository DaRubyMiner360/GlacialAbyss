package ml.darubyminer360.glacialabyss.enchantment;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.EquipmentSlot;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModEnchantments {
    public static Enchantment GHOUL_CHASER = register("ghoul_chaser", new GhoulChaserEnchantment(Enchantment.Rarity.UNCOMMON, EnchantmentTarget.WEAPON, new EquipmentSlot[] { EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND }));
    public static Enchantment CALL_OF_THE_GHOULS = register("call_of_the_ghouls", new GhoulCallEnchantment(Enchantment.Rarity.UNCOMMON, EnchantmentTarget.WEAPON, new EquipmentSlot[] { EquipmentSlot.MAINHAND, EquipmentSlot.OFFHAND }));

    private static Enchantment register(String name, Enchantment enchantment) {
        return Registry.register(Registry.ENCHANTMENT, new Identifier(GlacialAbyss.MOD_ID, name), enchantment);
    }

    public static void register() {
        GlacialAbyss.LOGGER.info("Registering Mod Enchantments for " + GlacialAbyss.MOD_ID);
    }
}