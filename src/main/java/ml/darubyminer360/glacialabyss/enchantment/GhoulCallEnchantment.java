package ml.darubyminer360.glacialabyss.enchantment;

import ml.darubyminer360.glacialabyss.item.custom.StaffOfGhoulsItem;
import net.minecraft.enchantment.Enchantment;
import net.minecraft.enchantment.EnchantmentTarget;
import net.minecraft.entity.*;
import net.minecraft.item.ItemStack;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.math.BlockPos;

public class GhoulCallEnchantment extends Enchantment {
    public GhoulCallEnchantment(Rarity weight, EnchantmentTarget type, EquipmentSlot... slotTypes) {
        super(weight, type, slotTypes);
    }

    @Override
    public boolean isAcceptableItem(ItemStack stack) {
        if (stack.getItem() instanceof StaffOfGhoulsItem) {
            return true;
        }

        return false;
    }

    @Override
    public int getMaxLevel() {
        return 5;
    }
}