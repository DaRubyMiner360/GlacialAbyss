package ml.darubyminer360.glacialabyss.mixin;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.entity.IHelpableEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.ItemEntity;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.world.GameMode;
import net.minecraft.world.World;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(PlayerEntity.class)
public abstract class PlayerEntityMixin extends LivingEntity {
    protected PlayerEntityMixin(EntityType<? extends LivingEntity> entityType, World world) {
        super(entityType, world);
        throw new AssertionError();
    }

    @Inject(method = "jump", at = @At("HEAD"), cancellable = true)
    protected void tryJump(CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "attack", at = @At("HEAD"), cancellable = true)
    protected void tryAttack(Entity target, CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "eatFood", at = @At("HEAD"), cancellable = true)
    protected void tryEatFood(World world, ItemStack stack, CallbackInfoReturnable<ItemStack> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.setReturnValue(stack);
        }
    }

    @Inject(method = "dropItem(Lnet/minecraft/item/ItemStack;ZZ)Lnet/minecraft/entity/ItemEntity;", at = @At("HEAD"), cancellable = true)
    protected void tryDropItem(ItemStack stack, boolean throwRandomly, boolean retainOwnership, CallbackInfoReturnable<ItemEntity> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            if (stack.isEmpty()) {
                info.setReturnValue(null);
            }

            ItemEntity itemEntity = new ItemEntity(this.world, this.getX(), this.getY(), this.getZ(), stack);
            itemEntity.setPickupDelay(0);
            if (retainOwnership) {
                itemEntity.setThrower(this.getUuid());
            }
            info.setReturnValue(itemEntity);
        }
    }

    @Inject(method = "canPlaceOn", at = @At("HEAD"), cancellable = true)
    protected void canPlaceOn(BlockPos pos, Direction facing, ItemStack stack, CallbackInfoReturnable<Boolean> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.setReturnValue(false);
        }
    }

    @Inject(method = "isImmobile", at = @At("HEAD"), cancellable = true)
    protected void tryIsImmobile(CallbackInfoReturnable<Boolean> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.setReturnValue(true);
        }
    }

    @Inject(method = "getMovementSpeed", at = @At("HEAD"), cancellable = true)
    protected void tryGetMovementSpeed(CallbackInfoReturnable<Float> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.setReturnValue(0f);
        }
    }

    @Inject(method = "isInvulnerableTo", at = @At("HEAD"), cancellable = true)
    protected void tryIsInvulnerableTo(DamageSource damageSource, CallbackInfoReturnable<Boolean> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.setReturnValue(true);
        }
    }

    @Inject(method = "damage", at = @At("HEAD"), cancellable = true)
    protected void tryDamage(DamageSource source, float amount, CallbackInfoReturnable<Boolean> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid())) {
            info.setReturnValue(false);
        }
    }

    @Inject(method = "interact", at = @At("HEAD"), cancellable = true)
    protected void tryInteract(Entity entity, Hand hand, CallbackInfoReturnable<ActionResult> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.setReturnValue(ActionResult.FAIL);
        }
    }

    @Inject(method = "takeShieldHit", at = @At("HEAD"), cancellable = true)
    protected void tryTakeShieldHit(LivingEntity attacker, CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "disableShield", at = @At("HEAD"), cancellable = true)
    protected void tryDisableShield(boolean sprinting, CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "canTakeDamage", at = @At("HEAD"), cancellable = true)
    protected void tryCanTakeDamage(CallbackInfoReturnable<Boolean> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.setReturnValue(false);
        }
    }

    @Inject(method = "damageArmor", at = @At("HEAD"), cancellable = true)
    protected void tryDamageArmor(DamageSource source, float amount, CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "updateSwimming", at = @At("HEAD"), cancellable = true)
    protected void tryUpdateSwimming(CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "onSwimmingStart", at = @At("HEAD"), cancellable = true)
    protected void trySwimmingStart(CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "canModifyBlocks", at = @At("HEAD"), cancellable = true)
    protected void tryCanModifyBlocks(CallbackInfoReturnable<Boolean> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.setReturnValue(false);
        }
    }

    @Inject(method = "resetLastAttackedTicks", at = @At("HEAD"), cancellable = true)
    protected void tryResetLastAttackedTicks(CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "isBlockBreakingRestricted", at = @At("HEAD"), cancellable = true)
    protected void tryIsBlockBreakingRestricted(World world, BlockPos pos, GameMode gameMode, CallbackInfoReturnable<Boolean> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.setReturnValue(true);
        }
    }
}
