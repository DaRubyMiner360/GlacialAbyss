package ml.darubyminer360.glacialabyss.mixin;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.entity.IHelpableEntity;
import net.minecraft.entity.Entity;
import net.minecraft.entity.MovementType;
import net.minecraft.server.command.CommandOutput;
import net.minecraft.util.Nameable;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.entity.EntityLike;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Entity.class)
public abstract class EntityMixin implements Nameable, EntityLike, CommandOutput {
    @Inject(method = "move", at = @At("HEAD"), cancellable = true)
    protected void tryMove(MovementType movementType, Vec3d movement, CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "setPos", at = @At("HEAD"), cancellable = true)
    protected void trySetPos(double x, double y, double z, CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "setSneaking", at = @At("HEAD"), cancellable = true)
    protected void trySetSneaking(boolean sneaking, CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "isSneaking", at = @At("HEAD"), cancellable = true)
    protected void tryIsSneaking(CallbackInfoReturnable<Boolean> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.setReturnValue(false);
        }
    }
}
