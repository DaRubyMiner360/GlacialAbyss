package ml.darubyminer360.glacialabyss.mixin;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.advancement.criterion.ModCriteria;
import ml.darubyminer360.glacialabyss.entity.IHelpableEntity;
import net.minecraft.block.BlockState;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.LivingEntity;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.util.Hand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(LivingEntity.class)
public abstract class LivingEntityMixin extends Entity {
    @Shadow public abstract void setAttacker(@Nullable LivingEntity attacker);

    public LivingEntityMixin(EntityType<?> type, World world) {
        super(type, world);
        throw new AssertionError();
    }

    @Inject(method = "damage", at = @At("HEAD"), cancellable = true)
    public void damage(DamageSource source, float amount, CallbackInfoReturnable<Boolean> info) {
        if (GlacialAbyss.blessedEntities.contains(this.getUuid())) {
            info.setReturnValue(false);
        }

        if (source.getAttacker() != null && source.getAttacker().isPlayer() && !source.getAttacker().getWorld().isClient()) {
            // Start Temporary
            if (this instanceof IHelpableEntity) {
                ((IHelpableEntity) this).startDespawnDelay();
            }
            // End Temporary
            if (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn()) {
                amount = 0.0f;
                this.setAttacker((LivingEntity) source.getAttacker());
                info.setReturnValue(true);
            }
        }
    }

    @Inject(method = "fall", at = @At("HEAD"), cancellable = true)
    protected void tryFall(double heightDifference, boolean onGround, BlockState landedState, BlockPos landedPosition, CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "swingHand(Lnet/minecraft/util/Hand;Z)V", at = @At("HEAD"), cancellable = true)
    protected void trySwingHand(Hand hand, boolean fromServerPlayer, CallbackInfo info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.cancel();
        }
    }

    @Inject(method = "shouldSpawnConsumptionEffects", at = @At("HEAD"), cancellable = true)
    protected void tryShouldSpawnConsumptionEffects(CallbackInfoReturnable<Boolean> info) {
        if (GlacialAbyss.frozenPlayers.contains(this.getUuid()) || (this instanceof IHelpableEntity && ((IHelpableEntity) this).isWaitingToDespawn())) {
            info.setReturnValue(false);
        }
    }
}
