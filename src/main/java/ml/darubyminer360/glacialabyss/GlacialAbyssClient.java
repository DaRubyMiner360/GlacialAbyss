package ml.darubyminer360.glacialabyss;

import ml.darubyminer360.glacialabyss.block.ModBlocks;
import ml.darubyminer360.glacialabyss.entity.ModEntities;
import ml.darubyminer360.glacialabyss.entity.model.PhantasmEntityModel;
import ml.darubyminer360.glacialabyss.entity.render.PhantasmEntityRenderer;
import ml.darubyminer360.glacialabyss.particle.ModParticles;
import ml.darubyminer360.glacialabyss.particle.custom.FrostParticle;
import ml.darubyminer360.glacialabyss.screen.ModScreenHandlers;
import ml.darubyminer360.glacialabyss.screen.RemolderScreen;
import ml.darubyminer360.glacialabyss.util.ModModelPredicateProvider;
import net.fabricmc.api.ClientModInitializer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.fabricmc.fabric.api.blockrenderlayer.v1.BlockRenderLayerMap;
import net.fabricmc.fabric.api.client.particle.v1.ParticleFactoryRegistry;
import net.fabricmc.fabric.api.client.rendereregistry.v1.EntityRendererRegistry;
import net.fabricmc.fabric.api.client.rendering.v1.EntityModelLayerRegistry;
import net.fabricmc.fabric.api.client.screenhandler.v1.ScreenRegistry;
import net.minecraft.client.render.RenderLayer;
import net.minecraft.client.render.entity.model.EntityModelLayer;
//import net.minecraft.client.render.entity.model.ShieldEntityModel;
import net.minecraft.util.Identifier;

@Environment(EnvType.CLIENT)
public class GlacialAbyssClient implements ClientModInitializer {
//    public static final EntityModelLayer ABYSSAL_SHIELD_MODEL_LAYER = new EntityModelLayer(new Identifier(GlacialAbyss.MOD_ID, "abyssal_shield"), "main");
//    public static final EntityModelLayer CHARGED_ABYSSAL_SHIELD_MODEL_LAYER = new EntityModelLayer(new Identifier(GlacialAbyss.MOD_ID, "charged_abyssal_shield"), "main");

    public static final EntityModelLayer MODEL_PHANTASM_LAYER = new EntityModelLayer(new Identifier(GlacialAbyss.MOD_ID, "phantasm"), "main");

    @Override
    public void onInitializeClient() {
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.FROSTED_DOOR, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.FROSTED_TRAPDOOR, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.FROSTED_SAPLING, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.FROSTED_LEAVES, RenderLayer.getCutout());

        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.HAUNTED_DOOR, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.HAUNTED_TRAPDOOR, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.HAUNTED_SAPLING, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.HAUNTED_LEAVES, RenderLayer.getCutout());

        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.FROSTED_FLOWER, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.FROSTED_PLANT, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.POTTED_FROSTED_FLOWER, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.POTTED_FROSTED_PLANT, RenderLayer.getCutout());

        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.WITHER_BERRY_VINE, RenderLayer.getCutout());
        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.WITHER_BERRY_VINE_PLANT, RenderLayer.getCutout());

        BlockRenderLayerMap.INSTANCE.putBlock(ModBlocks.ABYSSAL_ALTAR, RenderLayer.getTranslucent());

        /*EntityModelLayerRegistry.registerModelLayer(ABYSSAL_SHIELD_MODEL_LAYER, ShieldEntityModel::getTexturedModelData);
        EntityModelLayerRegistry.registerModelLayer(CHARGED_ABYSSAL_SHIELD_MODEL_LAYER, ShieldEntityModel::getTexturedModelData);
        ClientSpriteRegistryCallback.event(SpriteAtlasTexture.BLOCK_ATLAS_TEXTURE).register((atlasTexture, registry) -> {
            registry.register(new Identifier(GlacialAbyss.MOD_ID, "entity/abyssal_shield_base"));
            registry.register(new Identifier(GlacialAbyss.MOD_ID, "entity/charged_abyssal_shield_base"));
            registry.register(new Identifier(GlacialAbyss.MOD_ID, "entity/abyssal_shield_base_nopattern"));
            registry.register(new Identifier(GlacialAbyss.MOD_ID, "entity/charged_abyssal_shield_base_nopattern"));
        });*/

        ModModelPredicateProvider.registerModModels();

        ScreenRegistry.register(ModScreenHandlers.REMOLDER_SCREEN_HANDLER, RemolderScreen::new);

        ParticleFactoryRegistry.getInstance().register(ModParticles.FROST, FrostParticle.Factory::new);

        EntityRendererRegistry.INSTANCE.register(ModEntities.PHANTASM, PhantasmEntityRenderer::new);

        EntityModelLayerRegistry.registerModelLayer(MODEL_PHANTASM_LAYER, PhantasmEntityModel::getTexturedModelData);
    }
}
