package ml.darubyminer360.glacialabyss.block;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.block.custom.*;
import ml.darubyminer360.glacialabyss.block.entity.ModSignTypes;
import ml.darubyminer360.glacialabyss.item.ModItemGroup;
import ml.darubyminer360.glacialabyss.item.custom.ModBlockItem;
import ml.darubyminer360.glacialabyss.util.IModUsable;
import ml.darubyminer360.glacialabyss.world.feature.tree.FrostedSaplingGenerator;
import ml.darubyminer360.glacialabyss.world.feature.tree.HauntedSaplingGenerator;
import net.fabricmc.fabric.api.item.v1.FabricItemSettings;
import net.fabricmc.fabric.api.object.builder.v1.block.FabricBlockSettings;
import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.block.Material;
import net.minecraft.block.PressurePlateBlock;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.item.BlockItem;
import net.minecraft.item.ItemGroup;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.math.intprovider.UniformIntProvider;
import net.minecraft.util.registry.Registry;

public class ModBlocks {
    public static final Block ABYSSAL_STONE = registerModBlock("abyssal_stone", new GlacialAbyssBlock(FabricBlockSettings.of(Material.STONE).strength(2.5f, 7.0f).requiresTool()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block ABYSSAL_STONE_BRICKS = registerModBlock("abyssal_stone_bricks", new GlacialAbyssBlock(FabricBlockSettings.of(Material.STONE).strength(2.5f, 7.0f).requiresTool()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block ABYSSAL_STONE_TILES = registerModBlock("abyssal_stone_tiles", new GlacialAbyssBlock(FabricBlockSettings.of(Material.STONE).strength(2.5f, 7.0f).requiresTool()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROZEN_OBSIDIAN = registerModBlock("frozen_obsidian", new GlacialAbyssBlock(FabricBlockSettings.of(Material.STONE).strength(55.0f, 1500.0f).requiresTool()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block CHARGED_ABYSSAL_BLOCK = registerModBlock("charged_abyssal_block", new GlacialAbyssBlock(FabricBlockSettings.of(Material.METAL).strength(8f).requiresTool()).setHasGlint(true), ModItemGroup.GLACIAL_ABYSS);
    public static final Block ABYSSAL_BLOCK = registerModBlock("abyssal_block", new GlacialAbyssBlock(FabricBlockSettings.of(Material.METAL).strength(6f).requiresTool()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block RAW_ABYSSAL_BLOCK = registerModBlock("raw_abyssal_block", new GlacialAbyssBlock(FabricBlockSettings.of(Material.METAL).strength(5.5f).requiresTool()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block ABYSSAL_ORE = registerModBlock("abyssal_ore", new GlacialAbyssOreBlock(FabricBlockSettings.of(Material.STONE).strength(4.5f).requiresTool(), UniformIntProvider.create(4, 10)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block UNSTABLE_ABYSS_ORE = registerModBlock("unstable_abyss_ore", new GlacialAbyssOreBlock(FabricBlockSettings.of(Material.STONE).strength(2f).requiresTool(), UniformIntProvider.create(4, 10)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block DEEPSLATE_UNSTABLE_ABYSS_ORE = registerModBlock("deepslate_unstable_abyss_ore", new GlacialAbyssOreBlock(FabricBlockSettings.of(Material.STONE).strength(2.5f).requiresTool(), UniformIntProvider.create(4, 10)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block UNSTABLE_ABYSS_BLOCK = registerModBlock("unstable_abyss_block", new GlacialAbyssBlock(FabricBlockSettings.of(Material.METAL).strength(3f).requiresTool()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_DIRT = registerModBlock("frosted_dirt", new GlacialAbyssBlock(FabricBlockSettings.copy(Blocks.DIRT)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_GRASS_BLOCK = registerModBlock("frosted_grass_block", new GlacialAbyssGrassBlock(FabricBlockSettings.copy(Blocks.GRASS_BLOCK)), ModItemGroup.GLACIAL_ABYSS);

    public static final Block FROSTED_LEAVES = registerModBlock("frosted_leaves", new ModLeavesBlock(FabricBlockSettings.copy(Blocks.OAK_LEAVES).nonOpaque()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_SAPLING = registerModBlock("frosted_sapling", new ModSaplingBlock(new FrostedSaplingGenerator(), FabricBlockSettings.copy(Blocks.OAK_SAPLING)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_LOG = registerModBlock("frosted_log", new ModPillarBlock(FabricBlockSettings.copy(Blocks.OAK_LOG).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_WOOD = registerModBlock("frosted_wood", new ModPillarBlock(FabricBlockSettings.copy(Blocks.OAK_WOOD).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block STRIPPED_FROSTED_LOG = registerModBlock("stripped_frosted_log", new ModPillarBlock(FabricBlockSettings.copy(Blocks.STRIPPED_OAK_LOG).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block STRIPPED_FROSTED_WOOD = registerModBlock("stripped_frosted_wood", new ModPillarBlock(FabricBlockSettings.copy(Blocks.STRIPPED_OAK_WOOD).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_PLANKS = registerModBlock("frosted_planks", new ModBlock(FabricBlockSettings.copy(Blocks.OAK_PLANKS).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_BUTTON = registerModBlock("frosted_button", new ModStoneButtonBlock(FabricBlockSettings.of(Material.WOOD).strength(2.5f).noCollision()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_PRESSURE_PLATE = registerModBlock("frosted_pressure_plate", new ModPressurePlateBlock(PressurePlateBlock.ActivationRule.EVERYTHING, FabricBlockSettings.of(Material.WOOD).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_FENCE = registerModBlock("frosted_fence", new ModFenceBlock(FabricBlockSettings.of(Material.WOOD).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_FENCE_GATE = registerModBlock("frosted_fence_gate", new ModFenceGateBlock(FabricBlockSettings.of(Material.WOOD).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_SLAB = registerModBlock("frosted_slab", new ModSlabBlock(FabricBlockSettings.of(Material.METAL).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_STAIRS = registerModBlock("frosted_stairs", new ModStairsBlock(ModBlocks.FROSTED_PLANKS.getDefaultState(), FabricBlockSettings.of(Material.WOOD).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_DOOR = registerModBlock("frosted_door", new ModDoorBlock(FabricBlockSettings.of(Material.WOOD).strength(2.5f).nonOpaque()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_TRAPDOOR = registerModBlock("frosted_trapdoor", new ModTrapdoorBlock(FabricBlockSettings.of(Material.WOOD).strength(2.5f).nonOpaque()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_WALL_SIGN_BLOCK = registerModBlockWithoutBlockItem("frosted_wall_sign", new ModWallSignBlock(FabricBlockSettings.copy(Blocks.OAK_WALL_SIGN), ModSignTypes.FROSTED), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_SIGN_BLOCK = registerModBlockWithoutBlockItem("frosted_sign", new ModSignBlock(FabricBlockSettings.copy(Blocks.OAK_SIGN), ModSignTypes.FROSTED), ModItemGroup.GLACIAL_ABYSS);

    public static final Block HAUNTED_LEAVES = registerModBlock("haunted_leaves", new HauntedLeavesBlock(FabricBlockSettings.copy(Blocks.OAK_LEAVES).nonOpaque()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_SAPLING = registerModBlock("haunted_sapling", new HauntedSaplingBlock(new HauntedSaplingGenerator(), FabricBlockSettings.copy(Blocks.OAK_SAPLING)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_LOG = registerModBlock("haunted_log", new HauntedPillarBlock(FabricBlockSettings.copy(Blocks.OAK_LOG).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_WOOD = registerModBlock("haunted_wood", new HauntedPillarBlock(FabricBlockSettings.copy(Blocks.OAK_WOOD).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block STRIPPED_HAUNTED_LOG = registerModBlock("stripped_haunted_log", new HauntedPillarBlock(FabricBlockSettings.copy(Blocks.STRIPPED_OAK_LOG).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block STRIPPED_HAUNTED_WOOD = registerModBlock("stripped_haunted_wood", new HauntedPillarBlock(FabricBlockSettings.copy(Blocks.STRIPPED_OAK_WOOD).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_PLANKS = registerModBlock("haunted_planks", new HauntedBlock(FabricBlockSettings.copy(Blocks.OAK_PLANKS).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_BUTTON = registerModBlock("haunted_button", new HauntedStoneButtonBlock(FabricBlockSettings.of(Material.WOOD).strength(2.5f).noCollision()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_PRESSURE_PLATE = registerModBlock("haunted_pressure_plate", new HauntedPressurePlateBlock(PressurePlateBlock.ActivationRule.EVERYTHING, FabricBlockSettings.of(Material.WOOD).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_FENCE = registerModBlock("haunted_fence", new HauntedFenceBlock(FabricBlockSettings.of(Material.WOOD).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_FENCE_GATE = registerModBlock("haunted_fence_gate", new HauntedFenceGateBlock(FabricBlockSettings.of(Material.WOOD).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_SLAB = registerModBlock("haunted_slab", new HauntedSlabBlock(FabricBlockSettings.of(Material.METAL).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_STAIRS = registerModBlock("haunted_stairs", new HauntedStairsBlock(ModBlocks.HAUNTED_PLANKS.getDefaultState(), FabricBlockSettings.of(Material.WOOD).strength(2.5f)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_DOOR = registerModBlock("haunted_door", new HauntedDoorBlock(FabricBlockSettings.of(Material.WOOD).strength(2.5f).nonOpaque()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_TRAPDOOR = registerModBlock("haunted_trapdoor", new HauntedTrapdoorBlock(FabricBlockSettings.of(Material.WOOD).strength(2.5f).nonOpaque()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_WALL_SIGN_BLOCK = registerModBlockWithoutBlockItem("haunted_wall_sign", new HauntedWallSignBlock(FabricBlockSettings.copy(Blocks.OAK_WALL_SIGN), ModSignTypes.HAUNTED), ModItemGroup.GLACIAL_ABYSS);
    public static final Block HAUNTED_SIGN_BLOCK = registerModBlockWithoutBlockItem("haunted_sign", new HauntedSignBlock(FabricBlockSettings.copy(Blocks.OAK_SIGN), ModSignTypes.HAUNTED), ModItemGroup.GLACIAL_ABYSS);

    public static final Block FROSTED_FLOWER = registerModBlock("frosted_flower", new ModFlowerBlock(StatusEffects.WITHER, 12, FabricBlockSettings.copy(Blocks.DANDELION).strength(2.5f).breakInstantly().nonOpaque()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block POTTED_FROSTED_FLOWER = registerModBlockWithoutBlockItem("potted_frosted_flower", new ModFlowerPotBlock(ModBlocks.FROSTED_FLOWER, FabricBlockSettings.copy(Blocks.POTTED_DANDELION).breakInstantly().nonOpaque()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block FROSTED_PLANT = registerModBlock("frosted_plant", new ModFlowerBlock(StatusEffects.WITHER, 12, FabricBlockSettings.copy(Blocks.DEAD_BUSH).strength(2.5f).breakInstantly().nonOpaque()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block POTTED_FROSTED_PLANT = registerModBlockWithoutBlockItem("potted_frosted_plant", new ModFlowerPotBlock(ModBlocks.FROSTED_FLOWER, FabricBlockSettings.copy(Blocks.POTTED_DEAD_BUSH).breakInstantly().nonOpaque()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block WITHER_BERRY_VINE = registerModBlockWithoutBlockItem("wither_berry_vine", new WitherBerryVineHeadBlock(FabricBlockSettings.copy(Blocks.CAVE_VINES).ticksRandomly().noCollision().luminance(WitherBerryVines.getLuminanceSupplier(14)).breakInstantly().sounds(BlockSoundGroup.CAVE_VINES)));
    public static final Block WITHER_BERRY_VINE_PLANT = registerModBlockWithoutBlockItem("wither_berry_vine_plant", new WitherBerryVineBodyBlock(FabricBlockSettings.copy(Blocks.CAVE_VINES_PLANT).noCollision().luminance(WitherBerryVines.getLuminanceSupplier(14)).breakInstantly().sounds(BlockSoundGroup.CAVE_VINES)));
    public static final Block CONTAINED_PHANTOM_HEART = registerModBlock("contained_phantom_heart", new ModBlock(FabricBlockSettings.of(Material.SNOW_BLOCK).strength(0.6f).sounds(BlockSoundGroup.CORAL)), ModItemGroup.GLACIAL_ABYSS);
    public static final Block PARADISE_ROSE = registerModBlock("paradise_rose", new ModFlowerBlock(StatusEffects.WITHER, 12, FabricBlockSettings.copy(Blocks.POPPY).strength(2.5f).breakInstantly().nonOpaque()), ModItemGroup.GLACIAL_ABYSS);
    public static final Block POTTED_PARADISE_ROSE = registerModBlockWithoutBlockItem("potted_paradise_rose", new ModFlowerPotBlock(ModBlocks.PARADISE_ROSE, FabricBlockSettings.copy(Blocks.POTTED_POPPY).breakInstantly().nonOpaque()), ModItemGroup.GLACIAL_ABYSS);

    public static final Block ABYSSAL_ALTAR = registerModBlock("abyssal_altar", new AbyssalAltarBlock(FabricBlockSettings.of(Material.METAL).strength(10f).requiresTool().nonOpaque()).setHasGlint(true), ModItemGroup.GLACIAL_ABYSS);
    public static final Block REMOLDER = registerModBlock("remolder", new RemolderBlock(FabricBlockSettings.of(Material.METAL).nonOpaque()), ModItemGroup.GLACIAL_ABYSS);

    private static Block registerBlock(String name, Block block) {
        return registerBlock(name, block, ModItemGroup.GLACIAL_ABYSS);
    }

    private static Block registerBlock(String name, Block block, ItemGroup group) {
        registerBlockItem(name, block, group);
        return Registry.register(Registry.BLOCK, new Identifier(GlacialAbyss.MOD_ID, name), block);
    }

    private static Block registerModBlock(String name, IModUsable block) {
        return registerModBlock(name, block, ModItemGroup.GLACIAL_ABYSS);
    }

    private static Block registerModBlock(String name, IModUsable block, ItemGroup group) {
        registerModBlockItem(name, block, group);
        return Registry.register(Registry.BLOCK, new Identifier(GlacialAbyss.MOD_ID, name), (Block) block);
    }

    private static BlockItem registerBlockItem(String name, Block block) {
        return registerBlockItem(name, block, ModItemGroup.GLACIAL_ABYSS);
    }

    private static BlockItem registerBlockItem(String name, Block block, ItemGroup group) {
        return Registry.register(Registry.ITEM, new Identifier(GlacialAbyss.MOD_ID, name), new BlockItem(block, new FabricItemSettings().group(group)));
    }

    private static BlockItem registerModBlockItem(String name, IModUsable block) {
        return registerModBlockItem(name, block, ModItemGroup.GLACIAL_ABYSS);
    }

    private static ModBlockItem registerModBlockItem(String name, IModUsable block, ItemGroup group) {
        return Registry.register(Registry.ITEM, new Identifier(GlacialAbyss.MOD_ID, name), new ModBlockItem((Block) block, new FabricItemSettings().group(group)).setHasGlint(block.hasGlint()));
    }

    private static Block registerBlockWithoutBlockItem(String name, Block block) {
        return registerBlockWithoutBlockItem(name, block, ModItemGroup.GLACIAL_ABYSS);
    }

    private static Block registerBlockWithoutBlockItem(String name, Block block, ItemGroup group) {
        return Registry.register(Registry.BLOCK, new Identifier(GlacialAbyss.MOD_ID, name), block);
    }

    private static Block registerModBlockWithoutBlockItem(String name, IModUsable block) {
        return registerModBlockWithoutBlockItem(name, block, ModItemGroup.GLACIAL_ABYSS);
    }

    private static Block registerModBlockWithoutBlockItem(String name, IModUsable block, ItemGroup group) {
        return Registry.register(Registry.BLOCK, new Identifier(GlacialAbyss.MOD_ID, name), (Block) block);
    }

    public static void register() {
        GlacialAbyss.LOGGER.info("Registering Mod Blocks for " + GlacialAbyss.MOD_ID);
    }
}
