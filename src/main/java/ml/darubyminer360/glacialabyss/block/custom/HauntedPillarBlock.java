package ml.darubyminer360.glacialabyss.block.custom;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.item.ModItems;
import ml.darubyminer360.glacialabyss.item.custom.KeyItem;
import ml.darubyminer360.glacialabyss.util.KeyShape;
import ml.darubyminer360.glacialabyss.util.ModComponents;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.ShapeContext;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.state.StateManager;
import net.minecraft.state.property.BooleanProperty;
import net.minecraft.state.property.IntProperty;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Hand;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Direction;
import net.minecraft.util.shape.VoxelShape;
import net.minecraft.util.shape.VoxelShapes;
import net.minecraft.world.BlockView;
import net.minecraft.world.World;

import java.util.ArrayList;
import java.util.List;

public class HauntedPillarBlock extends ModPillarBlock {
    public static final IntProperty ARENA_INFO = IntProperty.of("arena_info", 0, 3);

    public HauntedPillarBlock(Settings settings) {
        super(settings);
        setDefaultState(getStateManager().getDefaultState().with(AXIS, Direction.Axis.Y).with(ARENA_INFO, 0));
    }

    @Override
    protected void appendProperties(StateManager.Builder<Block, BlockState> builder) {
        super.appendProperties(builder);
        builder.add(ARENA_INFO);
    }

    @Override
    public VoxelShape getCollisionShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        if (world instanceof World) {
            long time = ((World) world).getTimeOfDay();
            if (time > 13000 && time < 23000) {
                boolean defeatedPhantasm = ModComponents.DEFEATED_PHANTASM.get(((World) world).getLevelProperties()).getValue();
                if (state.get(ARENA_INFO) != 1 || defeatedPhantasm) {
                    if (state.get(ARENA_INFO) != 2 || defeatedPhantasm) {
                        return VoxelShapes.empty();
                    }
                }
            }
        }
        return super.getCollisionShape(state, world, pos, context);
    }

    @Override
    public VoxelShape getOutlineShape(BlockState state, BlockView world, BlockPos pos, ShapeContext context) {
        if (world instanceof World) {
            long time = ((World) world).getTimeOfDay();
            if (time > 13000 && time < 23000) {
                boolean defeatedPhantasm = ModComponents.DEFEATED_PHANTASM.get(((World) world).getLevelProperties()).getValue();
                if (state.get(ARENA_INFO) != 1 || defeatedPhantasm) {
                    if (state.get(ARENA_INFO) != 2 || defeatedPhantasm) {
                        return VoxelShapes.empty();
                    }
                }
            }
        }
        return super.getOutlineShape(state, world, pos, context);
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        if (player.getStackInHand(hand).getItem() == ModItems.CRYOGENIC_KEY && player.getStackInHand(hand).hasNbt() && KeyShape.valueOf(player.getStackInHand(hand).getNbt().getString(GlacialAbyss.MOD_ID + ".shape")) == KeyShape.CRYSTAL) {
            unlockAll(state, world, pos, player, hand, hit, new ArrayList<>());
        }

        return super.onUse(state, world, pos, player, hand, hit);
    }

    public void unlockAll(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit, List<BlockPos> previousPositions) {
        List<BlockPos> positions = new ArrayList<>();

        positions.add(new BlockPos(pos.getX() - 1, pos.getY() - 1, pos.getZ() - 1));
        positions.add(new BlockPos(pos.getX() - 1, pos.getY() - 1, pos.getZ()));
        positions.add(new BlockPos(pos.getX(), pos.getY() - 1, pos.getZ() - 1));
        positions.add(new BlockPos(pos.getX(), pos.getY() - 1, pos.getZ()));
        positions.add(new BlockPos(pos.getX() + 1, pos.getY() - 1, pos.getZ() + 1));
        positions.add(new BlockPos(pos.getX() + 1, pos.getY() - 1, pos.getZ()));
        positions.add(new BlockPos(pos.getX(), pos.getY() - 1, pos.getZ() + 1));

        positions.add(new BlockPos(pos.getX() - 1, pos.getY(), pos.getZ() - 1));
        positions.add(new BlockPos(pos.getX() - 1, pos.getY(), pos.getZ()));
        positions.add(new BlockPos(pos.getX(), pos.getY(), pos.getZ() - 1));
        positions.add(new BlockPos(pos.getX(), pos.getY(), pos.getZ()));
        positions.add(new BlockPos(pos.getX() + 1, pos.getY(), pos.getZ() + 1));
        positions.add(new BlockPos(pos.getX() + 1, pos.getY(), pos.getZ()));
        positions.add(new BlockPos(pos.getX(), pos.getY(), pos.getZ() + 1));

        positions.add(new BlockPos(pos.getX() - 1, pos.getY() + 1, pos.getZ() - 1));
        positions.add(new BlockPos(pos.getX() - 1, pos.getY() + 1, pos.getZ()));
        positions.add(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() - 1));
        positions.add(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ()));
        positions.add(new BlockPos(pos.getX() + 1, pos.getY() + 1, pos.getZ() + 1));
        positions.add(new BlockPos(pos.getX() + 1, pos.getY() + 1, pos.getZ()));
        positions.add(new BlockPos(pos.getX(), pos.getY() + 1, pos.getZ() + 1));

        List<BlockPos> oldPositions = new ArrayList<>(previousPositions);
        previousPositions.addAll(positions);
        for (BlockPos blockPos : positions) {
            if (world.getBlockState(blockPos).getBlock() instanceof HauntedPillarBlock && world.getBlockState(blockPos).get(ARENA_INFO) == 2) {
                world.setBlockState(blockPos, world.getBlockState(blockPos).with(ARENA_INFO, 3));
                if (!oldPositions.contains(blockPos) && blockPos != pos) {
                    unlockAll(world.getBlockState(blockPos), world, blockPos, player, hand, hit, previousPositions);
                }
            }
        }
    }
}
