package ml.darubyminer360.glacialabyss.block.custom;

public class GlacialAbyssBlockWithEntity extends ModBlockWithEntity {
    public GlacialAbyssBlockWithEntity(Settings settings) {
        super(settings.luminance(state -> GlacialAbyssBlock.luminance));
    }
}
