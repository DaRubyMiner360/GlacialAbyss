package ml.darubyminer360.glacialabyss.block.custom;

import ml.darubyminer360.glacialabyss.util.IModUsable;
import net.minecraft.block.WallSignBlock;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.text.Text;
import net.minecraft.util.SignType;
import net.minecraft.world.BlockView;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class ModWallSignBlock extends WallSignBlock implements IModUsable {
    private boolean hasGlint = false;
    private List<Text> tooltip = new ArrayList<>();

    public ModWallSignBlock(Settings settings, SignType signType) {
        super(settings, signType);
    }

    @Override
    public void appendTooltip(ItemStack stack, @Nullable BlockView world, List<Text> tooltip, TooltipContext options) {
        super.appendTooltip(stack, world, tooltip, options);
        tooltip.addAll(this.tooltip);
    }

    @Override
    public ModWallSignBlock setTooltip(List<Text> lines) {
        this.tooltip = lines;
        return this;
    }

    @Override
    public ModWallSignBlock addTooltip(Text line) {
        this.tooltip.add(line);
        return this;
    }

    @Override
    public List<Text> getTooltip() {
        return this.tooltip;
    }

    @Override
    public boolean hasGlint() {
        return this.hasGlint;
    }

    @Override
    public boolean hasGlint(ItemStack stack) {
//        if (this.hasGlint()) {
//            return true;
//        }
        return this.asItem().hasGlint(stack);
    }

    @Override
    public ModWallSignBlock setHasGlint(boolean value) {
        this.hasGlint = value;
        return this;
    }
}
