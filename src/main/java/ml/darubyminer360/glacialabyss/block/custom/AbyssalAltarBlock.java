package ml.darubyminer360.glacialabyss.block.custom;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.block.ModBlocks;
import ml.darubyminer360.glacialabyss.block.entity.AbyssalAltarBlockEntity;
import ml.darubyminer360.glacialabyss.block.entity.ModBlockEntities;
import ml.darubyminer360.glacialabyss.event.AbyssalAltarCustomCraftActionsCallback;
import ml.darubyminer360.glacialabyss.event.AbyssalAltarFinishCraftCallback;
import ml.darubyminer360.glacialabyss.event.AbyssalAltarStartCraftCallback;
import ml.darubyminer360.glacialabyss.recipe.AbyssalAltarRecipe;
import ml.darubyminer360.glacialabyss.util.Utils;
import net.minecraft.block.BlockEntityProvider;
import net.minecraft.block.BlockRenderType;
import net.minecraft.block.BlockState;
import net.minecraft.block.Blocks;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityTicker;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.damage.DamageSource;
import net.minecraft.entity.effect.StatusEffectInstance;
import net.minecraft.entity.effect.StatusEffects;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.fluid.Fluids;
import net.minecraft.item.ItemStack;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.ActionResult;
import net.minecraft.util.Formatting;
import net.minecraft.util.Hand;
import net.minecraft.util.ItemScatterer;
import net.minecraft.util.hit.BlockHitResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Random;

public class AbyssalAltarBlock extends GlacialAbyssBlockWithEntity implements BlockEntityProvider {
    public AbyssalAltarBlock(Settings settings) {
        super(settings);
    }

    @Override
    public BlockRenderType getRenderType(BlockState state) {
        return BlockRenderType.MODEL;
    }

    @Override
    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
        ItemStack item = player.getStackInHand(hand);

//        if (item == null || item == ItemStack.EMPTY) {
//            return ActionResult.FAIL;
//        }
        if (GlacialAbyss.frozenPlayers.contains(player.getUuid())) {
            return ActionResult.FAIL;
        }

        AbyssalAltarBlockEntity entity = null;
        Optional<AbyssalAltarRecipe> recipe = Optional.empty();
//        if (!world.isClient()) {
            BlockEntity blockEntity = world.getBlockEntity(pos);
            if (blockEntity instanceof AbyssalAltarBlockEntity) {
                if (!world.isClient()) {
                    entity = (AbyssalAltarBlockEntity) blockEntity;
                    List<BlockPos> surroundingBlocks = getSurroundingBlocks(pos);
                    for (int i = 0; i < surroundingBlocks.size(); i++) {
                        entity.setStack(i, world.getBlockState(surroundingBlocks.get(i)).getBlock().asItem().getDefaultStack());
                    }
                    ItemStack itemStack = ItemStack.EMPTY;
                    if (entity.getMainItem() != null && entity.getMainItem() != ItemStack.EMPTY) {
                        itemStack = entity.getMainItem().copy();
                    }
                    entity.setStack(surroundingBlocks.size(), item);
                    player.setStackInHand(hand, itemStack);
                }

                if (AbyssalAltarBlockEntity.hasRecipe(entity) || world.isClient()) {
                    recipe = AbyssalAltarBlockEntity.getRecipe(entity);

                    return doCraftingAction(state, world, pos, player, item, entity, recipe);
                }
                else if (item != null && item != ItemStack.EMPTY) {
                    if (!world.isClient()) {
                        player.sendMessage(new TranslatableText("message.glacialabyss.invalid_portal_" + (new Random().nextInt(2) + 1)).formatted(Formatting.DARK_RED), false);

                        if (GlacialAbyss.CONFIG.altar_punishment.replace_blocks) {
                            setFailedSurroundingBlocks(world, pos, true);
                        }
                        if (GlacialAbyss.CONFIG.altar_punishment.strike_lightning) {
                            Utils.strikeLightning(world, pos, 30);
                        }

                        if (GlacialAbyss.CONFIG.altar_punishment.do_damage) {
                            player.damage(DamageSource.MAGIC, new Random().nextFloat(player.getMaxHealth()));
                        }
                    }
                }
            }
//        }

        return ActionResult.SUCCESS;
    }

    @Override
    public void onStateReplaced(BlockState state, World world, BlockPos pos, BlockState newState, boolean moved) {
        if (state.getBlock() != newState.getBlock()) {
            BlockEntity blockEntity = world.getBlockEntity(pos);
            if (blockEntity instanceof AbyssalAltarBlockEntity) {
                for (int i = 0; i < getSurroundingBlocks(pos).size(); i++) {
                    ((AbyssalAltarBlockEntity) blockEntity).getItems().set(i, ItemStack.EMPTY);
                }
                ItemScatterer.spawn(world, pos, (AbyssalAltarBlockEntity) blockEntity);
                world.updateComparators(pos,this);
            }
            super.onStateReplaced(state, world, pos, newState, moved);
        }
    }

    @Nullable
    @Override
    public BlockEntity createBlockEntity(BlockPos pos, BlockState state) {
        return new AbyssalAltarBlockEntity(pos, state);
    }

    @Nullable
    @Override
    public <T extends BlockEntity> BlockEntityTicker<T> getTicker(World world, BlockState state, BlockEntityType<T> type) {
        return checkType(type, ModBlockEntities.ABYSSAL_ALTAR_ENTITY, AbyssalAltarBlockEntity::tick);
    }

    public static BlockPos getDest(BlockPos pos, World destWorld) {
        double y = pos.getY();

        BlockPos destPos = new BlockPos(pos.getX(), y, pos.getZ());
        int tries = 0;

        boolean attempting = false;
        while ((!destWorld.getBlockState(destPos).isAir() && !destWorld.getBlockState(destPos)
                .canBucketPlace(Fluids.WATER)) &&
                (!destWorld.getBlockState(destPos.up()).isAir() && !destWorld.getBlockState(destPos.up())
                        .canBucketPlace(Fluids.WATER)) && tries < 25) {
            destPos = destPos.up(2);
            tries++;
        }

//        for (int i = -2; i <= 2; i++) {
//            for (int o = -2; o <= 2; o++) {
//                attempting = attempting || (!destWorld.getBlockState(destPos.add(i, 0, o)).isAir() && !destWorld.getBlockState(destPos.add(i, 0, o))
//                        .canBucketPlace(Fluids.WATER)) &&
//                        (!destWorld.getBlockState(destPos.add(i, 0, o).up()).isAir() && !destWorld.getBlockState(destPos.add(i, 0, o).up())
//                                .canBucketPlace(Fluids.WATER));
//            }
//        }
//
//        while (attempting && tries < 25) {
//            attempting = false;
//            for (int i = -2; i <= 2; i++) {
//                for (int o = -2; o <= 2; o++) {
//                    attempting = attempting || (!destWorld.getBlockState(destPos.add(i, 0, o)).isAir() && !destWorld.getBlockState(destPos.add(i, 0, o))
//                            .canBucketPlace(Fluids.WATER)) &&
//                            (!destWorld.getBlockState(destPos.add(i, 0, o).up()).isAir() && !destWorld.getBlockState(destPos.add(i, 0, o).up())
//                                    .canBucketPlace(Fluids.WATER));
//                }
//            }
//
//            destPos = destPos.up(3);
//            tries++;
//        }

        return destPos;
    }

    public ActionResult doCraftingAction(BlockState state, World world, BlockPos pos, PlayerEntity player, ItemStack item, AbyssalAltarBlockEntity entity, Optional<AbyssalAltarRecipe> recipe) {
        ActionResult startCraftEventResult = AbyssalAltarStartCraftCallback.EVENT.invoker().startCraft(state, world, pos, player, item, entity, recipe);
        if (startCraftEventResult == ActionResult.FAIL) {
            return ActionResult.FAIL;
        }

        new Thread(() -> {
            try {
                if (!world.isClient()) {
                    List<String> messages = recipe.get().getMessages();
                    String message = messages.get(new Random().nextInt(messages.size()));
//                player.sendMessage(new TranslatableText("message.glacialabyss.enter_portal_" + (new Random().nextInt(2) + 1)).formatted(Formatting.DARK_RED), false);
                    player.sendMessage(new TranslatableText(message).formatted(Formatting.DARK_RED), false);
                }

                int fireTicks = player.getFireTicks();
                int frozenTicks = player.getFrozenTicks();
                if (!world.isClient()) {
                    if (recipe.get().getStopPlayer()) {
//                    player.addStatusEffect(new StatusEffectInstance(StatusEffects.BLINDNESS, 146, 255, true, false, false));
                        player.addStatusEffect(new StatusEffectInstance(StatusEffects.BLINDNESS, ((entity.getMaxProgress() - entity.getProgress()) / 1000 * 20) + 26, 255, true, false, false));
                    }
                }
                Thread.sleep(300);
                if (!world.isClient()) {
                    if (recipe.get().getStopPlayer()) {
                        player.setFireTicks(0);
                        player.setFrozenTicks(0);
                        GlacialAbyss.frozenPlayers.add(player.getUuid());
                    }
                }

                if (!world.isClient()) {
                    Thread.sleep(entity.getMaxProgress() - entity.getProgress());
                    if (recipe.get().getStopPlayer()) {
                        player.setFireTicks(fireTicks);
                        player.setFrozenTicks(frozenTicks);
                    }
                }

                ActionResult customCraftActionsEventResult = AbyssalAltarCustomCraftActionsCallback.EVENT.invoker().runCustomActions(state, world, pos, player, item, entity, recipe);

                AbyssalAltarRecipe.Result result = new AbyssalAltarRecipe.Result();
                if (recipe.isPresent()) {
                    result = recipe.get().getResult();
                }
                ActionResult finishCraftEventResult = AbyssalAltarFinishCraftCallback.EVENT.invoker().finishCraft(state, world, pos, player, item, result, entity, recipe);
                
                if (!world.isClient()) {
                    entity.setMainItem(result.item);

                    if (finishCraftEventResult != ActionResult.FAIL) {
                        if (!result.requiresCollection) {
                            player.getInventory().offerOrDrop(entity.getMainItem());
                            entity.setMainItem(ItemStack.EMPTY);
                        }
                    }

                    if (recipe.get().getStopPlayer()) {
                        GlacialAbyss.frozenPlayers.remove(player.getUuid());
                    }
                }
            } catch (InterruptedException ignored) {}
        }).start();

//        if (Objects.equals(recipe.getId().getPath(), "abyssal_portal")) {
//            if (world.getRegistryKey() == ModDimensions.GLACIAL_ABYSS_KEY) {
////                new Thread(() -> {
////                    try {
////                        player.sendMessage(new TranslatableText("message.glacialabyss.create_portal_" + (new Random().nextInt(2) + 1)).formatted(Formatting.DARK_RED), false);
////                        player.addStatusEffect(new StatusEffectInstance(StatusEffects.BLINDNESS, 226, 255, true, false, false));
////                        Thread.sleep(300);
////                        item.setCount(item.getCount() - 1);
////                        int fireTicks = player.getFireTicks();
////                        int frozenTicks = player.getFrozenTicks();
////                        player.setFireTicks(0);
////                        player.setFrozenTicks(0);
////                        GlacialAbyss.travelingPlayers.add(player.getUuid());
////
////                        Thread.sleep(10000);
////                        chargeSurroundingBlocks(world, pos, true);
////                        Utils.strikeLightning(world, pos, 30);
////
////                        player.setFireTicks(fireTicks);
////                        player.setFrozenTicks(frozenTicks);
////                        GlacialAbyss.travelingPlayers.remove(player.getUuid());
////                    } catch (InterruptedException ignored) {}
////                }).start();
//                return ActionResult.SUCCESS;
//            }
//        }
//        if (item.getItem() == ModItems.ABYSSAL_KEY) {
//            if (areSurroundingBlocksChangeable(world, pos)) {
//                new Thread(() -> {
//                    try {
//                        player.sendMessage(new TranslatableText("message.glacialabyss.create_portal_" + (new Random().nextInt(2) + 1)).formatted(Formatting.DARK_RED), false);
//                        player.addStatusEffect(new StatusEffectInstance(StatusEffects.BLINDNESS, 226, 255, true, false, false));
//                        Thread.sleep(300);
//                        item.setCount(item.getCount() - 1);
//                        int fireTicks = player.getFireTicks();
//                        int frozenTicks = player.getFrozenTicks();
//                        player.setFireTicks(0);
//                        player.setFrozenTicks(0);
//                        GlacialAbyss.travelingPlayers.add(player.getUuid());
//
//                        Thread.sleep(10000);
//                        chargeSurroundingBlocks(world, pos, true);
//                        Utils.strikeLightning(world, pos, 30);
//
//                        player.setFireTicks(fireTicks);
//                        player.setFrozenTicks(frozenTicks);
//                        GlacialAbyss.travelingPlayers.remove(player.getUuid());
//                    } catch (InterruptedException ignored) {}
//                }).start();
//
//                return ActionResult.SUCCESS;
//            }
//            else {
//                player.sendMessage(new TranslatableText("message.glacialabyss.invalid_portal_" + (new Random().nextInt(2) + 1)).formatted(Formatting.DARK_RED), false);
//
//                setFailedSurroundingBlocks(world, pos, true);
//                Utils.strikeLightning(world, pos, 30);
//
//                player.damage(DamageSource.MAGIC, new Random().nextFloat(player.getMaxHealth()));
//            }
//        }
//        else {
//            if (world.getRegistryKey() == ModDimensions.GLACIAL_ABYSS_KEY || areSurroundingBlocksValid(world, pos)) {
//                new Thread(() -> {
//                    try {
//                        player.sendMessage(new TranslatableText("message.glacialabyss.enter_portal_" + (new Random().nextInt(2) + 1)).formatted(Formatting.DARK_RED), false);
//                        player.addStatusEffect(new StatusEffectInstance(StatusEffects.BLINDNESS, 146, 255, true, false, false));
//                        Thread.sleep(300);
//                        int fireTicks = player.getFireTicks();
//                        int frozenTicks = player.getFrozenTicks();
//                        player.setFireTicks(0);
//                        player.setFrozenTicks(0);
//                        GlacialAbyss.travelingPlayers.add(player.getUuid());
//
//
//                        Thread.sleep(5000);
//                        player.setFireTicks(fireTicks);
//                        player.setFrozenTicks(frozenTicks);
//
//                        MinecraftServer server = world.getServer();
//                        if (server != null) {
//                            if (player instanceof ServerPlayerEntity) {
//                                ServerPlayerEntity serverPlayer = (ServerPlayerEntity) player;
//                                if (world.getRegistryKey() == ModDimensions.GLACIAL_ABYSS_KEY) {
//                                    ServerWorld overWorld = server.getWorld(World.OVERWORLD);
//                                    if (overWorld != null) {
//                                        BlockPos destPos = getDest(player.getBlockPos().add(0, 1, 0), overWorld, false);
//                                        serverPlayer.teleport(overWorld, destPos.getX(), destPos.getY(), destPos.getZ(),
//                                                serverPlayer.bodyYaw, serverPlayer.prevPitch);
//                                    }
//                                }
//                                else {
//                                    ServerWorld glacialAbyss = server.getWorld(ModDimensions.GLACIAL_ABYSS_KEY);
//                                    if (glacialAbyss != null) {
//                                        BlockPos destPos = getDest(serverPlayer.getBlockPos(), glacialAbyss, true);
//                                        boolean doSetBlock = true;
//                                        for (BlockPos checkPos : BlockPos.iterate(destPos.down(10).west(10).south(10), destPos.up(10).east(10).north(10))) {
//                                            if (glacialAbyss.getBlockState(checkPos).getBlock() == this) {
//                                                doSetBlock = false;
//                                                break;
//                                            }
//                                        }
//                                        if (doSetBlock) {
//                                            glacialAbyss.setBlockState(destPos, this.getDefaultState());
//                                        }
//                                        serverPlayer.teleport(glacialAbyss, destPos.getX(), destPos.getY(), destPos.getZ(),
//                                                serverPlayer.bodyYaw, serverPlayer.prevPitch);
//                                    }
//                                }
//                            }
//                        }
//
//                        GlacialAbyss.travelingPlayers.remove(player.getUuid());
//                    } catch (InterruptedException ignored) {}
//                }).start();
//                return ActionResult.SUCCESS;
//            }
//        }
        return ActionResult.SUCCESS;
    }

    public static List<BlockPos> getSurroundingBlocks(BlockPos pos) {
        List<BlockPos> positions = new ArrayList<>();
        positions.add(pos.add(-2, -1, 0));
//        positions.add(pos.add(-2, -1, -1));
//        positions.add(pos.add(-2, -1, 1));
        positions.add(pos.add(-2, -1, -2));
        positions.add(pos.add(-2, -1, 2));
//        positions.add(pos.add(-1, -1, -2));
//        positions.add(pos.add(-1, -1, 2));
        positions.add(pos.add(0, -1, -2));
        positions.add(pos.add(0, -1, 2));
//        positions.add(pos.add(1, -1, -2));
//        positions.add(pos.add(1, -1, 2));
        positions.add(pos.add(2, -1, -2));
        positions.add(pos.add(2, -1, 2));
//        positions.add(pos.add(2, -1, -1));
//        positions.add(pos.add(2, -1, 1));
        positions.add(pos.add(2, -1, 0));
        return positions;
    }

    private void chargeSurroundingBlocks(World world, BlockPos pos) {
        chargeSurroundingBlocks(world, pos, false);
    }

    private void chargeSurroundingBlocks(World world, BlockPos pos, boolean lightning) {
        for (BlockPos p : getSurroundingBlocks(pos)) {
            setChargedBlock(world, p, lightning);
        }
    }

    private void setFailedSurroundingBlocks(World world, BlockPos pos) {
        setFailedSurroundingBlocks(world, pos, false);
    }

    private void setFailedSurroundingBlocks(World world, BlockPos pos, boolean lightning) {
        for (BlockPos p : getSurroundingBlocks(pos)) {
            setFailedBlock(world, p, lightning);
        }
    }

    private void setFailedBlock(World world, BlockPos pos) {
        setFailedBlock(world, pos, false);
    }

    private void setFailedBlock(World world, BlockPos pos, boolean lightning) {
        int r = new Random().nextInt(4);
        world.setBlockState(pos, (r == 0 ? Blocks.OBSIDIAN : r == 1 ? Blocks.CRYING_OBSIDIAN : r == 2 ? Blocks.MAGMA_BLOCK : Blocks.NETHERRACK).getDefaultState());
        if (lightning) {
            Utils.strikeLightning(world, pos);
        }
    }

    private void setChargedBlock(World world, BlockPos pos) {
        setChargedBlock(world, pos, false);
    }

    private void setChargedBlock(World world, BlockPos pos, boolean lightning) {
        world.setBlockState(pos, ModBlocks.CHARGED_ABYSSAL_BLOCK.getDefaultState());
        if (lightning) {
            Utils.strikeLightning(world, pos);
        }
    }
}
//public class AbyssalAltarBlock extends ModBlock {
//    public AbyssalAltarBlock(Settings settings) {
//        super(settings);
//    }
//
//    @Override
//    public ActionResult onUse(BlockState state, World world, BlockPos pos, PlayerEntity player, Hand hand, BlockHitResult hit) {
//        if (GlacialAbyss.travelingPlayers.contains(player.getUuid())) {
//            return ActionResult.FAIL;
//        }
//
//        ItemStack item;
//        if (hand == Hand.MAIN_HAND) {
//            item = player.getMainHandStack();
//        }
//        else {
//            item = player.getOffHandStack();
//        }
//
//        if (world.isClient()) {
//            if (item.getItem() == ModItems.ABYSSAL_KEY) {
//                return ActionResult.SUCCESS;
//            }
//            else {
//                if (world.getRegistryKey() == ModDimensions.GLACIAL_ABYSS_KEY || areSurroundingBlocksValid(world, pos)) {
//                    return ActionResult.SUCCESS;
//                }
//            }
//            return ActionResult.FAIL;
//        }
//
//        if (item.getItem() == ModItems.ABYSSAL_KEY) {
//            if (areSurroundingBlocksChangeable(world, pos)) {
//                new Thread(() -> {
//                    try {
//                        player.sendMessage(new TranslatableText("message.glacialabyss.create_portal_" + (new Random().nextInt(2) + 1)).formatted(Formatting.DARK_RED), false);
//                        player.addStatusEffect(new StatusEffectInstance(StatusEffects.BLINDNESS, 226, 255, true, false, false));
//                        Thread.sleep(300);
//                        item.setCount(item.getCount() - 1);
//                        int fireTicks = player.getFireTicks();
//                        int frozenTicks = player.getFrozenTicks();
//                        player.setFireTicks(0);
//                        player.setFrozenTicks(0);
//                        GlacialAbyss.travelingPlayers.add(player.getUuid());
//
//                        Thread.sleep(10000);
//                        chargeSurroundingBlocks(world, pos, true);
//                        Utils.strikeLightning(world, pos, 30);
//
//                        player.setFireTicks(fireTicks);
//                        player.setFrozenTicks(frozenTicks);
//                        GlacialAbyss.travelingPlayers.remove(player.getUuid());
//                    } catch (InterruptedException ignored) {}
//                }).start();
//
//                return ActionResult.SUCCESS;
//            }
//            else {
//                player.sendMessage(new TranslatableText("message.glacialabyss.invalid_portal_" + (new Random().nextInt(2) + 1)).formatted(Formatting.DARK_RED), false);
//
//                setFailedSurroundingBlocks(world, pos, true);
//                Utils.strikeLightning(world, pos, 30);
//
//                player.damage(DamageSource.MAGIC, new Random().nextFloat(player.getMaxHealth()));
//            }
//        }
//        else {
//            if (world.getRegistryKey() == ModDimensions.GLACIAL_ABYSS_KEY || areSurroundingBlocksValid(world, pos)) {
//                new Thread(() -> {
//                    try {
//                        player.sendMessage(new TranslatableText("message.glacialabyss.enter_portal_" + (new Random().nextInt(2) + 1)).formatted(Formatting.DARK_RED), false);
//                        player.addStatusEffect(new StatusEffectInstance(StatusEffects.BLINDNESS, 146, 255, true, false, false));
//                        Thread.sleep(300);
//                        int fireTicks = player.getFireTicks();
//                        int frozenTicks = player.getFrozenTicks();
//                        player.setFireTicks(0);
//                        player.setFrozenTicks(0);
//                        GlacialAbyss.travelingPlayers.add(player.getUuid());
//
//
//                        Thread.sleep(5000);
//                        player.setFireTicks(fireTicks);
//                        player.setFrozenTicks(frozenTicks);
//
//                        MinecraftServer server = world.getServer();
//                        if (server != null) {
//                            if (player instanceof ServerPlayerEntity) {
//                                ServerPlayerEntity serverPlayer = (ServerPlayerEntity) player;
//                                if (world.getRegistryKey() == ModDimensions.GLACIAL_ABYSS_KEY) {
//                                    ServerWorld overWorld = server.getWorld(World.OVERWORLD);
//                                    if (overWorld != null) {
//                                        BlockPos destPos = getDest(player.getBlockPos().add(0, 1, 0), overWorld, false);
//                                        serverPlayer.teleport(overWorld, destPos.getX(), destPos.getY(), destPos.getZ(),
//                                                serverPlayer.bodyYaw, serverPlayer.prevPitch);
//                                    }
//                                } else {
//                                    ServerWorld glacialAbyss = server.getWorld(ModDimensions.GLACIAL_ABYSS_KEY);
//                                    if (glacialAbyss != null) {
//                                        BlockPos destPos = getDest(serverPlayer.getBlockPos(), glacialAbyss, true);
//                                        boolean doSetBlock = true;
//                                        for (BlockPos checkPos : BlockPos.iterate(destPos.down(10).west(10).south(10), destPos.up(10).east(10).north(10))) {
//                                            if (glacialAbyss.getBlockState(checkPos).getBlock() == this) {
//                                                doSetBlock = false;
//                                                break;
//                                            }
//                                        }
//                                        if (doSetBlock) {
//                                            glacialAbyss.setBlockState(destPos, this.getDefaultState());
//                                        }
//                                        serverPlayer.teleport(glacialAbyss, destPos.getX(), destPos.getY(), destPos.getZ(),
//                                                serverPlayer.bodyYaw, serverPlayer.prevPitch);
//                                    }
//                                }
//                            }
//                        }
//
//                        GlacialAbyss.travelingPlayers.remove(player.getUuid());
//                    } catch (InterruptedException ignored) {}
//                }).start();
//                return ActionResult.SUCCESS;
//            }
//        }
//        return ActionResult.FAIL;
//    }
//
//    public static BlockPos getDest(BlockPos pos, World destWorld, boolean isInDimension) {
//        double y = 61;
//
//        if (!isInDimension) {
//            y = pos.getY();
//        }
//
//        BlockPos destPos = new BlockPos(pos.getX(), y, pos.getZ());
//        int tries = 0;
//        while ((!destWorld.getBlockState(destPos).isAir() && !destWorld.getBlockState(destPos)
//                .canBucketPlace(Fluids.WATER)) &&
//                (!destWorld.getBlockState(destPos.up()).isAir() && !destWorld.getBlockState(destPos.up())
//                        .canBucketPlace(Fluids.WATER)) && tries < 25) {
//            destPos = destPos.up(3);
//            tries++;
//        }
//
//        return destPos;
//    }
//
//    public List<BlockPos> getSurroundingBlocks(BlockPos pos) {
//        List<BlockPos> positions = new ArrayList<>();
//        positions.add(pos.add(-2, -1, 0));
//        positions.add(pos.add(-2, -1, -1));
//        positions.add(pos.add(-2, -1, 1));
//        positions.add(pos.add(-2, -1, -2));
//        positions.add(pos.add(-2, -1, 2));
//        positions.add(pos.add(-1, -1, -2));
//        positions.add(pos.add(-1, -1, 2));
//        positions.add(pos.add(0, -1, -2));
//        positions.add(pos.add(0, -1, 2));
//        positions.add(pos.add(1, -1, -2));
//        positions.add(pos.add(1, -1, 2));
//        positions.add(pos.add(2, -1, -2));
//        positions.add(pos.add(2, -1, 2));
//        positions.add(pos.add(2, -1, -1));
//        positions.add(pos.add(2, -1, 1));
//        positions.add(pos.add(2, -1, 0));
//        return positions;
//    }
//
//    private boolean areSurroundingBlocksChangeable(World world, BlockPos pos) {
//        for (BlockPos p : getSurroundingBlocks(pos)) {
//            if (!isChangeableBlock(world, p)) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    private boolean areSurroundingBlocksValid(World world, BlockPos pos) {
//        for (BlockPos p : getSurroundingBlocks(pos)) {
//            if (!isValidBlock(world, p)) {
//                return false;
//            }
//        }
//        return true;
//    }
//
//    private void chargeSurroundingBlocks(World world, BlockPos pos) {
//        chargeSurroundingBlocks(world, pos, false);
//    }
//
//    private void chargeSurroundingBlocks(World world, BlockPos pos, boolean lightning) {
//        for (BlockPos p : getSurroundingBlocks(pos)) {
//            setChargedBlock(world, p, lightning);
//        }
//    }
//
//    private void setFailedSurroundingBlocks(World world, BlockPos pos) {
//        setFailedSurroundingBlocks(world, pos, false);
//    }
//
//    private void setFailedSurroundingBlocks(World world, BlockPos pos, boolean lightning) {
//        for (BlockPos p : getSurroundingBlocks(pos)) {
//            setFailedBlock(world, p, lightning);
//        }
//    }
//
//    private boolean isChangeableBlock(World world, BlockPos pos) {
////        return ModTags.Blocks.ABYSSAL_ALTAR_CHANGEABLE_BLOCKS.contains(world.getBlockState(pos).getBlock());
//        return world.getBlockState(pos).isIn(ModTags.Blocks.ABYSSAL_ALTAR_CHANGEABLE_BLOCKS);
//    }
//
//    private boolean isValidBlock(World world, BlockPos pos) {
////        return ModTags.Blocks.ABYSSAL_ALTAR_VALID_BLOCKS.contains(world.getBlockState(pos).getBlock());
//        return world.getBlockState(pos).isIn(ModTags.Blocks.ABYSSAL_ALTAR_VALID_BLOCKS);
//    }
//
//    private void setFailedBlock(World world, BlockPos pos) {
//        setFailedBlock(world, pos, false);
//    }
//
//    private void setFailedBlock(World world, BlockPos pos, boolean lightning) {
//        int r = new Random().nextInt(4);
//        world.setBlockState(pos, (r == 0 ? Blocks.OBSIDIAN : r == 1 ? Blocks.CRYING_OBSIDIAN : r == 2 ? Blocks.MAGMA_BLOCK : Blocks.NETHERRACK).getDefaultState());
//        if (lightning) {
//            Utils.strikeLightning(world, pos);
//        }
//    }
//
//    private void setChargedBlock(World world, BlockPos pos) {
//        setChargedBlock(world, pos, false);
//    }
//
//    private void setChargedBlock(World world, BlockPos pos, boolean lightning) {
//        world.setBlockState(pos, ModBlocks.CHARGED_ABYSSAL_BLOCK.getDefaultState());
//        if (lightning) {
//            Utils.strikeLightning(world, pos);
//        }
//    }
//}
