package ml.darubyminer360.glacialabyss.block.custom;

public class GlacialAbyssBlock extends ModBlock {
    public static int luminance = 6;

    public GlacialAbyssBlock(Settings settings) {
        super(settings.luminance(state -> luminance));
    }
}
