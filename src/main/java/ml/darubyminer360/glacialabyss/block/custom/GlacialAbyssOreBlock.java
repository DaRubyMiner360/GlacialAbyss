package ml.darubyminer360.glacialabyss.block.custom;

import net.minecraft.util.math.intprovider.UniformIntProvider;

public class GlacialAbyssOreBlock extends ModOreBlock {
    public GlacialAbyssOreBlock(Settings settings) {
        super(settings.luminance(state -> GlacialAbyssBlock.luminance));
    }

    public GlacialAbyssOreBlock(Settings settings, UniformIntProvider experienceDropped) {
        super(settings.luminance(state -> GlacialAbyssBlock.luminance), experienceDropped);
    }
}
