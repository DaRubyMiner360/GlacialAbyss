package ml.darubyminer360.glacialabyss.block.custom;

public class GlacialAbyssGrassBlock extends ModGrassBlock {
    public GlacialAbyssGrassBlock(Settings settings) {
        super(settings.luminance(state -> GlacialAbyssBlock.luminance));
    }
}
