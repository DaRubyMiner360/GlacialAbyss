package ml.darubyminer360.glacialabyss.block.custom;

import ml.darubyminer360.glacialabyss.util.IModUsable;
import net.minecraft.block.Block;
import net.minecraft.block.BlockState;
import net.minecraft.block.SaplingBlock;
import net.minecraft.block.sapling.SaplingGenerator;
import net.minecraft.client.item.TooltipContext;
import net.minecraft.item.ItemStack;
import net.minecraft.tag.TagKey;
import net.minecraft.text.Text;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.BlockView;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Supplier;

public class ModSaplingBlock extends SaplingBlock implements IModUsable {
    private boolean hasGlint = false;
    private List<Text> tooltip = new ArrayList<>();

    protected final Supplier<List<Block>> groundBlocks;
    protected final Supplier<List<TagKey<Block>>> groundBlockTags;

    public ModSaplingBlock(SaplingGenerator generator, Settings settings) {
        this(generator, settings, null, null);
    }

    public ModSaplingBlock(SaplingGenerator generator, Settings settings, Supplier<List<Block>> groundBlocks, Supplier<List<TagKey<Block>>> groundBlockTags) {
        super(generator, settings);
        this.groundBlocks = groundBlocks;
        this.groundBlockTags = groundBlockTags;
    }

    @Override
    protected boolean canPlantOnTop(BlockState floor, BlockView world, BlockPos pos) {
        boolean found = false;
        if (groundBlocks == null && groundBlockTags == null) {
            found = super.canPlantOnTop(floor, world, pos);
        }
        if (groundBlocks != null) {
            for (int i = 0; i < groundBlocks.get().size(); i++) {
                found = found || floor.isOf(groundBlocks.get().get(i));
            }
        }
        if (groundBlockTags != null) {
            for (int i = 0; i < groundBlockTags.get().size(); i++) {
                found = found || floor.isIn(groundBlockTags.get().get(i));
            }
        }
        return found;
    }

    @Override
    public void appendTooltip(ItemStack stack, @Nullable BlockView world, List<Text> tooltip, TooltipContext options) {
        super.appendTooltip(stack, world, tooltip, options);
        tooltip.addAll(this.tooltip);
    }

    @Override
    public ModSaplingBlock setTooltip(List<Text> lines) {
        this.tooltip = lines;
        return this;
    }

    @Override
    public ModSaplingBlock addTooltip(Text line) {
        this.tooltip.add(line);
        return this;
    }

    @Override
    public List<Text> getTooltip() {
        return this.tooltip;
    }

    @Override
    public boolean hasGlint() {
        return this.hasGlint;
    }

    @Override
    public boolean hasGlint(ItemStack stack) {
//        if (this.hasGlint()) {
//            return true;
//        }
        return this.asItem().hasGlint(stack);
    }

    @Override
    public ModSaplingBlock setHasGlint(boolean value) {
        this.hasGlint = value;
        return this;
    }
}
