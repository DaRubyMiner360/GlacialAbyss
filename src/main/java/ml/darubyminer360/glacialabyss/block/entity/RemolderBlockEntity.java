package ml.darubyminer360.glacialabyss.block.entity;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.item.ModItems;
import ml.darubyminer360.glacialabyss.screen.RemolderScreenHandler;
import ml.darubyminer360.glacialabyss.util.ImplementedInventory;
import ml.darubyminer360.glacialabyss.util.KeyShape;
import ml.darubyminer360.glacialabyss.util.ModTags;
import net.fabricmc.fabric.api.registry.FuelRegistry;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.NamedScreenHandlerFactory;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.screen.ScreenHandler;
import net.minecraft.text.LiteralText;
import net.minecraft.text.Text;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class RemolderBlockEntity extends BlockEntity implements NamedScreenHandlerFactory, ImplementedInventory {
    private final DefaultedList<ItemStack> inventory =
            DefaultedList.ofSize(4, ItemStack.EMPTY);

    protected final PropertyDelegate propertyDelegate;
    private int progress = 0;
    private int maxProgress = 72;
    private int fuelTime = 0;
    private int maxFuelTime = 0;

    public RemolderBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntities.REMOLDER, pos, state);
        this.propertyDelegate = new PropertyDelegate() {
            public int get(int index) {
                switch (index) {
                    case 0: return RemolderBlockEntity.this.progress;
                    case 1: return RemolderBlockEntity.this.maxProgress;
                    case 2: return RemolderBlockEntity.this.fuelTime;
                    case 3: return RemolderBlockEntity.this.maxFuelTime;
                    default: return 0;
                }
            }

            public void set(int index, int value) {
                switch(index) {
                    case 0: RemolderBlockEntity.this.progress = value; break;
                    case 1: RemolderBlockEntity.this.maxProgress = value; break;
                    case 2: RemolderBlockEntity.this.fuelTime = value; break;
                    case 3: RemolderBlockEntity.this.maxFuelTime = value; break;
                }
            }

            public int size() {
                return 4;
            }
        };
    }

    @Override
    public DefaultedList<ItemStack> getItems() {
        return inventory;
    }

    @Override
    public Text getDisplayName() {
        return new TranslatableText("screen.glacialabyss.remolder.name");
    }

    @Nullable
    @Override
    public ScreenHandler createMenu(int syncId, PlayerInventory inv, PlayerEntity player) {
        return new RemolderScreenHandler(syncId, inv, this, this.propertyDelegate);
    }

    @Override
    protected void writeNbt(NbtCompound nbt) {
        super.writeNbt(nbt);
        Inventories.writeNbt(nbt, inventory);
        nbt.putInt("remolder.progress", progress);
        nbt.putInt("remolder.fuelTime", fuelTime);
        nbt.putInt("remolder.maxFuelTime", maxFuelTime);
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        Inventories.readNbt(nbt, inventory);
        super.readNbt(nbt);
        progress = nbt.getInt("remolder.progress");
        fuelTime = nbt.getInt("remolder.fuelTime");
        maxFuelTime = nbt.getInt("remolder.maxFuelTime");
    }

    private void consumeFuel() {
        if (!getStack(0).isEmpty()) {
            this.fuelTime = FuelRegistry.INSTANCE.get(this.removeStack(0, 1).getItem());
            this.maxFuelTime = this.fuelTime;
        }
    }

    public static void tick(World world, BlockPos pos, BlockState state, RemolderBlockEntity entity) {
        if (isConsumingFuel(entity)) {
            entity.fuelTime--;
        }

        if (hasRecipe(entity)) {
            if (hasFuelInFuelSlot(entity) && !isConsumingFuel(entity)) {
                entity.consumeFuel();
            }
            if (isConsumingFuel(entity)) {
                entity.progress++;
                if (entity.progress > entity.maxProgress) {
                    craftItem(entity);
                }
            }
        } else {
            entity.resetProgress();
        }
    }

    private static boolean hasFuelInFuelSlot(RemolderBlockEntity entity) {
        return !entity.getStack(0).isEmpty();
    }

    private static boolean isConsumingFuel(RemolderBlockEntity entity) {
        return entity.fuelTime > 0;
    }

    private static boolean hasRecipe(RemolderBlockEntity entity) {
        boolean validFirstSlot = false;

//        if (ModTags.Items.CRYSTALS.contains(entity.getStack(1))) {
        if (entity.getStack(1).isIn(ModTags.Items.CRYSTALS)) {
            validFirstSlot = true;
        }
//        if (ModTags.Items.KEY_PARTS.contains(entity.getStack(1))) {
        else if (entity.getStack(1).isIn(ModTags.Items.KEY_PARTS)) {
            validFirstSlot = true;
        }

//        boolean validSecondSlot = ModTags.Items.KEY_PARTS.contains(entity.getStack(2));
        boolean validSecondSlot = entity.getStack(2).isIn(ModTags.Items.KEY_PARTS);


        SimpleInventory inventory = new SimpleInventory(entity.inventory.size());
        for (int i = 0; i < entity.inventory.size(); i++) {
            inventory.setStack(i, entity.getStack(i));
        }

        return validFirstSlot && validSecondSlot && canInsertAmountIntoOutputSlot(inventory);
    }

    private static void craftItem(RemolderBlockEntity entity) {
        KeyShape newShape = KeyShape.KEY;
        boolean validFirstSlot = false;

//        if (ModTags.Items.CRYSTALS.contains(entity.getStack(1))) {
        if (entity.getStack(1).isIn(ModTags.Items.CRYSTALS)) {
            newShape = KeyShape.CRYSTAL;
            validFirstSlot = true;
        }
//        if (ModTags.Items.KEY_PARTS.contains(entity.getStack(1))) {
        else if (entity.getStack(1).isIn(ModTags.Items.KEY_PARTS)) {
            newShape = KeyShape.KEY;
            validFirstSlot = true;
        }

//        boolean validSecondSlot = ModTags.Items.KEY_PARTS.contains(entity.getStack(2));
        boolean validSecondSlot = entity.getStack(2).isIn(ModTags.Items.KEY_PARTS);

        if (validFirstSlot && validSecondSlot) {
            ItemStack result = entity.getStack(2).copy();
            NbtCompound nbt = result.getOrCreateNbt();
            nbt.putString(GlacialAbyss.MOD_ID + ".shape", newShape.name());
            result.setNbt(nbt);
            entity.setStack(3, result);
            entity.removeStack(1, 1);
            entity.removeStack(2, 1);

            entity.resetProgress();
        }
    }

    private void resetProgress() {
        this.progress = 0;
    }

    private static boolean canInsertItemIntoOutputSlot(SimpleInventory inventory, ItemStack output) {
        return inventory.getStack(3).getItem() == output.getItem() || inventory.getStack(3).isEmpty();
    }

    private static boolean canInsertAmountIntoOutputSlot(SimpleInventory inventory) {
        return inventory.getStack(3).getMaxCount() > inventory.getStack(3).getCount();
    }
}
