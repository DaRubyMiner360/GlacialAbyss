package ml.darubyminer360.glacialabyss.block.entity;

import ml.darubyminer360.glacialabyss.block.ModBlocks;
import ml.darubyminer360.glacialabyss.block.custom.AbyssalAltarBlock;
import ml.darubyminer360.glacialabyss.util.ImplementedInventory;
import ml.darubyminer360.glacialabyss.recipe.AbyssalAltarRecipe;
import net.minecraft.block.BlockState;
import net.minecraft.block.entity.BlockEntity;
import net.minecraft.entity.EntityType;
import net.minecraft.entity.SpawnReason;
import net.minecraft.inventory.Inventories;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NbtCompound;
import net.minecraft.screen.PropertyDelegate;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;

import java.util.List;
import java.util.Optional;

public class AbyssalAltarBlockEntity extends BlockEntity implements ImplementedInventory {
    private final DefaultedList<ItemStack> inventory = DefaultedList.ofSize(9, ItemStack.EMPTY);

    protected final PropertyDelegate propertyDelegate;
    private int progress = 0;

    private List<BlockPos> surroundingBlocks;

    public AbyssalAltarBlockEntity(BlockPos pos, BlockState state) {
        super(ModBlockEntities.ABYSSAL_ALTAR_ENTITY, pos, state);
        this.propertyDelegate = new PropertyDelegate() {
            public int get(int index) {
                return switch (index) {
                    case 0 -> AbyssalAltarBlockEntity.this.progress;
                    default -> 0;
                };
            }

            public void set(int index, int value) {
                switch (index) {
                    case 0 -> AbyssalAltarBlockEntity.this.progress = value;
                }
            }

            public int size() {
                return 2;
            }
        };

        if (state.getBlock() == ModBlocks.ABYSSAL_ALTAR) {
            surroundingBlocks = AbyssalAltarBlock.getSurroundingBlocks(pos);
        }
    }

    @Override
    public DefaultedList<ItemStack> getItems() {
        return inventory;
    }

    public ItemStack getMainItem() {
        return inventory.get(8);
    }

    public void setMainItem(ItemStack itemStack) {
        inventory.set(surroundingBlocks.size(), itemStack);
        this.setStack(surroundingBlocks.size(), itemStack);
    }

    @Override
    public void readNbt(NbtCompound nbt) {
        super.readNbt(nbt);
        Inventories.readNbt(nbt, inventory);
    }

    @Override
    public void writeNbt(NbtCompound nbt) {
        Inventories.writeNbt(nbt, inventory);
        super.writeNbt(nbt);
    }

    public static void tick(World world, BlockPos pos, BlockState state, AbyssalAltarBlockEntity entity) {
        List<ItemStack> inventory = entity.inventory.stream().toList();
        if (state.getBlock() == ModBlocks.ABYSSAL_ALTAR) {
            for (int i = 0; i < entity.surroundingBlocks.size(); i++) {
                entity.inventory.set(i, world.getBlockState(entity.surroundingBlocks.get(i)).getBlock().asItem().getDefaultStack());
            }
        }

        Optional<AbyssalAltarRecipe> recipe = getRecipe(entity);
        if (recipe.isPresent()) {

            if (entity.inventory != inventory) {
                entity.resetProgress();
            }

            entity.progress++;
            if (entity.progress > recipe.get().getTime()) {
                craftItem(entity);
            }
        }
        else {

            entity.resetProgress();
        }
    }

    public static Optional<AbyssalAltarRecipe> getRecipe(AbyssalAltarBlockEntity entity) {
        if (entity != null) {
            World world = entity.world;
            SimpleInventory inventory = new SimpleInventory(entity.inventory.size());
            for (int i = 0; i < entity.inventory.size(); i++) {
                inventory.setStack(i, entity.getStack(i));
            }

            Optional<AbyssalAltarRecipe> match = world.getRecipeManager()
                    .getFirstMatch(AbyssalAltarRecipe.Type.INSTANCE, inventory, world);

            return match;
        }
        return Optional.empty();
    }

    public static boolean hasRecipe(AbyssalAltarBlockEntity entity) {
        return getRecipe(entity).isPresent();
    }

    private static void craftItem(AbyssalAltarBlockEntity entity) {
        World world = entity.world;
        SimpleInventory inventory = new SimpleInventory(entity.inventory.size());
        for (int i = 0; i < entity.inventory.size(); i++) {
            inventory.setStack(i, entity.getStack(i));
        }

        Optional<AbyssalAltarRecipe> match = world.getRecipeManager().getFirstMatch(AbyssalAltarRecipe.Type.INSTANCE, inventory, world);

        if (match.isPresent()) {
            entity.setStack(entity.surroundingBlocks.size(), new ItemStack(match.get().getOutput().getItem(), entity.getStack(entity.surroundingBlocks.size()).getCount()));

            if (!world.isClient()) {
                EntityType.LIGHTNING_BOLT.spawn((ServerWorld) world, null, null, null, entity.pos, SpawnReason.TRIGGERED, true, true);
            }

            entity.resetProgress();
        }
    }

    public int getProgress() {
        return progress;
    }

    public int getMaxProgress() {
        return hasRecipe(this) ? getRecipe(this).get().getTime() : AbyssalAltarRecipe.Serializer.INSTANCE.time;
    }

    public void resetProgress() {
        this.progress = 0;
    }
}
