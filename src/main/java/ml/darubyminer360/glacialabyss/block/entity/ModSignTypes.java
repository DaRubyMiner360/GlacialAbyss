package ml.darubyminer360.glacialabyss.block.entity;

import ml.darubyminer360.glacialabyss.mixin.SignTypeAccessor;
import net.minecraft.util.SignType;

public class ModSignTypes {
    public static final SignType FROSTED = SignTypeAccessor.registerNew(SignTypeAccessor.newSignType("frosted"));
    public static final SignType HAUNTED = SignTypeAccessor.registerNew(SignTypeAccessor.newSignType("haunted"));
}
