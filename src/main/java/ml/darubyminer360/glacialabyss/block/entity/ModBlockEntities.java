package ml.darubyminer360.glacialabyss.block.entity;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import ml.darubyminer360.glacialabyss.block.ModBlocks;
import net.fabricmc.fabric.api.object.builder.v1.block.entity.FabricBlockEntityTypeBuilder;
import net.minecraft.block.entity.BlockEntityType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModBlockEntities {
//    public static final BlockEntityType<AbyssalAltarBlockEntity> ABYSSAL_ALTAR_ENTITY = registerBlockEntity("abyssal_altar_entity", AbyssalAltarBlockEntity::new, ABYSSAL_ALTAR);
    public static final BlockEntityType<AbyssalAltarBlockEntity> ABYSSAL_ALTAR_ENTITY = Registry.register(Registry.BLOCK_ENTITY_TYPE, new Identifier(GlacialAbyss.MOD_ID, "abyssal_altar_entity"), FabricBlockEntityTypeBuilder.create(AbyssalAltarBlockEntity::new, ModBlocks.ABYSSAL_ALTAR).build(null));
    public static final BlockEntityType<RemolderBlockEntity> REMOLDER = Registry.register(Registry.BLOCK_ENTITY_TYPE, new Identifier(GlacialAbyss.MOD_ID, "remolder_entity"), FabricBlockEntityTypeBuilder.create(RemolderBlockEntity::new, ModBlocks.REMOLDER).build(null));

    public static void register() {
        GlacialAbyss.LOGGER.info("Registering Mod Block Entities for " + GlacialAbyss.MOD_ID);
    }
}
