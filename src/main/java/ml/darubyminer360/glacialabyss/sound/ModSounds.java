package ml.darubyminer360.glacialabyss.sound;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import net.minecraft.sound.BlockSoundGroup;
import net.minecraft.sound.SoundEvent;
import net.minecraft.sound.SoundEvents;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModSounds {
    // Sound Effects
    public static SoundEvent DRIFTING_AWAY_SHORT = registerSoundEvent("drifting_away_short");

    // Normal Music
    public static SoundEvent DRIFTING_AWAY = registerSoundEvent("drifting_away");

    // Boss Music
    public static SoundEvent PHANTASM = registerSoundEvent("phantasm");
    public static SoundEvent PARADISE_KING = registerSoundEvent("paradise_king");
    public static SoundEvent HAUNTED_PALADIN = registerSoundEvent("haunted_paladin");

    private static SoundEvent registerSoundEvent(String name) {
        Identifier id = new Identifier(GlacialAbyss.MOD_ID, name);
        return Registry.register(Registry.SOUND_EVENT, id, new SoundEvent(id));
    }
}
