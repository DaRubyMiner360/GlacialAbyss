package ml.darubyminer360.glacialabyss;

import me.shedaniel.autoconfig.AutoConfig;
import me.shedaniel.autoconfig.serializer.GsonConfigSerializer;
import ml.darubyminer360.glacialabyss.advancement.criterion.ModCriteria;
import ml.darubyminer360.glacialabyss.block.ModBlocks;
import ml.darubyminer360.glacialabyss.block.custom.AbyssalAltarBlock;
import ml.darubyminer360.glacialabyss.block.entity.ModBlockEntities;
import ml.darubyminer360.glacialabyss.effect.ModEffects;
import ml.darubyminer360.glacialabyss.entity.ModEntities;
import ml.darubyminer360.glacialabyss.entity.boss.phantasm.PhantasmEntity;
import ml.darubyminer360.glacialabyss.event.AbyssalAltarCustomCraftActionsCallback;
import ml.darubyminer360.glacialabyss.event.AbyssalAltarFinishCraftCallback;
import ml.darubyminer360.glacialabyss.item.ModItems;
import ml.darubyminer360.glacialabyss.particle.ModParticles;
import ml.darubyminer360.glacialabyss.enchantment.ModEnchantments;
import ml.darubyminer360.glacialabyss.recipe.ModRecipes;
import ml.darubyminer360.glacialabyss.screen.ModScreenHandlers;
import ml.darubyminer360.glacialabyss.sound.ModSounds;
import ml.darubyminer360.glacialabyss.util.GlacialAbyssConfig;
import ml.darubyminer360.glacialabyss.util.KeyShape;
import ml.darubyminer360.glacialabyss.util.ModLootTableModifiers;
import ml.darubyminer360.glacialabyss.util.ModRegistries;
import ml.darubyminer360.glacialabyss.world.ModBiomes;
import ml.darubyminer360.glacialabyss.world.ModDimensions;
import ml.darubyminer360.glacialabyss.world.feature.ModConfiguredFeatures;
import ml.darubyminer360.glacialabyss.world.gen.ModWorldGen;
import net.fabricmc.api.ModInitializer;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.server.world.ServerWorld;
import net.minecraft.sound.SoundCategory;
import net.minecraft.util.ActionResult;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3d;
import net.minecraft.world.World;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.UUID;

public class GlacialAbyss implements ModInitializer {
    public static final String MOD_ID = "glacialabyss";
    public static final Logger LOGGER = LogManager.getLogger(MOD_ID);
    public static GlacialAbyssConfig CONFIG = new GlacialAbyssConfig();

    public static List<UUID> frozenPlayers = new ArrayList<>();
    public static List<UUID> blessedEntities = new ArrayList<>();
    public static List<UUID> blessedCooldownEntities = new ArrayList<>();

    @Override
    public void onInitialize() {
        AutoConfig.register(GlacialAbyssConfig.class, GsonConfigSerializer::new);
        CONFIG = AutoConfig.getConfigHolder(GlacialAbyssConfig.class).getConfig();

        ModConfiguredFeatures.register();

        ModItems.register();
        ModBlocks.register();

        ModRegistries.register();

        ModEffects.register();

        ModBlockEntities.register();
        ModRecipes.register();

        ModParticles.register();
        ModEnchantments.register();

        ModScreenHandlers.register();

        ModCriteria.register();
        ModLootTableModifiers.modifyLootTables();

        ModBiomes.register();
        ModWorldGen.generateModWorldGen();

        handleEvents();
    }

    public void handleEvents() {
        AbyssalAltarFinishCraftCallback.EVENT.register((state, world, pos, player, item, output, entity, recipe) -> {
            if (Objects.equals(recipe.get().getId().getPath(), GlacialAbyss.MOD_ID)) {
                output.item.setNbt(item.getOrCreateNbt().copy());

                return ActionResult.SUCCESS;
            }
            return ActionResult.PASS;
        });

        AbyssalAltarCustomCraftActionsCallback.EVENT.register((state, world, pos, player, item, entity, recipe) -> {
            if (recipe.isEmpty()) {
                return ActionResult.PASS;
            }

            if (Objects.equals(recipe.get().getId().getPath(), "abyssal_portal")) {
//                 if (world.isClient()) {
//                    world.playSound(null, pos, ModSounds.DRIFTING_AWAY_SHORT, SoundCategory.BLOCKS, 1f, 1f);
//                 }
//                 else {
                if (!world.isClient()) {
                    MinecraftServer server = world.getServer();
                    if (server != null && player instanceof ServerPlayerEntity serverPlayer) {
                        world.playSound(null, pos, ModSounds.DRIFTING_AWAY_SHORT, SoundCategory.BLOCKS, 1f, 1f);
                        if (world.getRegistryKey() == ModDimensions.GLACIAL_ABYSS_KEY) {
                            ServerWorld overworld = server.getWorld(World.OVERWORLD);
                            if (overworld != null) {
                                overworld.playSound(null, pos, ModSounds.DRIFTING_AWAY_SHORT, SoundCategory.BLOCKS, 1f, 1f);
                            }
                        }
                        else {
                            ServerWorld glacialAbyss = server.getWorld(ModDimensions.GLACIAL_ABYSS_KEY);
                            if (glacialAbyss != null) {
                                glacialAbyss.playSound(null, pos, ModSounds.DRIFTING_AWAY_SHORT, SoundCategory.BLOCKS, 1f, 1f);
                            }
                        }

                        if (world.getRegistryKey() == ModDimensions.GLACIAL_ABYSS_KEY) {
                            ServerWorld overworld = server.getWorld(World.OVERWORLD);
                            if (overworld != null) {
                                BlockPos destPos = AbyssalAltarBlock.getDest(player.getBlockPos().add(0, 1, 0), overworld);
                                serverPlayer.teleport(overworld, destPos.getX(), destPos.getY() + 1, destPos.getZ(), serverPlayer.bodyYaw, serverPlayer.prevPitch);
                            }
                        }
                        else {
                            ServerWorld glacialAbyss = server.getWorld(ModDimensions.GLACIAL_ABYSS_KEY);
                            if (glacialAbyss != null) {
                                BlockPos destPos = AbyssalAltarBlock.getDest(serverPlayer.getBlockPos(), glacialAbyss);
//                                boolean doSetBlock = true;
//                            for (BlockPos checkPos : BlockPos.iterate(destPos.down(10).west(10).south(10), destPos.up(10).east(10).north(10))) {
//                                if (glacialAbyss.getBlockState(checkPos).getBlock() == ModBlocks.ABYSSAL_ALTAR) {
//                                    doSetBlock = false;
//                                    List<BlockPos> surroundingBlocks = AbyssalAltarBlock.getSurroundingBlocks(pos);
//                                    List<BlockPos> destSurroundingBlocks = AbyssalAltarBlock.getSurroundingBlocks(checkPos);
//                                    for (int i = 0; i < surroundingBlocks.size(); i++) {
//                                        if (glacialAbyss.getBlockState(destSurroundingBlocks.get(i)).getBlock() != world.getBlockState(surroundingBlocks.get(i)).getBlock()) {
//                                            doSetBlock = true;
//                                            break;
//                                        }
//                                    }
//                                    break;
//                                }
//                            }
//                                if (doSetBlock) {
                                    glacialAbyss.setBlockState(destPos, ModBlocks.ABYSSAL_ALTAR.getDefaultState());

                                    List<BlockPos> surroundingBlocks = AbyssalAltarBlock.getSurroundingBlocks(pos);
                                    List<BlockPos> destSurroundingBlocks = AbyssalAltarBlock.getSurroundingBlocks(destPos);
                                    for (int i = 0; i < surroundingBlocks.size(); i++) {
                                        glacialAbyss.setBlockState(destSurroundingBlocks.get(i), world.getBlockState(surroundingBlocks.get(i)));
                                    }
//                                }
                                serverPlayer.teleport(glacialAbyss, destPos.getX(), destPos.getY() + 1, destPos.getZ(), serverPlayer.bodyYaw, serverPlayer.prevPitch);
                            }
                        }
                    }
                    else {
                        world.playSound(null, pos, ModSounds.DRIFTING_AWAY_SHORT, SoundCategory.BLOCKS, 1f, 1f);
                    }
                }

                return ActionResult.SUCCESS;
            }
            else if (Objects.equals(recipe.get().getId().getPath(), "phantasm")) {
                if (!world.isClient()) {
                    if (item.hasNbt() && KeyShape.valueOf(item.getNbt().getString(GlacialAbyss.MOD_ID + ".shape")) == KeyShape.CRYSTAL) {
                        PhantasmEntity boss = new PhantasmEntity(ModEntities.PHANTASM, world);
                        boss.setPosition(new Vec3d(pos.getX(), pos.getY() + 2, pos.getZ()));
                        world.spawnEntity(boss);

                        return ActionResult.SUCCESS;
                    }
                }
            }

            return ActionResult.PASS;
        });
    }
}
