package ml.darubyminer360.glacialabyss.recipe;

import com.google.gson.*;
import net.minecraft.inventory.SimpleInventory;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.network.PacketByteBuf;
import net.minecraft.recipe.*;
import net.minecraft.util.Identifier;
import net.minecraft.util.JsonHelper;
import net.minecraft.util.collection.DefaultedList;
import net.minecraft.util.registry.Registry;
import net.minecraft.world.World;

public class AbyssalAltarRecipe implements Recipe<SimpleInventory> {
    private final Identifier id;
    private final Result result;
    private final DefaultedList<Ingredient> recipeBlocks;
    private final Ingredient recipeItem;
    private final int time;
    private final boolean stopPlayer;
    private final DefaultedList<String> messages;

    public AbyssalAltarRecipe(Identifier id, Result result, DefaultedList<Ingredient> recipeBlocks, Ingredient recipeItem, int time, boolean stopPlayer, DefaultedList<String> messages) {
        this.id = id;
        this.result = result;
        this.recipeBlocks = recipeBlocks;
        this.recipeItem = recipeItem;
        this.time = time;
        this.stopPlayer = stopPlayer;
        this.messages = messages;
    }

    @Override
    public boolean matches(SimpleInventory inventory, World world) {
        return recipeBlocks.get(0).test(inventory.getStack(0)) &&
                recipeBlocks.get(1).test(inventory.getStack(1)) &&
                recipeBlocks.get(2).test(inventory.getStack(2)) &&
                recipeBlocks.get(3).test(inventory.getStack(3)) &&
                recipeBlocks.get(4).test(inventory.getStack(4)) &&
                recipeBlocks.get(5).test(inventory.getStack(5)) &&
                recipeBlocks.get(6).test(inventory.getStack(6)) &&
                recipeBlocks.get(7).test(inventory.getStack(7)) &&
                recipeItem.test(inventory.getStack(8));
    }

    @Override
    public ItemStack craft(SimpleInventory inventory) {
        return result.item;
    }

    @Override
    public boolean fits(int width, int height) {
        return true;
    }

    @Override
    public ItemStack getOutput() {
        return result.item.copy();
    }

    public DefaultedList<Ingredient> getBlocks() {
        return recipeBlocks;
    }
    
    public Ingredient getItem() {
        return recipeItem;
    }

    public Result getResult() {
        return result;
    }

    public int getTime() {
        return time;
    }
    
    public boolean getStopPlayer() {
        return stopPlayer;
    }
    
    public DefaultedList<String> getMessages() {
        return messages;
    }

    public static Result resultFromJson(JsonObject json) {
        Item item = getItem(json);
        if (json.has("data")) {
            throw new JsonParseException("Disallowed data tag found");
        }
        int count = JsonHelper.getInt(json, "count", 1);
        if (count < 1) {
            throw new JsonSyntaxException("Invalid output count: " + count);
        }
        boolean requiresCollection = JsonHelper.getBoolean(json, "requirescollection", true);
        return new Result(new ItemStack(item, count), requiresCollection);
    }

    public static Item getItem(JsonObject json) {
        String string = JsonHelper.getString(json, "item");
        Item item = Registry.ITEM.getOrEmpty(new Identifier(string)).orElseThrow(() -> new JsonSyntaxException("Unknown item '" + string + "'"));
        if (item == Items.AIR) {
            throw new JsonSyntaxException("Invalid item: " + string);
        }
        return item;
    }

    @Override
    public Identifier getId() {
        return id;
    }
    
    @Override
    public RecipeSerializer<?> getSerializer() {
        return Serializer.INSTANCE;
    }

    @Override
    public RecipeType<?> getType() {
        return Type.INSTANCE;
    }

    public static class Result {
        public ItemStack item;
        public boolean requiresCollection;

        public Result() {
            this(ItemStack.EMPTY);
        }
        public Result(ItemStack item) {
            this(item, true);
        }
        public Result(ItemStack item, boolean requiresCollection) {
            this.item = item;
            this.requiresCollection = requiresCollection;
        }

        public void write(PacketByteBuf buf) {
            buf.writeItemStack(item);
            buf.writeBoolean(requiresCollection);
        }

        public static Result fromPacket(PacketByteBuf buf) {
            return new Result(buf.readItemStack(), buf.readBoolean());
        }

        public Result copy() {
            return new Result(item.copy(), requiresCollection);
        }
    }

    public static class Type implements RecipeType<AbyssalAltarRecipe> {
        private Type() { }
        public static final Type INSTANCE = new Type();
        public static final String ID = "abyssal_altar";
    }

    public static class Serializer implements RecipeSerializer<AbyssalAltarRecipe> {
        public static final Serializer INSTANCE = new Serializer(5000, true);
        public static final String ID = "abyssal_altar";

        public final int time;
        public final boolean stopPlayer;

        public Serializer(int time, boolean stopPlayer) {
            this.time = time;
            this.stopPlayer = stopPlayer;
        }

        @Override
        public AbyssalAltarRecipe read(Identifier id, JsonObject json) {
            JsonArray blocks = JsonHelper.getArray(json, "blocks");
            DefaultedList<Ingredient> recipeBlocks = DefaultedList.ofSize(8, Ingredient.EMPTY);

            for (int i = 0; i < recipeBlocks.size(); i++) {
                recipeBlocks.set(i, Ingredient.fromJson(blocks.get(i)));
            }

//            JsonObject jsonObject = JsonHelper.getObject(json, "item");
//            String itemString = JsonHelper.getString(jsonObject, "item");
//            boolean returnItem = JsonHelper.getBoolean(jsonObject, "returnitem", false);
////            Ingredient item = new Ingredient(Ingredient.ofStacks(new ItemStack(Registry.ITEM.getOrEmpty(new Identifier(itemString)).orElseThrow(() -> new IllegalStateException("Item: " + itemString + " does not exist")))), returnItem);
            Ingredient item = json.has("item") ? Ingredient.fromJson(JsonHelper.getObject(json, "item")) : Ingredient.EMPTY;


            Result result = json.has("result") ? resultFromJson(JsonHelper.getObject(json, "result")) : new Result();

            boolean stopPlayer = JsonHelper.getBoolean(json, "stopplayer", this.stopPlayer);

            DefaultedList<String> messages = DefaultedList.of();
            if (JsonHelper.hasArray(json, "messages")) {
                JsonArray messagesElement = JsonHelper.getArray(json, "messages");

                if (!messagesElement.isJsonNull()) {
                    if (messagesElement.isJsonArray()) {
                        for (int i = 0; i < messagesElement.getAsJsonArray().size(); i++) {
                            messages.add(messagesElement.getAsJsonArray().get(i).getAsString());
                        }
                    }
                    else {
                        messages.add(messagesElement.getAsString());
                    }
                }
            }
            else if (JsonHelper.hasString(json, "message")) {
                messages.add(JsonHelper.getString(json, "message"));
            }

            int time = JsonHelper.getInt(json, "time", this.time);

            return new AbyssalAltarRecipe(id, result, recipeBlocks, item, time, stopPlayer, messages);
        }

        @Override
        public AbyssalAltarRecipe read(Identifier id, PacketByteBuf buf) {
//            DefaultedList<Ingredient> blocks = DefaultedList.ofSize(8, Ingredient.EMPTY);
            DefaultedList<Ingredient> blocks = DefaultedList.ofSize(buf.readInt(), Ingredient.EMPTY);
//            Ingredient item = Ingredient.ofStacks(buf.readItemStack());
            for (int i = 0; i < blocks.size(); i++) {
                blocks.set(i, Ingredient.fromPacket(buf));
            }

            Ingredient item = Ingredient.fromPacket(buf);

            Result result = Result.fromPacket(buf);

            boolean stopPlayer = buf.readBoolean();

            DefaultedList<String> messages = DefaultedList.ofSize(buf.readInt());
            for (int i = 0; i < messages.size(); i++) {
                messages.set(i, buf.readString());
            }

            int time = buf.readVarInt();

            return new AbyssalAltarRecipe(id, result, blocks, item, time, stopPlayer, messages);
        }

        @Override
        public void write(PacketByteBuf buf, AbyssalAltarRecipe recipe) {
            buf.writeInt(recipe.recipeBlocks.size());
            for (Ingredient block : recipe.recipeBlocks) {
                block.write(buf);
            }

            recipe.recipeItem.write(buf);

            recipe.result.write(buf);

            buf.writeBoolean(recipe.stopPlayer);

            buf.writeInt(recipe.messages.size());
            for (String message : recipe.messages) {
                buf.writeString(message);
            }

            buf.writeVarInt(recipe.time);
        }
    }
}
