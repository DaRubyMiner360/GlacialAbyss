package ml.darubyminer360.glacialabyss.recipe;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModRecipes {
    public static void register() {
        Registry.register(Registry.RECIPE_SERIALIZER, new Identifier(GlacialAbyss.MOD_ID, AbyssalAltarRecipe.Serializer.ID),
                AbyssalAltarRecipe.Serializer.INSTANCE);
        Registry.register(Registry.RECIPE_TYPE, new Identifier(GlacialAbyss.MOD_ID, AbyssalAltarRecipe.Type.ID),
                AbyssalAltarRecipe.Type.INSTANCE);
    }
}
