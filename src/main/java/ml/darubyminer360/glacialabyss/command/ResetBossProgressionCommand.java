package ml.darubyminer360.glacialabyss.command;

import com.mojang.brigadier.CommandDispatcher;
import com.mojang.brigadier.context.CommandContext;
import com.mojang.brigadier.exceptions.CommandSyntaxException;
import ml.darubyminer360.glacialabyss.util.ModComponents;
import net.minecraft.server.command.CommandManager;
import net.minecraft.server.command.ServerCommandSource;
import net.minecraft.text.TranslatableText;
import net.minecraft.util.Formatting;

public class ResetBossProgressionCommand {
    public static void register(CommandDispatcher<ServerCommandSource> dispatcher, boolean dedicated) {
        dispatcher.register(CommandManager.literal("resetbossprogression").executes(ResetBossProgressionCommand::run));
    }

    private static int run(CommandContext<ServerCommandSource> context) throws CommandSyntaxException {
        ModComponents.DEFEATED_PHANTASM.get(context.getSource().getWorld().getLevelProperties()).setValue(false);
        ModComponents.DEFEATED_PARADISE_KING.get(context.getSource().getWorld().getLevelProperties()).setValue(false);
        ModComponents.DEFEATED_HAUNTED_PALADIN.get(context.getSource().getWorld().getLevelProperties()).setValue(false);
        ModComponents.HELPED_HAUNTED_PALADIN.get(context.getSource().getWorld().getLevelProperties()).setValue(false);

        context.getSource().getPlayer().sendMessage(new TranslatableText("message.glacialabyss.reset_boss_progression_success").formatted(Formatting.GREEN), false);
        return 0;
    }
}
