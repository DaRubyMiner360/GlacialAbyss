package ml.darubyminer360.glacialabyss.particle;

import ml.darubyminer360.glacialabyss.GlacialAbyss;
import net.fabricmc.fabric.api.particle.v1.FabricParticleTypes;
import net.minecraft.particle.DefaultParticleType;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

public class ModParticles {
    public static final DefaultParticleType FROST = FabricParticleTypes.simple();

    public static void register() {
        Registry.register(Registry.PARTICLE_TYPE, new Identifier(GlacialAbyss.MOD_ID, "frost"), FROST);
    }
}
